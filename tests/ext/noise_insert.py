import pathlib

import dotenv
import numpy as np
import torch
import torchaudio

from adf.ext.noise_insertion import (
    NoiseInsertionAdfDataset,
    NoiseInsertionSampler,
    crop_or_repeat,
    mix_signal,
)
from adf.loader import AdfDataLoader
from adf.util import get_adf_storage_dir


def _save_loop(directory, ds, sampler):
    directory.mkdir(parents=True, exist_ok=True)

    for batch in AdfDataLoader(ds, sampler, is_batch_sampler=False):
        print(batch)

        for i in range(len(batch.json_list)):
            audio = batch.audio[i : i + 1, :]
            file = directory / f"file_{i:03d}.wav"
            print(audio.shape, file)
            torchaudio.save(file, audio, sample_rate=16_000)
        break


def test_noise_replace():
    data_dir = get_adf_storage_dir()

    ls_data = [
        data_dir
        / "librispeech"
        / "adf"
        / "train-clean-100"
        / "adf_ls_train_clean_100_000005.partial.tar",
        # data_dir / "librispeech" / "adf" / "train-clean-360",
        # data_dir / "librispeech" / "adf" / "train-other-500",
    ]

    fma_data = [data_dir / "fma" / "adf" / "fma_small"]

    ds = NoiseInsertionAdfDataset(
        main_data=ls_data, noise_data=fma_data, mode="replace", seed=123
    )
    sampler = NoiseInsertionSampler(ds, 3_200_000, 500, 100, 0.5)

    directory = pathlib.Path("/tmp/adf/batch/replace")
    _save_loop(directory, ds, sampler)


def test_noise_mix():
    data_dir = get_adf_storage_dir()

    ls_data = [
        data_dir
        / "librispeech"
        / "adf"
        / "train-clean-100"
        / "adf_ls_train_clean_100_000005.partial.tar",
        # data_dir / "librispeech" / "adf" / "train-clean-360",
        # data_dir / "librispeech" / "adf" / "train-other-500",
    ]

    fma_data = [data_dir / "fma" / "adf" / "fma_small"]

    ds = NoiseInsertionAdfDataset(
        main_data=ls_data,
        noise_data=fma_data,
        seed=123,
        mode="mix",
        snr_min=-5,
        snr_max=5,
    )
    sampler = NoiseInsertionSampler(ds, 3_200_000, 500, 100, 1)

    directory = pathlib.Path("/tmp/adf/batch/mix")
    _save_loop(directory, ds, sampler)


def test_crop_or_repeat():
    x = torch.ones((100,))
    rng = np.random.default_rng()

    for i in range(1, 201):
        y, _ = crop_or_repeat(x, desired_length=i, rng=rng)
        assert len(y) == i


def test_mix_signal():
    x, sr = torchaudio.load("ls_92-6488-0000.wav")
    y, sr = torchaudio.load("fma_000002.wav")

    directory = pathlib.Path("/tmp/adf/batch/mix_snr")
    directory.mkdir(exist_ok=True, parents=True)

    torchaudio.save(directory / "a.wav", x, sr)
    torchaudio.save(directory / "b.wav", y, sr)

    for idx, snr in enumerate(range(-20, 21)):
        mix, _ = mix_signal(x[0, :], y[0, :], snr, rng=np.random.default_rng(123))

        torchaudio.save(
            directory / f"mix_{idx:02d}_snr_{snr:02d}.wav", mix[None, :], sr
        )


if __name__ == "__main__":
    dotenv.load_dotenv()
    test_noise_replace()
    test_noise_mix()
    test_crop_or_repeat()
    test_mix_signal()
