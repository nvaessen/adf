########################################################################################
#
# Tests for AdfDataset functionality.
#
# Author(s): Nik Vaessen
########################################################################################

import pathlib

from typing import List

import dotenv

from adf import AdfDataLoader
from adf.dataset import AdfDataset
from adf.samplers.sequential import SequentialBatchSampler
from adf.samplers.similar_length import SimilarLengthBatchSampler
from adf.util import get_adf_storage_dir


########################################################################################
# test reloading of dataset state


def _loop(iterations: int, loader: AdfDataLoader):
    count = 0
    for data in loader:
        count += 1

        tensor, json_content = data
        print(tensor.shape[0] * tensor.shape[1], tensor.shape)

        if count >= iterations:
            break


def normal_iterations(
    iterations: int,
    dataset_path: List[pathlib.Path],
    num_workers: int = 4,
):
    dataset = AdfDataset(dataset_path, 123)
    loader = AdfDataLoader(dataset, num_workers=num_workers)

    _loop(iterations, loader)


def normal_iterations_with_batches(
    iterations: int,
    dataset_path: List[pathlib.Path],
    num_workers: int = 4,
):
    dataset = AdfDataset(dataset_path, 123)
    seq_sampler = SequentialBatchSampler(dataset, batch_size=8)
    loader = AdfDataLoader(dataset, adf_sampler=seq_sampler, num_workers=num_workers)

    _loop(iterations, loader)


def length_equal_iterations(
    iterations: int,
    dataset_path: List[pathlib.Path],
    num_workers: int = 4,
):
    dataset = AdfDataset(dataset_path, 123)
    sampler = SimilarLengthBatchSampler(dataset, 3_200_000, 1000, 100)
    loader = AdfDataLoader(dataset, adf_sampler=sampler, num_workers=num_workers)

    _loop(iterations, loader)


def complete_val(dataset_path: List[pathlib.Path], num_workers: int):
    dataset = AdfDataset(dataset_path, 123)

    # sequentially...
    sampler = SequentialBatchSampler(dataset, batch_size=8)
    loader = AdfDataLoader(dataset, adf_sampler=sampler, num_workers=num_workers)

    for count, x in enumerate(loader):
        print(count)

    # similar length
    sampler = SimilarLengthBatchSampler(dataset, 500_000, 1000, 100)
    loader = AdfDataLoader(dataset, adf_sampler=sampler, num_workers=num_workers)

    for count, x in enumerate(loader):
        print(count)


def main():
    storage_dir = get_adf_storage_dir()

    train_dataset_path = [
        storage_dir / "librispeech" / "adf" / "train-clean-100",
        storage_dir / "librispeech" / "adf" / "train-clean-360",
        storage_dir / "librispeech" / "adf" / "train-other-500",
    ]

    val_dataset_path = [
        storage_dir / "librispeech" / "adf" / "val-clean-100",
        storage_dir / "librispeech" / "adf" / "val-clean-360",
        storage_dir / "librispeech" / "adf" / "val-other-500",
    ]

    iterations = 5
    num_workers = 4

    print("normal with default batch size (1)")
    normal_iterations(iterations, train_dataset_path, num_workers=num_workers)

    print("normal with larger batch size (8)")
    normal_iterations_with_batches(
        iterations, train_dataset_path, num_workers=num_workers
    )

    print("batch size of 3.2M samples (200 sec)")
    length_equal_iterations(iterations, train_dataset_path, num_workers=num_workers)

    print("loop over validation set")
    complete_val(val_dataset_path, num_workers=num_workers)


if __name__ == "__main__":
    dotenv.load_dotenv()
    main()
