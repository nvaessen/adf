########################################################################################
#
# A map-based dataset for ADF datasets.
#
# Author(s): Nik Vaessen
########################################################################################

import io
import json
import pathlib

from collections import defaultdict

from dataclasses import dataclass, field
from typing import Union, List, Any, Dict, BinaryIO, NamedTuple

import fastjsonschema
import numpy as np
import torch
import torchaudio
import pandas as pd

from torch.utils.data import Dataset, get_worker_info

from adf.format import get_index_dataframe_with_json_schema
from adf.util import get_adf_storage_dir


########################################################################################
# exports

__all__ = ["DatasetItem", "AdfDataOutput", "AdfDataset"]

########################################################################################
# The ADF PyTorch Dataset


@dataclass(order=True)
class DatasetItem:
    """
    Represents a row in the dataset index. This is used by a Sampler to indicate which
    item should be retrieved in the __getitem__ method of the AdfDataset.
    """

    identifier: Any = field(compare=False)
    num_audio_samples: int = field(compare=True)


class AdfDataOutput(NamedTuple):
    """
    The DataLoader will return a batch of data as a tuple.
    The first element will be the audio file(s) as a tensor
    The second elements will be the label(s) as a list of dictionaries.

    The audio tensor will have shape [BATCH_SIZE, NUM_SAMPLES].
    The label dictionaries will be in a list of length BATCH_SIZE

    Being a Tuple, the `audio` is accessible for being pinned by the DataLoader.
    """

    audio: torch.Tensor
    json_list: List[Dict]


class AdfDataset(Dataset):
    def __init__(
        self, adf_directory: Union[pathlib.Path, List[pathlib.Path]], seed: int
    ):
        (
            self.index_df,
            self.json_schema,
            self.tar_files,
            self.idx_files,
        ) = get_index_dataframe_with_json_schema(adf_directory)

        adf_storage_dir = get_adf_storage_dir()
        self.file_handles = defaultdict(
            lambda: {
                f: (adf_storage_dir / f).open("rb")
                for f in pd.unique(self.index_df["relative_tar_file_path"])
            }
        )

        self.json_validator = fastjsonschema.compile(self.json_schema)

        self.seed = seed
        self.rng = np.random.default_rng(self.seed)

    def __len__(self):
        return len(self.index_df)

    def __getitem__(self, item: Union[int, DatasetItem]) -> AdfDataOutput:
        worker_info = get_worker_info()

        data = self.load_from_tar(item, self.index_df, self.file_handles[worker_info])
        self.json_validator(data.json_list[0])

        return data

    @classmethod
    def load_from_tar(
        cls,
        item: Union[int, DatasetItem],
        index_df: pd.DataFrame,
        file_handles: Dict[pathlib.Path, BinaryIO],
    ) -> AdfDataOutput:
        # select row from dataset
        if isinstance(item, int):
            row = index_df.iloc[item]
        else:
            row = index_df.loc[item.identifier]

        # get file handle
        tar_file_handle = file_handles[row.relative_tar_file_path]

        # load audio file
        tar_file_handle.seek(row.audio_byte_start)
        audio_bytes = io.BytesIO(tar_file_handle.read(row.audio_byte_length))
        audio_tensor, sr = torchaudio.load(audio_bytes)

        # load JSON
        tar_file_handle.seek(row.json_byte_start)
        json_bytes = io.BytesIO(tar_file_handle.read(row.json_byte_length))
        json_content = json.load(json_bytes)

        return AdfDataOutput(audio_tensor, [json_content])
