########################################################################################
#
# Entrypoint for the ADF command line interface.
#
# Author(s): Nik Vaessen
########################################################################################

import io
import json
import pathlib
import sys

from typing import List

import click
import dotenv
import joblib
import numpy as np
import pandas as pd
import tqdm

from adf.format import (
    create_adf_tar_files_from_directory,
    create_index_file,
    ADF_DEFAULT_JSON_SCHEMA,
    get_index_dataframe_with_json_schema,
)
from adf.util import get_adf_storage_dir

########################################################################################
# main command grouping subcommands


@click.group()
def cli():
    dotenv.load_dotenv()


########################################################################################
# command to create an ADF dataset from collection of single files


@cli.command()
@click.argument("input_directories", nargs=-1, type=pathlib.Path)
@click.argument("output_directory", nargs=1, type=pathlib.Path)
@click.option("--json-schema", required=False, default=None, type=pathlib.Path)
@click.option("--sort-by-length", is_flag=True, default=False)
@click.option("--frames-per-tar", required=False, default="1h", type=str)
@click.option("--name", required=False, default="", type=str)
@click.option("--audio-ext", required=False, default=".wav", type=str)
@click.option("--workers", required=False, default=-1, type=int)
@click.option("--sample-rate", required=False, default=16_000, type=int)
def tar(
    input_directories: List[pathlib.Path],
    output_directory: pathlib.Path,
    json_schema: pathlib.Path,
    sort_by_length: bool,
    frames_per_tar: str,
    name: str,
    audio_ext: str,
    workers: int,
    sample_rate: int,
):
    if json_schema is None:
        json_schema = ADF_DEFAULT_JSON_SCHEMA
    else:
        with json_schema.open("r") as f:
            json_schema = json.load(f)

    parsed_frames_per_tar = None

    try:
        parsed_frames_per_tar = int(frames_per_tar)
        if parsed_frames_per_tar < 0:
            parsed_frames_per_tar = None
    except ValueError:
        duration = frames_per_tar[:-1]
        unit = frames_per_tar[-1]

        if unit not in ["s", "m", "h", "d"]:
            raise ValueError(f"cannot parse {frames_per_tar=}")
        try:
            duration = int(duration)
        except ValueError:
            raise ValueError(f"cannot parse {frames_per_tar=}")

        if unit == "s":
            parsed_frames_per_tar = duration * sample_rate
        if unit == "m":
            parsed_frames_per_tar = duration * sample_rate * 60
        if unit == "h":
            parsed_frames_per_tar = duration * sample_rate * 60 * 60
        if unit == "d":
            parsed_frames_per_tar = duration * sample_rate * 60 * 60 * 24

    print(
        f"interpreting {frames_per_tar=} as {parsed_frames_per_tar} frames",
        f"with {sample_rate}",
    )

    # first write the tar files
    if name != "":
        name += "_"

    naming_pattern = "adf_" + name + "{:06d}"

    create_adf_tar_files_from_directory(
        root_directory=input_directories,
        output_directory=output_directory,
        json_schema=json_schema,
        sort_by_length=sort_by_length,
        audio_file_extension=audio_ext,
        frames_per_tar=parsed_frames_per_tar,
        naming_pattern=naming_pattern,
        workers=workers,
    )

    # and write the JSON schema
    json_file = output_directory / "schema.json"
    with json_file.open("w") as f:
        json.dump(json_schema, f)

    json_file.chmod(0o440)


########################################################################################
# command to create the index of an ADF dataset


@cli.command()
@click.argument("tar_directory", nargs=1, type=pathlib.Path)
@click.option("--audio-ext", required=False, default=".wav", type=str)
@click.option("--subset", "subset_tag", required=True, type=str)
@click.option("--workers", "workers", default=-1, type=int)
def index(tar_directory: pathlib.Path, audio_ext: str, subset_tag: str, workers: int):
    parallel = joblib.Parallel(n_jobs=workers, return_as="generator")
    tar_files = [f for f in sorted(tar_directory.glob("*.tar"))]

    print(f"found {len(tar_files)} tar files in {tar_directory}", flush=True)
    generator = parallel(
        joblib.delayed(create_index_file)(
            tar_file=f, audio_file_extension=audio_ext, subset_tag=subset_tag
        )
        for f in sorted(tar_files)
    )

    print(f"starting to index with {workers=}", flush=True)
    p = len(str(len(tar_files)))
    for idx, idx_file in enumerate(generator):
        print(f"[{idx+1:0{p}}/{len(tar_files)}] written {idx_file}", flush=True)


########################################################################################
# Command to get statistics on an ADF dataset


@cli.command()
@click.argument("input_directories", nargs=-1, type=pathlib.Path)
@click.option("-j", "--load-json", is_flag=True, default=False)
def info(input_directories: List[pathlib.Path], load_json: bool):
    (df, json_schema, tar_files, idx_files,) = get_index_dataframe_with_json_schema(
        input_directories, validate_single_subset=False
    )

    print("Collecting information about dataset covered by: ")
    for directory in input_directories:
        print(f"\t{directory}")

    print()
    print(f"number of index and tar files: {len(tar_files)}")
    print(f"number of files in dataset: {len(df):_}")

    print(
        "observed sample rates of audio files:",
        ",".join(f"{x:_}" for x in pd.unique(df["sample_rate"])),
    )

    df["duration_in_sec"] = df["num_samples"] / df["sample_rate"]

    duration_in_hour = sum(df["duration_in_sec"]) / 3600
    duration_in_hour, fraction = divmod(duration_in_hour, 1)
    remaining_minutes, fraction = divmod(fraction * 60, 1)
    remaining_seconds = round(fraction * 60)
    print(
        f"duration of audio: "
        f"{duration_in_hour:.0f} hours, "
        f"{remaining_minutes:.0f} minutes and "
        f"{remaining_seconds:.0f} seconds",
    )
    print()

    print(f"minimum duration of files: {min(df['duration_in_sec']):.2f} seconds")
    print(f"maximum duration of files: {max(df['duration_in_sec']):.2f} seconds")
    print(f"average duration of files: {np.mean(df['duration_in_sec']):.2f} seconds")

    df = df.sort_values("duration_in_sec")
    shortest_row = df.iloc[0]
    longest_row = df.iloc[-1]
    pd.set_option("display.max_colwidth", 10000)
    print("\nshortest file:")
    print(shortest_row.to_string())
    print("\nlongest file:")
    print(longest_row.to_string())

    if load_json:
        print("loading all JSON files", file=sys.stderr)
        storage_dir = get_adf_storage_dir()

        def _read(tar_file_path: str, start: int, length: int):
            tar_file = storage_dir / tar_file_path
            with open(tar_file, "rb") as f:
                f.seek(start)
                raw_data = f.read(length)
                return json.load(io.BytesIO(raw_data))

        parallel = joblib.Parallel(n_jobs=-1, return_as="generator")
        generator = parallel(
            joblib.delayed(_read)(
                row.relative_tar_file_path,
                row.json_byte_start,
                row.json_byte_length,
            )
            for name, row in df.iterrows()
        )

        json_files = []
        for x in tqdm.tqdm(generator, total=len(df)):
            if len(json_files) == 0:
                print("example JSON file")
                print(json.dumps(x, indent=2))
                print(flush=True)

            json_files.append(x)

        df_json = pd.DataFrame(json_files)

        if "speaker_id" in df_json.columns:
            print(
                f"Number of unique speaker IDs: {len(df_json['speaker_id'].unique())}"
            )

        if "session_id" in df_json.columns:
            print(
                f"Number of unique session IDs: {len(df_json['session_id'].unique())}"
            )

        if "biological_sex" in df_json.columns:
            num_utterance = len(df_json)
            assert sorted(df_json["biological_sex"].unique().tolist()) == sorted(
                ["M", "F"]
            ), "biological sex should be M or F (sorry)"

            male_utterance_count = len(df_json.loc[df_json["biological_sex"] == "M"])

            male_percentage = round(male_utterance_count / num_utterance * 100, 2)
            female_percentage = round(100 - male_percentage, 2)

            if "speaker_id" in df_json.columns:
                speaker_gender_map = {}
                for j in json_files:
                    speaker_gender_map[j["speaker_id"]] = j["biological_sex"]

                num_speakers = len(speaker_gender_map)
                male_speaker_count = sum(
                    1 if sex == "M" else 0 for sex in speaker_gender_map.values()
                )
                female_speaker_count = sum(
                    1 if sex == "F" else 0 for sex in speaker_gender_map.values()
                )

                print(
                    "male speaker count:",
                    male_speaker_count,
                    f"({male_speaker_count/num_speakers*100:.2f} %)",
                )
                print(
                    "female speaker count:",
                    female_speaker_count,
                    f"({female_speaker_count/num_speakers*100:.2f} %)",
                )

            print(
                f"male/female utterances: "
                f"{male_percentage:.2f}%/{female_percentage:.2f}%"
            )
