########################################################################################
#
# A BatchSampler which randomly fills batches to a particular length - useful when
# data is varied in length.
#
# Author(s): Nik Vaessen
########################################################################################

import warnings

from typing import Iterator, List

import numpy as np
import pandas as pd

from torch.utils.data import Sampler

from adf import AdfDataset, DatasetItem
from adf.samplers.sequential import SequentialBatchSampler

__all__ = ["batch_varying_length_audio", "SimilarLengthBatchSampler"]

########################################################################################
# sort tar files by average duration before accessing sequentially


def sort_index_by_average_total_duration(df: pd.DataFrame):
    # we compute the average length in each tar file and access them sequentially
    map_length = {}

    for name, group_df in df.groupby("relative_tar_file_path"):
        average_length = group_df["num_samples"].mean()
        map_length[name] = average_length

    df["average_length"] = df["relative_tar_file_path"].map(map_length)
    df = df.sort_values("average_length")

    return df.drop(columns="average_length")


########################################################################################
# logic for filling a batch with variable length data to a particular length


def batch_varying_length_audio(
    sample_generator: Iterator[DatasetItem],
    rng: np.random.Generator,
    max_num_audio_samples_in_batch: int,
    global_buffer_size: int,
    batching_buffer_size: int,
) -> Iterator[List[DatasetItem]]:
    """
    Logic for buffering and filling items with a varied lengths to a maximum batch size.

    :param sample_generator: Iterator generating a sequence of identifiers in th
    dataset alongside their lengths.
    :param rng: the random number generator to randomly select subsets to fill batches
    with.
    :param max_num_audio_samples_in_batch: the maximum length of a batch
    :param global_buffer_size: Number of sequential dataset items which are considered
    for filling a batch.
    :param batching_buffer_size: Random subset of global buffer which is sorted by
    length. Batches are sampled from this buffer starting from the shortest or longest
    file, whichever leads to longer batch

    :return: An iterator of the created batches
    """
    global_buffer = []
    batching_buffer_choice = [*range(global_buffer_size)]
    winding_down = False

    while not winding_down or len(global_buffer) > 0:
        # fill pool till full
        while not winding_down and len(global_buffer) < global_buffer_size:
            try:
                global_buffer.append(next(sample_generator))
            except StopIteration:
                winding_down = True

        # we randomly sample `buffer_size` from the candidate pool
        buffer_idx = rng.choice(
            batching_buffer_choice[0 : len(global_buffer)],
            min(batching_buffer_size, len(global_buffer)),
            replace=False,
        )
        buffer = [global_buffer.pop(i) for i in reversed(sorted(buffer_idx))]

        # we sort the buffer by shortest file
        buffer = sorted(buffer, key=lambda ds: ds.num_audio_samples)

        # we select the shortest/longest samples from buffer until max batch size is
        # exceeded
        batch_identifiers_min, batch_length_min, remaining_buffer_min = _select_batch(
            buffer, max_num_audio_samples_in_batch, start_with_largest_sample=False
        )
        batch_identifiers_max, batch_length_max, remaining_buffer_max = _select_batch(
            buffer, max_num_audio_samples_in_batch, start_with_largest_sample=True
        )

        # check whichever strategy lead to the longest batch
        if batch_length_min > batch_length_max:
            batch_identifiers = batch_identifiers_min
            remaining_buffer = remaining_buffer_min
        else:
            batch_identifiers = batch_identifiers_max
            remaining_buffer = remaining_buffer_max

        # if batch is empty we must skip this buffer
        if len(batch_identifiers) == 0:
            warnings.warn(
                f"Every sample in buffer exceeded {max_num_audio_samples_in_batch=}"
            )
            buffer.clear()
            continue

        # if buffer is empty we could've added more batches
        if len(remaining_buffer) == 0:
            warnings.warn("buffer size was too small to completely fill batch")

        # yield the batch
        yield batch_identifiers

        # refill remaining buffer to pool
        global_buffer.extend(remaining_buffer)


def _select_batch(
    original_buffer: List[DatasetItem],
    max_samples: int,
    start_with_largest_sample: bool = False,
):
    # make copy so this function can be called with true/false with same reference list
    buffer = list(original_buffer)

    batch_lengths = []
    batch_identifiers = []
    batch_length = 0

    pop_idx = -1 if start_with_largest_sample else 0

    while len(buffer) > 0:
        next_entry = buffer[pop_idx]

        entry_length = next_entry.num_audio_samples
        batch_lengths.append(entry_length)

        batch_length = max(batch_lengths) * len(batch_lengths)

        if batch_length > max_samples:
            batch_lengths.pop()
            batch_length = max(batch_lengths) * len(batch_lengths)
            break
        else:
            batch_identifiers.append(buffer.pop(pop_idx))

    return batch_identifiers, batch_length, buffer


########################################################################################
# Implementation of the Sampler


class SimilarLengthBatchSampler(Sampler):
    def __init__(
        self,
        dataset: AdfDataset,
        max_num_samples: int,
        pool_size: int,
        buffer_size: int,
        max_utterance_length: int = None,
        min_utterance_length: int = None,
        infinite: bool = False,
    ):
        super().__init__()

        self.dataset = dataset
        self.rng = self.dataset.rng

        self.index_df = self.dataset.index_df
        self.index_df = sort_index_by_average_total_duration(self.index_df)

        self.max_num_samples = max_num_samples
        self.max_utterance_length = max_utterance_length
        self.min_utterance_length = min_utterance_length
        self.pool_size = pool_size
        self.buffer_size = buffer_size

        self.infinite = infinite

    def __len__(self):
        # we cycle forever
        pass

    def __iter__(self):
        # we need to wrap SequentialBatchSampler in order to
        # use our own sorted index and to yield an item instead of a list of length 1
        def _iter_wrap():
            seq_sampler = SequentialBatchSampler(
                self.dataset,
                batch_size=1,
                infinite=self.infinite,
                allow_distribution=self.infinite,
            )
            for item in seq_sampler.__iter__(self.index_df):
                if (
                    self.max_utterance_length is not None
                    and item[0].num_audio_samples > self.max_utterance_length
                ):
                    continue
                if (
                    self.min_utterance_length is not None
                    and item[0].num_audio_samples < self.min_utterance_length
                ):
                    continue

                yield item[0]

        return batch_varying_length_audio(
            _iter_wrap(),
            self.rng,
            self.max_num_samples,
            self.pool_size,
            self.buffer_size,
        )
