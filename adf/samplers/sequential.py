########################################################################################
#
# Sample from an ADF dataset sequentially, in order from the first to last tar file.
# Supports distribution along multiple workers, which will evenly divide the dataset
# along each worker (so batches will not be sequential anymore).
#
# Author(s): Nik Vaessen
########################################################################################

import logging
import math

from typing import Iterator, List

from torch import distributed as dist
from torch.utils.data import Sampler

from adf import AdfDataset, DatasetItem

__all__ = ["SequentialBatchSampler"]

########################################################################################
# The basic (default) BatchSampler which simply batches over the data sequentially

logger = logging.getLogger("SequentialBatchSampler")


class SequentialBatchSampler(Sampler):
    def __init__(
        self,
        dataset: AdfDataset,
        batch_size: int = 1,
        infinite: bool = False,
        drop_last: bool = False,
        allow_distribution: bool = True,
    ):
        super().__init__()

        self.dataset = dataset
        self.batch_size = batch_size
        self.infinite = infinite
        self.drop_last = drop_last

        # get world size
        if dist.is_initialized() and allow_distribution:
            self.world_size = dist.get_world_size()
        else:
            self.world_size = 1

        # if running in DDP mode, we split into roughly equal chunks
        if self.world_size > 1:
            if not infinite:
                raise ValueError(
                    f"{allow_distribution=} and {self.world_size=}, so "
                    "`infinite=True` is required to prevent deadlocks"
                )

            self.rank = dist.get_rank()
            size_per_rank = len(self.dataset) // self.world_size

            self.start_idx = self.rank * size_per_rank
            self.end_idx = min(self.start_idx + size_per_rank, len(self.dataset))

            logger.info(
                f"rank {self.rank}/{self.world_size} has "
                f"{self.start_idx=} and {self.end_idx=} "
                f"with {len(self.dataset)=}"
            )

        else:
            self.start_idx = 0
            self.end_idx = len(self.dataset)

            logger.info(
                f"using {self.start_idx=} and {self.end_idx=} "
                f"with {len(self.dataset)=}"
            )

    def __len__(self):
        if self.infinite:
            return float("inf")
        else:
            num_batches = len(self.dataset) / self.batch_size
            if self.drop_last:
                return math.floor(num_batches)
            else:
                return math.ceil(num_batches)

    def __iter__(self, index_df=None) -> Iterator[List[DatasetItem]]:
        if index_df is None:
            index_df = self.dataset.index_df
        else:
            if len(index_df) != len(self.dataset.index_df):
                raise ValueError("overwritten index should have equal length")

        batch = []
        current_index = self.start_idx - 1

        while True:
            current_index += 1

            if current_index >= self.end_idx:
                if self.infinite:
                    current_index = self.start_idx
                else:
                    if not self.drop_last and len(batch) > 0:
                        yield batch
                    break

            row = index_df.iloc[current_index]
            item = DatasetItem(row.name, row.num_samples)

            if self.batch_size == 1:
                yield [item]
            else:
                batch.append(item)
                if len(batch) == self.batch_size:
                    yield batch
                    batch.clear()
