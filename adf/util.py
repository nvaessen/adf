########################################################################################
#
# Several utility functions shared across the library.
#
# Author(s): Nik Vaessen
########################################################################################

import os
import pathlib

from functools import lru_cache

########################################################################################
# find all ADF directories


def _find_env_variable(env_variable_name: str):
    value = os.getenv(env_variable_name, default=None)

    if value is None:
        raise ValueError(
            f"Environment variable ${env_variable_name} is undefined; "
            f"please set before using the Audio Dataset Format library."
        )
    else:
        return value


@lru_cache()
def get_adf_storage_dir() -> pathlib.Path:
    return pathlib.Path(_find_env_variable("ADF_STORAGE_DIR"))


@lru_cache()
def get_adf_download_dir() -> pathlib.Path:
    return pathlib.Path(_find_env_variable("ADF_DOWNLOAD_DIR"))


@lru_cache()
def get_adf_working_dir() -> pathlib.Path:
    return pathlib.Path(_find_env_variable("ADF_WORK_DIR"))
