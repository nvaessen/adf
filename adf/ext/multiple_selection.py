########################################################################################
#
# This extends the ADF dataset to selects and concatenate multiple files
# (from same/diff speaker) in the dataset as a single 'batch' item.
#
# Author(s): Nik Vaessen
########################################################################################

import pathlib
import warnings

from typing import Union, Dict, BinaryIO, Callable, Iterator

import torch
import pandas as pd

from torch.utils.data import Sampler

from adf.dataset import AdfDataset, DatasetItem, AdfDataOutput
from adf.samplers.sequential import SequentialBatchSampler
from adf.samplers.similar_length import (
    sort_index_by_average_total_duration,
    batch_varying_length_audio,
)


########################################################################################
# paired ADF dataset


class MultipleSelectionAdfDataset(AdfDataset):
    @classmethod
    def load_from_tar(
        cls,
        paired_item: Union[int, DatasetItem],
        index_df: pd.DataFrame,
        file_handles: Dict[pathlib.Path, BinaryIO],
    ) -> AdfDataOutput:
        item1 = paired_item.identifier[0]
        item2 = paired_item.identifier[1]

        audio_tensor, json_list = super().load_from_tar(item1, index_df, file_handles)
        other_audio_tensor, other_json_list = super().load_from_tar(
            item2, index_df, file_handles
        )

        paired_tensor = torch.concat([audio_tensor, other_audio_tensor], dim=1)

        def merge_json():
            merged = {}

            js = json_list[0]
            other_js = other_json_list[0]
            shared_keys = set(js.keys()).intersection(set(other_js.keys()))

            for k in shared_keys:
                v = js[k]

                if k == "transcription":
                    appended_transcript = []

                    for d in other_js["transcription"]:
                        if "start" in d:
                            d["start"] += js["number_of_samples"]
                        if "end" in d:
                            d["end"] += js["number_of_samples"]

                        appended_transcript.append(d)

                    merged[k] = v + appended_transcript
                elif isinstance(v, int):
                    merged[k] = v + other_js[k]
                elif isinstance(v, str):
                    merged[k] = f"{v}+{other_js[k]}"
                else:
                    raise ValueError(f"unable to merge {k=}")

            return [merged]

        return AdfDataOutput(paired_tensor, merge_json())


class MultipleSelectionSequentialBatchSampler(Sampler):
    def __init__(
        self,
        dataset: MultipleSelectionAdfDataset,
        max_num_samples: int,
        pool_size: int,
        buffer_size: int,
        identifier_to_speaker_id: Callable[[str], str],
        identifier_to_session_id: Callable[[str], str],
        same_speaker: bool = False,
    ):
        super().__init__()

        self.dataset = dataset
        self.rng = self.dataset.rng

        self.idx_df = self.dataset.index_df.copy()
        self.idx_df = sort_index_by_average_total_duration(self.idx_df)

        self.max_num_samples = max_num_samples
        self.pool_size = pool_size
        self.buffer_size = buffer_size
        self.same_speaker = same_speaker

        # add column for speaker ID
        self.idx_df["speaker_id"] = self.idx_df.index.map(identifier_to_speaker_id)

        # add column for session ID
        self.idx_df["session_id"] = self.idx_df.index.map(identifier_to_session_id)

        # cache frame of potential pairs for each session ID
        self.cache = {}

        for speaker_id, speaker_df in self.idx_df.groupby("speaker_id"):
            for session_id, session_df in speaker_df.groupby("session_id"):
                unique_speaker_ids = pd.unique(session_df["speaker_id"])
                if len(unique_speaker_ids) != 1:
                    warnings.warn(f"{session_id=} has {speaker_id} speakers")

                self.cache[(speaker_id, session_id)] = list(session_df.index)

    def __len__(self):
        # we cycle forever
        pass

    def __iter__(self):
        return batch_varying_length_audio(
            self._sequential_paired_generator(),
            self.rng,
            self.max_num_samples,
            self.pool_size,
            self.buffer_size,
        )

    def _sequential_paired_generator(self) -> Iterator[DatasetItem]:
        for item in SequentialBatchSampler(
            self.dataset, batch_size=1, infinite=True
        ).__iter__(self.idx_df):
            item = item[0]
            row = self.idx_df.loc[item.identifier]

            if self.same_speaker:
                # with same speaker we can simply sample from the session dataframe
                session_df: pd.DataFrame = self.idx_df.loc[
                    self.cache[(row.speaker_id, row.session_id)]
                ]
                session_df = session_df.drop(item.identifier)

                if len(session_df) == 0:
                    warnings.warn(
                        f"row with index {row.name} did not have any pairing choices"
                    )
                    continue

                pair_identifier = self.rng.choice(session_df.index, size=1)[0]
                paired_row = session_df.loc[pair_identifier]
            else:
                # with other speakers, we randomly sample from the whole index
                # until we hit a different speaker (which we assume is quite likely)
                count = 0
                while True:
                    count += 1

                    pair_identifier = self.rng.choice(self.idx_df.index, size=1)[0]
                    paired_row = self.idx_df.loc[pair_identifier]

                    if paired_row.speaker_id != row.speaker_id:
                        break

                    if count >= 100:
                        warnings.warn(
                            f"sampled for a speaker other than {row.speaker_id} a 100 "
                            f"times without success - skipping"
                        )
                        break

                if count >= 100:
                    continue

            # drop if either are longer than 30 sec
            sec30 = 16_000 * 30
            if row.num_samples > sec30 or paired_row.num_samples > sec30:
                continue

            # randomly flip coin to reverse order
            if self.rng.choice([True, False]):
                tmp = row
                row = paired_row
                paired_row = tmp

            yield DatasetItem(
                identifier=(
                    DatasetItem(row.name, row.num_samples),
                    DatasetItem(paired_row.name, paired_row.num_samples),
                ),
                num_audio_samples=row.num_samples + paired_row.num_samples,
            )
