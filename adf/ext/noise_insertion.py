########################################################################################
#
# This extends the ADF dataset to sample batches from 2 distinct datasets
# (where one just contains noisy data).
#
# Author(s): Nik Vaessen
########################################################################################

import math
import pathlib
import warnings

from typing import List, Tuple

import numpy as np
import torch
import torchaudio.functional

from torch.utils.data import Dataset, Sampler

from adf import AdfDataset, DatasetItem, AdfDataOutput
from adf.samplers.sequential import SequentialBatchSampler
from adf.samplers.similar_length import SimilarLengthBatchSampler

########################################################################################
# The sampler


class NoiseInsertionSampler(Sampler):
    def __init__(
        self,
        ds: "NoiseInsertionAdfDataset",
        max_num_samples: int,
        pool_size: int,
        buffer_size: int,
        noise_batch_size_percentage: float,
    ):
        super().__init__()

        # main dataset
        self.main_ds = ds.main_ds
        self.max_num_samples = max_num_samples
        self.pool_size = pool_size
        self.buffer_size = buffer_size

        # auxiliary dataset
        self.noise_ds = ds.noise_ds
        self.noise_batch_size_percentage = noise_batch_size_percentage
        assert 0 < self.noise_batch_size_percentage <= 1

    def __iter__(self):
        main_sampler = SimilarLengthBatchSampler(
            self.main_ds,
            self.max_num_samples,
            self.pool_size,
            self.buffer_size,
            infinite=True,
        ).__iter__()

        noise_sampler = SequentialBatchSampler(
            self.noise_ds, batch_size=1, infinite=True
        ).__iter__()

        while True:
            main_data = next(main_sampler)

            desired_noise_samples = math.ceil(
                len(main_data) * self.noise_batch_size_percentage
            )

            noise_data = []
            while len(noise_data) < desired_noise_samples:
                noise_data.extend(next(noise_sampler))

            yield main_data, noise_data


########################################################################################
# The dataset


def crop_or_repeat(tensor: torch.Tensor, desired_length: int, rng: np.random.Generator):
    assert len(tensor.shape) == 1
    assert desired_length > 0

    length = tensor.shape[0]

    if length > desired_length:
        # we randomly crop to desired length
        start_idx = rng.integers(low=0, high=length - desired_length, size=())
        end_idx = start_idx + desired_length
        tensor = tensor[start_idx:end_idx]

        return tensor, ("crop", start_idx, end_idx)

    elif length < desired_length:
        # we repeat to desired length
        num_repeats = (desired_length // length) + 1
        tensor = tensor.repeat(num_repeats)[0:desired_length]

        return tensor, ("repeat", num_repeats, desired_length)

    else:
        return tensor, (None, None, None)


def mix_signal(
    audio: torch.Tensor,
    noise: torch.Tensor,
    snr: int,
    rng: np.random.Generator,
):
    assert len(audio.shape) == 1
    assert len(noise.shape) == 1

    noise, op_tuple = crop_or_repeat(noise, audio.shape[0], rng)
    audio = torchaudio.functional.add_noise(audio, noise, torch.tensor(snr))

    return audio, op_tuple


class NoiseInsertionAdfDataset(Dataset):
    def __init__(
        self,
        main_data: List[pathlib.Path],
        noise_data: List[pathlib.Path],
        seed: int,
        mode: str = "replace",
        # only used when mode == 'mix
        snr_min: int = 0,
        snr_max: int = 15,
    ):
        super().__init__()

        self.main_ds = AdfDataset(main_data, seed)
        self.noise_ds = AdfDataset(noise_data, seed)

        self.seed = seed
        self.rng = np.random.default_rng(self.seed)
        self.mode = mode
        assert mode in ["replace", "mix"]

        self.snr_min = snr_min
        self.snr_max = snr_max
        assert self.snr_min <= snr_max

        if snr_min == snr_max:
            warnings.warn(f"{snr_max=} and {snr_min=} are equal")

    def __getitem__(self, items: Tuple[List[DatasetItem], List[DatasetItem]]):
        main_items, noise_items = items

        # serious edge case
        if len(noise_items) > len(main_items):
            noise_items = noise_items[0 : len(main_items)]
            warnings.warn(
                "batch size of noise items was larger than batch size of main items"
            )

        # determine which main items will be replaced/mixed
        replacement_idx = self.rng.choice(
            [i for i in range(len(main_items))], size=(len(noise_items)), replace=False
        ).tolist()

        if self.mode == "replace":
            return self._replace_item(main_items, noise_items, replacement_idx)
        else:
            return self._mix_item(main_items, noise_items, replacement_idx)

    def _mix_item(
        self,
        main_items: List[DatasetItem],
        noise_items: List[DatasetItem],
        replacement_idx: List[int],
    ):
        # load main data
        main_output_list = [self.main_ds[item] for idx, item in enumerate(main_items)]

        # load noise items and mix with main item
        for idx, noise_item in zip(replacement_idx, noise_items):
            audio_output = main_output_list[idx]
            noise_output = self.noise_ds[noise_item]

            audio_tensor = audio_output.audio[0, :]
            noise_tensor = noise_output.audio[0, :]

            # mix signals
            if self.snr_min == self.snr_max:
                snr = self.snr_min
            else:
                snr = self.rng.integers(self.snr_min, self.snr_max, size=()).item()

            audio_tensor, op_tuple = mix_signal(
                audio_tensor, noise_tensor, snr, self.rng
            )
            audio_tensor = audio_tensor[None, :]

            # modify meta data
            audio_json = audio_output.json_list[0]
            noise_json = noise_output.json_list[0]

            audio_identifier = audio_json["identifier"]
            noise_identifier = self._modify_identifier_after_crop_or_repeat(
                noise_json["identifier"], op_tuple
            )
            new_identifier = f"{audio_identifier}&<mix:{noise_identifier}:{snr}/>"

            audio_json["identifier"] = new_identifier

            main_output_list[idx] = AdfDataOutput(audio_tensor, [audio_json])

        return main_output_list

    def _replace_item(
        self,
        main_items: List[DatasetItem],
        noise_items: List[DatasetItem],
        replacement_idx: List[int],
    ):
        # load main data
        main_output_list = [
            None if idx in replacement_idx else self.main_ds[item]
            for idx, item in enumerate(main_items)
        ]

        # load noise items and replace some main items
        for idx, noise_item in zip(replacement_idx, noise_items):
            desired_length = main_items[idx].num_audio_samples

            noise_output = self.noise_ds[noise_item]

            noise_tensor, op_tuple = crop_or_repeat(
                noise_output.audio[0, :], desired_length, self.rng
            )

            noise_tensor = noise_tensor[None, :]
            noise_json = noise_output.json_list[0]

            # modify meta data
            noise_json["number_of_samples"] = noise_tensor.shape[1]
            noise_json["identifier"] = self._modify_identifier_after_crop_or_repeat(
                noise_json["identifier"], op_tuple
            )

            main_output_list[idx] = AdfDataOutput(noise_tensor, [noise_json])

        return main_output_list

    @staticmethod
    def _modify_identifier_after_crop_or_repeat(identifier: str, op_tuple):
        if op_tuple[0] is None:
            augment_str = ""
        elif op_tuple[0] == "crop":
            augment_str = f"&<crop:{op_tuple[1]}:{op_tuple[2]}/>"
        elif op_tuple[0] == "repeat":
            augment_str = f"&<repeat:{op_tuple[1]}x:{op_tuple[2]}/>"
        else:
            raise ValueError(f"unknown operation {op_tuple[0]}")

        return identifier + augment_str
