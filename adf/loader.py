########################################################################################
#
#
#
# Author(s): Nik Vaessen
########################################################################################

import pathlib

from typing import Union, List, TypeVar, Callable, Optional

import numpy as np
import torch

from torch.utils.data import DataLoader, Sampler

from adf import AdfDataset
from adf.dataset import AdfDataOutput

########################################################################################
# exports

__all__ = ["AdfDataLoader", "default_log", "default_collate", "pad_and_batch"]

########################################################################################
# wrapper around PyTorch DataLoader with logging of data pipeline


def pad_and_batch(audio_tensors: List[torch.Tensor]):
    original_lengths = [t.shape[1] for t in audio_tensors]
    max_length = max(original_lengths)

    padded_audio = torch.concat(
        [
            torch.nn.functional.pad(t, (0, max_length - t.shape[1]))
            for t in audio_tensors
        ]
    )

    return padded_audio, original_lengths


def default_collate(data: Union[AdfDataOutput, List[AdfDataOutput]]):
    if isinstance(data, list):
        # assume 1 audio file per element in list
        assert [len(d.json_list) == 1 for d in data]

        audio_tensor, _ = pad_and_batch([d.audio for d in data])
        json_content = [d.json_list[0] for d in data]

        return AdfDataOutput(audio_tensor, json_content)
    else:
        return data


def default_log(data: AdfDataOutput, log_file: pathlib.Path):
    audio_tensor, json_content = data

    batch_size = len(json_content)
    num_samples = audio_tensor.shape[0] * audio_tensor.shape[1]
    identifiers = [j["identifier"] for j in json_content]

    with log_file.open("a") as f:
        f.write(f"{batch_size=} | {num_samples=} | {identifiers=}\n")


def default_worker_init(worker_id: int):
    info = torch.utils.data.get_worker_info()
    dataset = info.dataset

    if isinstance(dataset, AdfDataset):
        dataset.seed = dataset.seed + worker_id
        dataset.rng = np.random.default_rng(dataset.seed)
    elif (
        hasattr(dataset, "rng")
        and hasattr(dataset, "seed")
        and isinstance(dataset.rng, np.random.Generator)
    ):
        dataset.seed = dataset.seed + worker_id
        dataset.rng = np.random.default_rng(dataset.seed)


O = TypeVar("O")


class AdfDataLoader(DataLoader):
    def __init__(
        self,
        dataset: AdfDataset,
        # control of ADF data
        adf_sampler: Sampler = None,
        collate_fn: Callable[[List[AdfDataOutput]], O] = default_collate,
        # controls of logging data pipeline
        log_fn: Callable[[O, pathlib.Path], str] = default_log,
        log_dir: pathlib.Path = pathlib.Path.cwd(),
        log_name="adf.run_{:04d}",
        log_file_search_pattern: str = "adf.run_*.log",
        use_latest_log_file: bool = False,
        # other settings of PyTorch DataLoader
        num_workers: int = 0,
        pin_memory: bool = True,
        pin_memory_device: str = "",
        timeout: float = 0,
        worker_init_fn: Callable[[int], None] = default_worker_init,
        prefetch_factor: Optional[int] = None,
        persistent_workers: bool = False,
        multiprocessing_context=None,
        is_batch_sampler: bool = True,
    ):
        super().__init__(
            dataset,
            # settings allowed to be set by users
            num_workers=num_workers,
            batch_sampler=adf_sampler if is_batch_sampler else None,
            pin_memory=pin_memory,
            pin_memory_device=pin_memory_device,
            timeout=timeout,
            collate_fn=collate_fn,
            worker_init_fn=worker_init_fn,
            prefetch_factor=prefetch_factor,
            persistent_workers=persistent_workers,
            multiprocessing_context=multiprocessing_context,
            # settings we disable by design of ADF
            batch_size=1 if is_batch_sampler else None,
            sampler=None if is_batch_sampler else adf_sampler,
            shuffle=None,
            drop_last=False,
        )

        # setup logging
        num_log_files = len([f for f in log_dir.glob(log_file_search_pattern)])

        if use_latest_log_file:
            log_file_prefix = log_name.format(num_log_files)
        else:
            log_file_prefix = log_name.format(num_log_files + 1)

        self.log_file = log_dir / f"{log_file_prefix}.log"
        self.log_fn = log_fn

    def __iter__(self):
        for data in super().__iter__():
            self.log_fn(data, self.log_file)
            yield data
