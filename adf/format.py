########################################################################################
#
# This file implements the logic to format a dataset in the Audio Dataset Format (ADF).
#
# A dataset in ADF requires:
# 1) a list of (absolute) paths to tar files (ending with `.tar`)
# 2) a list of paths to index file, where are equivalent to 1) but end with `.idx`
# #) A path to a JSON schema file (https://json-schema.org/)
#
# Each tar file contains a set of audio files and a corresponding JSON file
# (with any meta-information, such as transcription).
# The tar files are made seekable by providing an index files.
# An index file is a dataframes (stored in the Apache feather format) describing the
# (byte) location of each audio file and its JSON file in the tarfile
# The JSON schema file describes the format of the JSON files in the tar files.
#
# Author(s): Nik Vaessen
########################################################################################

import datetime
import functools
import os
import random
import json
import pathlib
import sys
import tarfile
import io

from typing import List, NamedTuple, Optional, Union, Tuple

import torchaudio
import fastjsonschema
import joblib

import pyarrow.feather as feather
import pandas as pd
import pandera as pa
import tqdm

from pandera.typing import Index, Series

from adf.util import get_adf_storage_dir


########################################################################################
# Schema of the ADF index (which is a dataframe)


class AdfIndexSchema(pa.DataFrameModel):
    # unique identifier of the audio file
    identifier: Index[str] = pa.Field(unique=True, str_length={"min_value": 1})

    # required information to read the audio file
    audio_byte_start: int = pa.Field(ge=0)  # the starting location in the tarfile
    audio_byte_length: int = pa.Field(ge=0)  # the offset to end of file

    # required information to read the JSON file
    json_byte_start: int = pa.Field(ge=0)  # the starting location in the tarfile
    json_byte_length: int = pa.Field(ge=0)  # the offset to end of file

    # required information about the audio file
    sample_rate: int = pa.Field(ge=0)  # samples per second
    num_samples: int = pa.Field(ge=0)  # total number of samples in audio file

    # tag for dataset subset (e.g., train/val/dev/test/eval/hold-out)
    subset_tag: str = pa.Field()

    # path to tar file (relative to $ADF_STORAGE_DIR)
    relative_tar_file_path: str = pa.Field()

    @classmethod
    @pa.check("relative_tar_file_path", name="path exists")
    def check_tar_file_path(cls, relative_tar_file_path: Series[str]):
        storage_dir = get_adf_storage_dir()

        return [(storage_dir / path).exist() for path in relative_tar_file_path]


########################################################################################
# A standard schema for the JSON file to cover most use-cases

ADF_DEFAULT_JSON_SCHEMA = {
    "description": "A default schema for providing labels for audio files.",
    "type": "object",
    "properties": {
        "identifier": {
            "description": "A unique identifier for the audio file over the whole dataset",
            "type": "string",
        },
        "transcription": {
            "type": ["array", "null"],
            "items": {
                "type": "object",
                "properties": {
                    "token": {
                        "description": "A token (as a sequence of one or more symbols) describing a particular event, such as a spoken word, or certain behaviour (like <sil>, <laugh> or <happy>)",
                        "type": "string",
                    },
                    "start": {
                        "description": "The location in the audio file where this token starts (given as sample number)",
                        "type": "integer",
                        "minimum": 0,
                    },
                    "end": {
                        "description": "The location in the audio file where this token ends (given as sample number)",
                        "type": "integer",
                        "minimum": 0,
                    },
                    "speaker_id": {
                        "description": "If there are multiple speakers, identify who spoke the token",
                        "type": "string",
                    },
                },
                "required": ["token"],
                "minItems": 0,
            },
        },
        "speaker_id": {
            "description": "An identifier for the speaker in the audio file (assuming there is only 1)",
            "type": ["string", "null"],
        },
        "language_tag": {
            "description": "IETF BCP 47 identifier for the language spoken in the audio file (assuming there is only 1)",
            "type": ["string", "null"],
        },
        "session_id": {
            "description": "An identifier for the recording session of the audio file (if it was, e.g., cut into multiple files)",
            "type": ["string", "null"],
        },
        "biological_sex": {
            "description": "An identifier for the biological sex of the speaker",
            "type": ["string", "null"],
        },
        "number_of_speakers": {
            "description": "The number of speakers in the audio file",
            "type": "integer",
            "minimum": 1,
        },
        "number_of_samples": {
            "description": "The number of audio samples in the audio file",
            "type": "integer",
            "minimum": 1,
        },
        "number_of_channels": {
            "description": "The number of audio channels in the audio file",
            "type": "integer",
            "minimum": 1,
        },
        "sample_rate": {
            "description": "The number of samples per second in the audio file",
            "type": "integer",
            "minimum": 1,
        },
    },
    "required": ["identifier"],
}


########################################################################################
# Logic for browsing a tarfile


class FileInTar(NamedTuple):
    name: str
    byte_offset: int
    byte_length: int


def get_files_in_tar_file(tar_file: pathlib.Path) -> List[FileInTar]:
    records = []

    with tarfile.open(tar_file, "r") as db:
        for tarinfo in db:
            if tarinfo.isfile():
                records.append(
                    FileInTar(
                        name=tarinfo.name,
                        byte_offset=tarinfo.offset_data,
                        byte_length=tarinfo.size,
                    )
                )

    return records


########################################################################################
# Logic for writing the index of a tarfile


def get_audio_file_metadata(tar_file: pathlib.Path, file: FileInTar):
    with tar_file.open("rb") as f:
        f.seek(file.byte_offset)
        data = f.read(file.byte_length)
        buffer = io.BytesIO(data)

        meta: torchaudio.AudioMetaData = torchaudio.info(buffer, backend="ffmpeg")

    return meta


def get_json_file(tar_file: pathlib.Path, file: FileInTar):
    with tar_file.open("rb") as f:
        f.seek(file.byte_offset)
        data = f.read(file.byte_length)
        buffer = io.BytesIO(data)

        return json.load(buffer)


def create_index_file(
    tar_file: pathlib.Path, audio_file_extension: str, subset_tag: str
):
    # find all files tar file
    content = get_files_in_tar_file(tar_file)
    adf_storage_dir = get_adf_storage_dir()
    relative_tar_file_path = tar_file.relative_to(adf_storage_dir)

    # split into audio and json files
    json_map = {}
    audio_map = {}

    for file in content:
        if file.name.endswith(".json"):
            key = file.name.removesuffix(".json")
            json_map[key] = file

        elif file.name.endswith(audio_file_extension):
            key = file.name.removesuffix(audio_file_extension)
            audio_map[key] = file

        else:
            raise ValueError(f"could not parse {file}")

    # check if all keys are equal
    unpaired_keys = set(json_map.keys()).difference(set(audio_map.keys()))

    if len(unpaired_keys) > 0:
        raise ValueError(
            f"The following keys are unpaired in {str(tar_file)}: "
            f"{list(unpaired_keys)}"
        )

    # create entries from pairs
    entries = []

    for key in json_map.keys():
        audio_file = audio_map[key]
        json_file = json_map[key]

        audio_meta = get_audio_file_metadata(tar_file, audio_file)

        entries.append(
            {
                "identifier": key,
                "audio_byte_start": audio_file.byte_offset,
                "audio_byte_length": audio_file.byte_length,
                "json_byte_start": json_file.byte_offset,
                "json_byte_length": json_file.byte_length,
                "sample_rate": audio_meta.sample_rate,
                "num_samples": audio_meta.num_frames,
                "subset_tag": subset_tag,
                "relative_tar_file_path": str(relative_tar_file_path),
            }
        )

    # create a dataframe from entries
    df = pd.DataFrame(entries)
    df = df.set_index("identifier")
    AdfIndexSchema.validate(df)

    # write the index file
    index_file = tar_file.parent / f"{tar_file.stem}.idx"

    if index_file.exists():
        index_file.unlink()

    feather.write_feather(df, str(index_file))
    index_file.chmod(0o440)

    return index_file


########################################################################################
# logic for writing tar files from a (nested) directory


def filter_tarinfo(ti: tarfile.TarInfo, mtime=int(datetime.datetime.now().timestamp())):
    ti.uname = "research"
    ti.gname = "data"
    ti.mode = 0o0444  # everyone can read
    ti.mtime = mtime

    return ti


def write_tar_file(
    tar_file: pathlib.Path,
    file_content: List[Tuple[pathlib.Path]],
    audio_file_extension: str,
):
    tar_file.parent.mkdir(exist_ok=True, parents=True)

    with tarfile.TarFile(str(tar_file), mode="w") as archive:
        for audio_file, json_file in file_content:
            # avoid adding symlinks
            audio_file = audio_file.resolve()
            json_file = json_file.resolve()

            assert audio_file.name.endswith(audio_file_extension)
            assert json_file.name.endswith(".json")

            archive.add(audio_file, arcname=audio_file.name, filter=filter_tarinfo)
            archive.add(json_file, arcname=json_file.name, filter=filter_tarinfo)

    tar_file.chmod(0o440)

    return tar_file


def _write_error_message(
    message: str, error_files: List[pathlib.Path], max_display: int = 10
):
    err_msg = f"{message}\n"

    count = -1
    while len(error_files) > 0:
        count += 1

        if count >= max_display:
            err_msg += f"\t{len(error_files)} additional files truncated..."
            break
        else:
            err_msg += f"\t{str(error_files.pop())}\n"

    return err_msg


def create_adf_tar_files_from_directory(
    root_directory: Union[pathlib.Path, List[pathlib.Path]],
    output_directory: pathlib.Path,
    json_schema: dict,
    workers: int,
    audio_file_extension: str = ".wav",
    sort_by_length: bool = True,
    frames_per_tar: Optional[int] = None,
    naming_pattern: str = "adf_{:06d}",
    rng: random.Random = random.Random(123),
):
    # find all audio files in the (nested) directory structure
    audio_files = []

    if isinstance(root_directory, pathlib.Path):
        root_directory = [root_directory]

    print(f"collecting audio files from {len(root_directory)} directories:")
    for folder in root_directory:
        if not folder.exists():
            raise ValueError(f"{folder} does not exist")

        glob_pattern = f"*{audio_file_extension}"
        print(
            f"{folder} - recursively globbing for `{glob_pattern}` - ",
            end="",
            flush=True,
        )

        audio_in_folder = [f for f in sorted(folder.rglob(glob_pattern))]
        audio_files.extend(audio_in_folder)

        print(f"found {len(audio_in_folder)} files")

    if len(audio_files) == 0:
        raise ValueError(f"did not find any files with {audio_file_extension}")
    else:
        print(f"found a total of {len(audio_files)} audio files")

    # check for duplicate names
    assert len(audio_files) == len(set([f.stem for f in audio_files]))

    # check that all audio files are readable by torchaudio and check their length
    audio_file_frames = _verify_audio_files(audio_files, workers)
    total_frames = sum(audio_file_frames.values())
    min_frames = min(audio_file_frames.values())
    max_frames = max(audio_file_frames.values())

    if frames_per_tar is None:
        frames_per_tar = total_frames

    if frames_per_tar < min_frames:
        raise ValueError(f"{frames_per_tar=} should be higher than {min_frames=}")

    if frames_per_tar < max_frames:
        raise ValueError(f"{frames_per_tar=} should be higher than {max_frames=}")

    # check that all audio files have a corresponding JSON file and validate the schema
    json_files = _verify_json(audio_files, json_schema, workers)

    # determine order of files in (consecutive) tar files
    if sort_by_length:
        audio_files = sorted(audio_files, key=lambda f: audio_file_frames[f])
    else:
        # we sort on name before shuffle for reproducibility
        audio_files = sorted(audio_files)
        rng.shuffle(audio_files)

    # decide on subsets of tar files
    tar_files = []

    boundaries = []
    current_start_idx = 0
    current_frames = 0

    for idx, f in enumerate(audio_files):
        if (new_frames := current_frames + audio_file_frames[f]) <= frames_per_tar:
            current_frames = new_frames
        else:
            # found boundary
            start_idx = current_start_idx
            end_idx = idx

            boundaries.append((start_idx, end_idx))

            # reset state
            current_start_idx = idx
            current_frames = audio_file_frames[f]

    if current_frames > 0:
        boundaries.append((current_start_idx, len(audio_files)))

    assert sum(len(audio_files[le:ri]) for le, ri in boundaries) == len(audio_files)

    # collect files for each tar
    arguments = []
    for start_idx, end_idx in boundaries:
        subset = audio_files[start_idx:end_idx]

        # collect all files to be included in tar file
        subset_files = zip(subset, [json_files[f] for f in subset])

        # determine filename
        split_idx = len(arguments)
        tar_file = output_directory / f"{naming_pattern.format(split_idx)}.tar"

        arguments.append((tar_file, subset_files, audio_file_extension))

    # parallel writing of tar files
    parallel = joblib.Parallel(n_jobs=workers, return_as="generator")
    generator = parallel(joblib.delayed(write_tar_file)(*args) for args in arguments)

    with tqdm.tqdm(total=len(arguments)) as pbar:
        for tar_file in generator:
            tar_files.append(tar_file)
            pbar.update(1)
            pbar.write(f"written {tar_file}")
            sys.stdout.flush()

    return tar_files


def _verify_audio_files(audio_files: List[pathlib.Path], workers: int):
    # check that all audio files are readable by torchaudio and check their length
    def check_audio_fn(file: pathlib.Path):
        has_error = False

        try:
            meta = torchaudio.info(file, backend="ffmpeg")
            num_frames = meta.num_frames
        except RuntimeError:
            has_error = True
            num_frames = None

        return file, num_frames, has_error

    print("verifying each audio file by probing it with torchaudio")
    parallel = joblib.Parallel(n_jobs=workers, return_as="generator")
    generator = parallel(joblib.delayed(check_audio_fn)(f) for f in audio_files)

    audio_file_frames = {}
    invalid_audio_files = []
    found_error = False

    for f, length, error in tqdm.tqdm(generator, total=len(audio_files)):
        if error:
            found_error = True
            invalid_audio_files.append(f)

        audio_file_frames[f] = length

    if found_error:
        raise ValueError(
            _write_error_message("Found invalid audio files:", invalid_audio_files)
        )

    return audio_file_frames


def _verify_json(audio_files: List[pathlib.Path], json_schema: dict, workers: int):
    # schema to verify json (make copy on each worker)
    schema_cache = {}

    # function to verify each file
    def check_json_fn(file: pathlib.Path):
        file_json = file.parent / f"{file.stem}.json"

        is_missing = not file_json.exists()
        is_invalid = False

        pid = os.getpid()
        if pid not in schema_cache:
            schema_cache[pid] = fastjsonschema.compile(json_schema)

        with file_json.open("r") as f:
            json_content = json.load(f)
            try:
                schema_cache[pid](json_content)
            except fastjsonschema.JsonSchemaException as e:
                is_invalid = True

        return file, file_json, is_missing, is_invalid

    # stores global information
    json_files = {}
    missing_json_files = []
    invalid_json_files = []
    found_json_error = False

    # joblib parallelization

    # joblib doesn't play well with fastjsonschema...
    if workers == -1:
        print(f"overwritten {workers=} to", end=" ")

        cpus_available = len(os.sched_getaffinity(0))
        workers = min(4, cpus_available)
        print(f"{workers=}")

    parallel = joblib.Parallel(n_jobs=workers, return_as="generator")
    generator = parallel(joblib.delayed(check_json_fn)(f) for f in audio_files)

    print("verifying each audio file has a valid JSON file")
    for audio_file, json_file, missing, invalid in tqdm.tqdm(
        generator, total=len(audio_files)
    ):
        json_files[audio_file] = json_file

        if missing:
            missing_json_files.append(json_file)

        if invalid:
            invalid_json_files.append(json_file)

    if found_json_error:
        if len(missing_json_files) > 0 and len(invalid_json_files) > 0:
            err_msg = "Found missing and invalid json files.\n"
        elif len(missing_json_files) > 0:
            err_msg = "Found missing json files.\n"
        else:
            err_msg = "Found invalid json files.\n"

        if len(missing_json_files) > 0:
            err_msg += _write_error_message("missing files:", missing_json_files)

        if len(invalid_json_files) > 0:
            err_msg += _write_error_message("invalid files:", invalid_json_files)

        raise ValueError(err_msg)
    else:
        print("found a valid JSON file for each audio file")

    return json_files


########################################################################################
# find and load all relevant paths from one or more ADF directories


def get_index_dataframe_with_json_schema(
    adf_paths: Union[pathlib.Path, List[pathlib.Path]],
    validate_single_subset: bool = True,
):
    # storage for all find
    idx_files = []
    tar_files = []
    json_schemas = []

    # collect all relevant files
    if isinstance(adf_paths, pathlib.Path):
        adf_paths = [adf_paths]

    adf_paths = [pathlib.Path(d) for d in adf_paths]

    assert len(adf_paths) >= 1

    for path in adf_paths:
        if not path.exists():
            raise ValueError(f"{path.absolute()} does not exist")

        if path.is_file():
            if path.suffix == ".idx":
                idx = [path]
                tar = [path.parent / f"{path.stem}.tar"]

                if not tar[0].exists():
                    raise ValueError(f"{tar[0]} does not exist")

            elif path.suffix == ".tar":
                idx = [path.parent / f"{path.stem}.idx"]
                tar = [path]

                if not idx[0].exists():
                    raise ValueError(f"{idx[0]} does not exist")

            else:
                raise ValueError(
                    f"given path {path} is not a folder, nor a .tar of .idx file"
                )

            schema = [path.parent / "schema.json"]
            if not schema[0].exists():
                raise ValueError(f"{schema[0]} does not exist")
        else:
            idx = sorted(list(path.glob("*.idx")))
            tar = sorted(list(path.glob("*.tar")))
            schema = list(path.glob("schema.json"))

        if len(tar) == 0:
            raise ValueError(f"no .tar files in {path}")
        if len(idx) == 0:
            raise ValueError(f"no .idx files in {path}")
        if len(idx) != len(tar):
            raise ValueError(f"expected equal amount of .tar and .idx files in {path}")
        if len(schema) > 1:
            raise ValueError(
                f"expected a single 'schema.json' file in {path}, found {schema}"
            )

        for f in idx:
            match = f.parent / f"{f.stem}.tar"
            if match not in tar:
                raise ValueError(f"{f} has no matching {match}")

        for f in tar:
            match = f.parent / f"{f.stem}.idx"
            if match not in idx:
                raise ValueError(f"{f} has no matching {match}")

        idx_files.extend(idx)
        tar_files.extend(tar)
        json_schemas.extend(schema)

    # assert order is equal
    assert len(idx_files) == len(tar_files)
    zipped_idx_tar = sorted([*zip(idx_files, tar_files)], key=lambda tpl: tpl[0])
    for idx, tar in zipped_idx_tar:
        assert idx.parent == tar.parent
        assert idx.stem == tar.stem

    # check whether schema's differ
    assert len(json_schemas) == len(adf_paths)
    json_schema_content = list()

    for schema in json_schemas:
        with schema.open("r") as f:
            json_schema_content.append(json.load(f))

    if not all(json_schema_content[0] == e for e in json_schema_content):
        # TODO: maybe UserWarning instead?
        raise ValueError("JSON schemas should be equivalent")

    # create dataframe from each index file
    dataframes = []

    for idx, tar in zipped_idx_tar:
        frame = feather.read_feather(idx)
        AdfIndexSchema.validate(frame)

        tar_files_in_idx = pd.unique(frame["relative_tar_file_path"])

        if len(tar_files_in_idx) != 1:
            raise ValueError(
                f"{idx} contains `tar_file_path` values other than {tar}: "
                f"{tar_files_in_idx}"
            )

        dataframes.append(frame)

    df = pd.concat(dataframes)
    AdfIndexSchema.validate(df)

    if validate_single_subset and len(subset_tags := pd.unique(df.subset_tag)) != 1:
        raise ValueError(
            f"Expecting all indexes to belong to a single subset, found {subset_tags=}"
        )

    return df, json_schema_content[0], tar_files, idx_files
