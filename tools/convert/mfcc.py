#! /usr/bin/env python3
########################################################################################
#
# This tools converts a set of audio files to MFCC features.
#
# Author(s): Nik Vaessen
########################################################################################

import pathlib

import click
import joblib
import torch
import torchaudio

########################################################################################
# Function to compute MFCC and store result

transforms = {}


def wav_to_mfcc(
    infile: pathlib.Path,
    outfile: pathlib.Path,
    window: int,
    stride: int,
    overwrite: bool,
):
    """
    Compute the MFCC features of an audio file and store them as a PyTorch tensor.

    Parameters
    ----------
    infile: path to file which needs to be converted
    outfile: path where converted file needs to be stored
    window: the window size (in number of samples) for the MFCC feature computation
    stride: the stride of the window (in number of samples) for the MFCC computation
    overwrite: if true, and outfile exists, nothing is done
    """
    if outfile.exists() and not overwrite:
        return

    audio, sr = torchaudio.load(infile)

    mfcc_transform = torchaudio.transforms.MFCC(
        sample_rate=sr, melkwargs={"hop_length": stride, "win_length": window}
    )

    mfcc = mfcc_transform(audio)
    torch.save(mfcc, outfile)


########################################################################################
# main command


@click.command(
    "Compute the MFCC features a set of audio files in a given root directory"
)
@click.option(
    "--dir",
    "directory_path",
    type=pathlib.Path,
    help="root directory containing (subdirectories of) audio files",
    required=True,
)
@click.option(
    "--ext",
    "audio_extension",
    type=str,
    help="the extension of the audio file(s)",
    required=True,
)
@click.option(
    "--out",
    "save_extension",
    type=str,
    help="the extension of the file to safe MFCC features in",
    default=".mfcc.pt",
)
@click.option(
    "--workers",
    type=int,
    default=1,
    help="Number of CPU cores to use for converting the dataset files.",
)
@click.option(
    "--overwrite",
    is_flag=True,
    help="overwrite a previously existing file storing the MFCC features",
)
@click.option(
    "--window",
    type=int,
    default=400,
    help="window length (in number of samples) when computing MFCC values",
)
@click.option(
    "--stride",
    type=int,
    default=320,
    help="stride (in number of samples) of the window when computing MFCC values",
)
def main(
    directory_path: pathlib.Path,
    audio_extension: str,
    save_extension: str,
    workers: int,
    overwrite: bool,
    window: int,
    stride: int,
):
    if audio_extension == save_extension:
        raise ValueError(
            f"--ext={audio_extension} cannot be equal to --out={save_extension}"
        )
    if len(audio_extension) <= 0:
        raise ValueError(f"{audio_extension=} must be non-empty")
    if len(save_extension) <= 0:
        raise ValueError(f"{save_extension=} must be non-empty")

    # find all files in the train and test subdirectories
    print(
        f"recursively finding all files in `{directory_path}` with pattern "
        f"'*{audio_extension}'"
    )
    audio_files = [
        f for f in directory_path.rglob(f"*{audio_extension}") if f.is_file()
    ]

    # use multiple workers to call FFMPEG and convert the .m4a files to .wav
    print(
        f"found {len(audio_files)} files with "
        f"`*{audio_extension}` in {directory_path} "
    )

    if len(audio_files) == 0:
        exit()

    # use multiple workers to compute and save the MFCC features
    joblib.Parallel(n_jobs=workers, verbose=1)(
        [
            joblib.delayed(wav_to_mfcc)(
                infile,
                infile.parent / (infile.stem + save_extension),
                window,
                stride,
                overwrite,
            )
            for infile in audio_files
        ]
    )


if __name__ == "__main__":
    main()
