#! /usr/bin/env python3

########################################################################################
#
# This tools converts a set of audio files to 16 kHz, 16 bit (pcm_s16le), mono audio
# by using FFMPEG.
#
# Author(s): Nik Vaessen
########################################################################################

import pathlib
import subprocess

import click
import joblib

########################################################################################
# methods for converting


def subprocess_convert_to_wav(
    infile: pathlib.Path,
    outfile: pathlib.Path,
    overwrite: bool = False,
    delete_original: bool = False,
):
    """
    Use a subprocess calling FFMPEG to convert a file to 16 KHz .wav file.
    If the outfile already exists, and `overwrite` is set to False, nothing is done.

    Parameters
    ----------
    infile: Path to file which needs to be converted
    outfile: Path where converted file needs to be stored
    overwrite: Must be True when infile and outfile are equal.
    When set to True, if outfile exists, and is different from infile, the
    outfile is overwritten. When set to False, if outfile exists and is
    different from infile, nothing is done.
    delete_original: If true, the infile is deleted after conversion. Cannot be set to
    True when infile and outfile are equal.
    """
    inplace = False

    if infile == outfile:
        if delete_original:
            raise ValueError(
                f"cannot delete original as {infile=} is equal to {outfile=}"
            )
        if not overwrite:
            raise ValueError(f"{infile=} is equal to {outfile=} while {overwrite=}")
        inplace = True
    elif outfile.exists():
        if overwrite:
            outfile.unlink()
        else:
            return

    try:
        if inplace:
            work_file = outfile.parent / f"tmp_{outfile.name}"
        else:
            work_file = outfile

        subprocess.check_output(
            [
                "ffmpeg",
                "-y",
                "-i",
                str(infile),
                "-ac",
                "1",
                "-vn",
                "-acodec",
                "pcm_s16le",
                "-ar",
                "16000",
                str(work_file),
            ],
            stderr=subprocess.STDOUT,
            # has to be done with joblib loky backend
            # see https://github.com/joblib/loky/issues/262
            # and https://github.com/kkroening/ffmpeg-python/issues/782
            stdin=subprocess.DEVNULL,
        )

        if delete_original:
            infile.unlink()
        if inplace:
            work_file.rename(outfile)

    except subprocess.CalledProcessError as e:
        raise ValueError(
            f"Command `{' '.join(e.cmd)}` failed with {e.returncode}. "
            f"output: \n{e.output.decode()}"
        )


###############################################################
# script execution


@click.command(
    "Convert a set of audio files in a given root "
    "directory to 16 KHz mono pcm_s16le wav files"
)
@click.option(
    "--dir",
    "directory_path",
    type=pathlib.Path,
    help="root directory containing (subdirectories of) audio files",
    required=True,
)
@click.option(
    "--ext",
    "audio_in_extension",
    type=str,
    help="the extension of the audio file(s) which need to be converted",
    required=True,
)
@click.option(
    "--out",
    "audio_out_extension",
    type=str,
    help="the extension of the audio file after conversion",
    default=".wav",
)
@click.option(
    "--workers",
    type=int,
    default=1,
    help="Number of CPU cores to use for converting the dataset files.",
)
@click.option(
    "--delete",
    "delete_original",
    is_flag=True,
    default=False,
    help="delete the original files after they have been converted",
)
@click.option(
    "--overwrite",
    is_flag=True,
    help="overwrite a previously existing file with the "
    "`audio_out_extension` extension",
)
def main(
    directory_path: pathlib.Path,
    audio_in_extension: str,
    audio_out_extension: str,
    workers: int,
    delete_original: bool,
    overwrite: bool,
):
    """
    Simple CLI to convert a (nested) directory of audio files to standard 16 kHZ,
    pcm_s16le, mono audio.
    """
    if audio_in_extension == audio_out_extension:
        if not overwrite:
            raise ValueError(
                f"--ext={audio_in_extension} cannot be equal to --out={audio_out_extension} "
                f"as {overwrite=}"
            )
        if delete_original:
            raise ValueError(
                f"--ext={audio_in_extension} cannot be equal to --out={audio_out_extension} "
                f"as {delete_original=}"
            )

    if len(audio_in_extension) <= 0:
        raise ValueError(f"{audio_in_extension=} must be non-empty")
    if len(audio_out_extension) <= 0:
        raise ValueError(f"{audio_out_extension=} must be non-empty")

    # find all audio files to convert
    print(
        f"recursively finding all files in `{directory_path}` with pattern "
        f"'*{audio_in_extension}'"
    )
    audio_files_to_convert = [
        f for f in directory_path.rglob(f"*{audio_in_extension}") if f.is_file()
    ]

    print(
        f"found {len(audio_files_to_convert)} files with "
        f"`*{audio_in_extension}` in {directory_path} "
    )

    if len(audio_files_to_convert) == 0:
        exit()

    # use multiple workers to call FFMPEG and convert files to .wav
    joblib.Parallel(n_jobs=workers, verbose=1)(
        [
            joblib.delayed(subprocess_convert_to_wav)(
                infile,
                infile.parent / (infile.stem + audio_out_extension),
                overwrite,
                delete_original,
            )
            for infile in audio_files_to_convert
        ]
    )


if __name__ == "__main__":
    main()
