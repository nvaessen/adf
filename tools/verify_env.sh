#! /usr/bin/env bash

# check whether paths have been set
if [[ -z "$ADF_STORAGE_DIR" ]]; then
  echo "The ADF_STORAGE_DIR environment variable is unset. Did you forget to create or fill the .env file?"
  exit 1
fi
if [[ -z "$ADF_DOWNLOAD_DIR" ]]; then
  echo "The ADF_DOWNLOAD_DIR environment variable is unset. Did you forget to create or fill the .env file?"
  exit 1
fi
if [[ -z "$ADF_WORK_DIR" ]]; then
  echo "The ADF_WORK_DIR environment variable is unset. Did you forget to create or fill the .env file?"
  exit 1
fi

# check if directories exists and are writable, otherwise try to create the directory
if [ -d "$ADF_STORAGE_DIR" ]; then
  echo "Using ADF_STORAGE_DIR=$ADF_STORAGE_DIR"
else
  mkdir -p "$ADF_STORAGE_DIR"
  echo "Using ADF_STORAGE_DIR=$ADF_STORAGE_DIR"
fi
if [ -d "$ADF_DOWNLOAD_DIR" ]; then
  echo "Using ADF_DOWNLOAD_DIR=$ADF_DOWNLOAD_DIR"
else
  mkdir -p "$ADF_DOWNLOAD_DIR"
  echo "Using ADF_DOWNLOAD_DIR=$ADF_DOWNLOAD_DIR"
fi
if [ -d "$ADF_WORK_DIR" ]; then
  echo "Using ADF_WORK_DIR=$ADF_WORK_DIR"
else
  mkdir -p "$ADF_WORK_DIR"
  echo "Using ADF_WORK_DIR=$ADF_WORK_DIR"
fi
