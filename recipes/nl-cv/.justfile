set dotenv-load

# folders for librispeech
storage-dir     := "${ADF_STORAGE_DIR}/nl-cv-v18/storage"
download-dir    := "${ADF_DOWNLOAD_DIR}/nl-cv-v18/download"
work-dir        := "${ADF_WORK_DIR}/nl-cv-v18/work"

# Performs all steps to start using the librispeech dataset
setup: download extract make-links to-wav write-json write-tar write-idx

download: verify-env
   ./download_cv_nl_v18.sh {{download-dir}}

extract: verify-env
    ./untar_cv_nl_v18.sh {{download-dir}} {{work-dir}}

make-links:
    # make folders
    mkdir -p {{work-dir}}/train
    mkdir -p {{work-dir}}/dev
    mkdir -p {{work-dir}}/test

    # make symlinks
    cat {{work-dir}}/cv-corpus-18.0-2024-06-14/nl/train.tsv | awk -F '\t' '{print $2}' | xargs -I {} ln -sf {{work-dir}}/cv-corpus-18.0-2024-06-14/nl/clips/{} {{work-dir}}/train/{}
    cat {{work-dir}}/cv-corpus-18.0-2024-06-14/nl/dev.tsv   | awk -F '\t' '{print $2}' | xargs -I {} ln -sf {{work-dir}}/cv-corpus-18.0-2024-06-14/nl/clips/{} {{work-dir}}/dev/{}
    cat {{work-dir}}/cv-corpus-18.0-2024-06-14/nl/test.tsv  | awk -F '\t' '{print $2}' | xargs -I {} ln -sf {{work-dir}}/cv-corpus-18.0-2024-06-14/nl/clips/{} {{work-dir}}/test/{}

# Converts audio to 16khz wav files
to-wav: verify-env
    ../../tools/convert/wav.py --dir {{work-dir}}/train --ext .mp3 --workers $(nproc) --overwrite
    ../../tools/convert/wav.py --dir {{work-dir}}/dev   --ext .mp3 --workers $(nproc) --overwrite
    ../../tools/convert/wav.py --dir {{work-dir}}/test  --ext .mp3 --workers $(nproc) --overwrite

write-json:
    python3 write_cv_json.py {{work-dir}}/train {{work-dir}}/cv-corpus-18.0-2024-06-14/nl/train.tsv
    python3 write_cv_json.py {{work-dir}}/dev   {{work-dir}}/cv-corpus-18.0-2024-06-14/nl/dev.tsv
    python3 write_cv_json.py {{work-dir}}/test  {{work-dir}}/cv-corpus-18.0-2024-06-14/nl/test.tsv

write-tar:
    # train
    adf tar \
    --sort-by-length --files-per-tar 5000 --name nl_cv_v18_train \
    {{work-dir}}/train {{storage-dir}}/train

    # dev
    adf tar \
    --sort-by-length --files-per-tar 5000 --name nl_cv_v18_dev \
    {{work-dir}}/dev {{storage-dir}}/dev

    # test
    adf tar \
    --sort-by-length --files-per-tar 5000 --name nl_cv_v18_test \
    {{work-dir}}/test {{storage-dir}}/test

write-idx:
    adf index {{storage-dir}}/train --subset train
    adf index {{storage-dir}}/dev   --subset dev
    adf index {{storage-dir}}/test  --subset test

[confirm]
delete-adf-dir: verify-env
    @echo "deleting {{storage-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{storage-dir}}

[confirm]
delete-download-dir: verify-env
    @echo "deleting {{download-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{download-dir}}

[confirm]
delete-extract-dir: verify-env
    @echo "deleting {{work-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{work-dir}}

# checks ADF directories in .env are defined, exists, and can be written too
@verify-env:
    ../../tools/verify_env.sh