#! /usr/bin/env bash
set -e

# default directory to save files in
DOWNLOAD_DIR=$1

# make sure all potential directories exist
mkdir -p "$DOWNLOAD_DIR"

MISSING=0

# bash function to download/verify/skip if exists
download() {
  # input
  URL=$1
  FILENAME=$2
  CHECKSUM=$3

  # check if file with checksum exists; this means we can safely skip download
  if [ ! -f "$DOWNLOAD_DIR/$FILENAME.checksum.txt" ]; then
    if [ ! -f "$DOWNLOAD_DIR/$FILENAME" ]; then
      echo "please manually download $FILENAME from https://commonvoice.mozilla.org/en/datasets (version 4) and place it in $DOWNLOAD_DIR"
      MISSING=1
      return
    fi
    echo "verifying checksum of $DOWNLOAD_DIR/$FILENAME"
    ../../tools/verify_checksum.sh "$DOWNLOAD_DIR/$FILENAME" "$CHECKSUM"
    echo "$CHECKSUM" > "$DOWNLOAD_DIR/$FILENAME.checksum.txt"
  else
    echo "$DOWNLOAD_DIR/$FILENAME exists and the checksum verified (as corresponding .checksum.txt file exists)"
  fi
}

## download files

# dutch
download manual cv-corpus-18.0-2024-06-14-nl.tar.gz 207e6cd7b199eb63758ac51b7da2fc7a9f4674014cd80aab24dc583684800232

exit $MISSING