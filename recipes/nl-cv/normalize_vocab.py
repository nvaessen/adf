import re

vocab_mapping = {
    " ": " ",
    "!": " ",
    '"': "'",
    "&": " ",
    "'": "'",
    "(": " ",
    ")": " ",
    ",": " ",
    "-": " ",
    ".": " ",
    ":": " ",
    ";": " ",
    "=": " ",
    "?": " ",
    "a": "a",
    "b": "b",
    "c": "c",
    "d": "d",
    "e": "e",
    "f": "f",
    "g": "g",
    "h": "h",
    "i": "i",
    "j": "j",
    "k": "k",
    "l": "l",
    "m": "m",
    "n": "n",
    "o": "o",
    "p": "p",
    "q": "q",
    "r": "r",
    "s": "s",
    "t": "t",
    "u": "u",
    "v": "v",
    "w": "w",
    "x": "x",
    "y": "y",
    "z": "z",
    "´": "'",
    "à": "a",
    "á": "a",
    "ä": "a",
    "ç": "c",
    "è": "e",
    "é": "e",
    "ê": "e",
    "ë": "e",
    "í": "i",
    "î": "i",
    "ï": "i",
    "ó": "o",
    "ö": "o",
    "ú": "u",
    "û": "u",
    "ü": "u",
    "ă": "a",
    "ć": "c",
    "đ": "d",
    "š": "s",
    "ţ": "t",
    "–": " ",
    "—": " ",
    "‘": "'",
    "’": "'",
    "“": "'",
    "”": "'",
    "…": " ",
}


def normalize(x: str):
    # change each non-ascii symbol as to the mapping above (and everything is lowercase)
    first_pass = "".join([vocab_mapping[c] for c in x.lower()])

    # fix whitespace due to vocabulary mapping
    second_pass = re.sub(r"\s\s+", " ", first_pass.strip())

    # remove 'quoted' parts of a sentence
    third_pass = re.sub(r"\s\'(.*?)\'\s", r" \1 ", second_pass)

    return third_pass
