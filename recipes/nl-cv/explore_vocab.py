import random
import jiwer
import pandas as pd
from normalize_vocab import normalize

df_train = pd.read_csv(
    "/home/nvaessen/scratch/adf/nl-cv-v18/work/cv-corpus-18.0-2024-06-14/nl/train.tsv",
    sep="\t",
)
df_dev = pd.read_csv(
    "/home/nvaessen/scratch/adf/nl-cv-v18/work/cv-corpus-18.0-2024-06-14/nl/train.tsv",
    sep="\t",
)
df_test = pd.read_csv(
    "/home/nvaessen/scratch/adf/nl-cv-v18/work/cv-corpus-18.0-2024-06-14/nl/train.tsv",
    sep="\t",
)

df = pd.concat([df_train, df_dev, df_test])

raw_vocab = set()
normalized_vocab = set()
for sentence in df["sentence"]:
    for c in sentence.lower():
        raw_vocab.add(c)
    for c in normalize(sentence):
        normalized_vocab.add(c)

print("raw vocab")
print(sorted(raw_vocab))
print("normalized vocab")
print(sorted(normalized_vocab))

print("\n10 random sentences\n")
seed = 1234
sentences = list(df["sentence"])

random.seed(seed)
random.shuffle(sentences)

for x in sentences[:10]:
    print(x)
    print(normalize(x))
    print()


# sentences with more than 3 edits
print("\nsentences with 5 or more edits\n")
for x in sentences:
    y = normalize(x)
    r = jiwer.process_characters(x.lower(), y)

    changes = r.substitutions + r.deletions + r.insertions
    if changes >= 5:
        print(x)
        print(y)
        print()
