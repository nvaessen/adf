#! /usr/bin/env python
########################################################################################
#
# This script supports writing the dataset into the
# ADF-format by providing a JSON label file for each audio file.
#
# Author(s): Nik Vaessen
########################################################################################

import json
import pathlib

from typing import Dict

import click
import pandas as pd
import torchaudio
import tqdm

from normalize_vocab import normalize

########################################################################################
# entrypoint of script


def get_json_of_audio_file(
    audio_file: pathlib.Path, transcripts: Dict[str, str], client_ids: Dict[str, str]
):
    key = audio_file.stem.split("_")[3]
    speaker_id = session_id = client_ids[key]
    transcription_str = normalize(transcripts[key])

    meta: torchaudio.AudioMetaData = torchaudio.info(audio_file)

    json_content = {
        "identifier": key,
        "transcription": [
            {"token": word.lower()} for word in transcription_str.split(" ")
        ],
        "speaker_id": speaker_id,
        "language_tag": "nl",
        "session_id": session_id,
        "number_of_speakers": 1,
        "number_of_channels": meta.num_channels,
        "number_of_samples": meta.num_frames,
        "sample_rate": meta.sample_rate,
    }

    return json_content


@click.command()
@click.argument("split_directory", nargs=1, type=pathlib.Path)
@click.argument("split_tsv", nargs=1, type=pathlib.Path)
def main(split_directory: pathlib.Path, split_tsv: pathlib.Path):
    # load tsv file of split
    df = pd.read_csv(split_tsv, sep="\t")

    # load all transcriptions
    transcripts = {}
    client_ids = {}

    for idx, row in df.iterrows():
        key = row["path"]
        assert key[-4:] == ".mp3"
        assert key[:16] == "common_voice_nl_"

        key = key.removeprefix("common_voice_nl_").removesuffix(".mp3")
        transcript = row["sentence"].lower()
        client_id = row["client_id"]

        transcripts[key] = transcript
        client_ids[key] = client_id

    # create a JSON file for each audio file
    print("writing JSON files...")
    audio_files = [f for f in split_directory.rglob("*.wav")]
    for audio_file in tqdm.tqdm(audio_files):
        json_file = audio_file.parent / f"{audio_file.stem}.json"
        json_content = get_json_of_audio_file(audio_file, transcripts, client_ids)

        with json_file.open("w") as f:
            json.dump(json_content, f)


if __name__ == "__main__":
    main()
