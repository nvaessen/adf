# default directory to save files in
DOWNLOAD_DIR=$1

# make sure all potential directories exist
mkdir -p "$DOWNLOAD_DIR"

# default directory to save files in
echo "Downloading the MUSAN dataset to $DOWNLOAD_DIR"

# bash function to download/verify/skip if exists
download() {
  # input
  URL=$1
  FILENAME=$2
  CHECKSUM=$3

  # check if file with checksum exists; this means we can safely skip download
  if [ ! -f "$DOWNLOAD_DIR/$FILENAME.checksum.txt" ]; then
    echo
    echo "downloading $FILENAME"
    curl -C - "$URL" --output "$DOWNLOAD_DIR/$FILENAME"
    echo "verifying checksum..."
    if ../../tools/verify_checksum.sh "$DOWNLOAD_DIR/$FILENAME" "$CHECKSUM"; then
      echo "$CHECKSUM" > "$DOWNLOAD_DIR/$FILENAME.checksum.txt"
    fi
  else
    echo "$DOWNLOAD_DIR/$FILENAME exists and the checksum verified (as corresponding .checksum.txt file exists)"
  fi
}

# download musan
download https://openslr.trmal.net/resources/17/musan.tar.gz musan.tar.gz 541bc447158e8c1ad43d38ff69a570878171d19f

