#! /usr/bin/env bash
set -e

# define paths to tar files and output location
DATA_DIR=$1
EXTRACT_DIR=$2

# make sure extraction dir exists
mkdir -p "$EXTRACT_DIR"

# utility function for extracting
extract() {
  tar_file=$1
  if [ -f "$tar_file" ]; then
    echo "extracting $tar_file"
    tar xzfv "$tar_file" -C "$EXTRACT_DIR"
  else
     echo "file $tar_file does not exist."
  fi
}

# extract the musan dataset
extract "$DATA_DIR"/musan.tar.gz