set dotenv-load

# folders for librispeech
storage-dir     := "${ADF_STORAGE_DIR}/musan/storage"
download-dir    := "${ADF_DOWNLOAD_DIR}/musan/download"
work-dir        := "${ADF_WORK_DIR}/musan/work"

# Performs all steps to start using the librispeech dataset
setup: download extract write-json write-tar write-idx

# Downloads tar.gz files from OpenSRL
download: verify-env
    ./download_musan.sh {{download-dir}}

# Extract the downloaded tar files
extract: verify-env
    ./untar_musan.sh {{download-dir}} {{work-dir}}

# Converts audio to 16khz wav files
to-wav: verify-env
    ../../tools/convert/wav.py --dir {{work-dir}} --ext .wav --workers $(nproc) --overwrite

# Compute the MFCC features of the audio
to-mfcc: verify-env
    ../../tools/convert/mfcc.py --dir {{work-dir}} --ext .flac --workers $(nproc) --overwrite

# Write a label file for each audio file in extracted dataset
write-json: verify-env
    python write_musan_json.py {{work-dir}}

# Write the data into ADF format
write-tar: verify-env \
    _write_speech _write_music _write_noise

write-idx: verify-env
    adf index {{storage-dir}}/speech --subset train
    adf index {{storage-dir}}/music  --subset train
    adf index {{storage-dir}}/noise  --subset train

_write_music: verify-env
    # music subset
    adf tar \
    --files-per-tar 5000 --name music \
    {{work-dir}}/musan/music \
    {{storage-dir}}/music

_write_speech: verify-env
    # speech subset
    adf tar \
    --files-per-tar 5000 --name speech \
    {{work-dir}}/musan/speech \
    {{storage-dir}}/speech

_write_noise: verify-env
    # noise subser
    adf tar \
    --files-per-tar 5000 --name noise \
    {{work-dir}}/musan/noise \
    {{storage-dir}}/noise

[confirm]
delete-adf-dir: verify-env
    @echo "deleting {{storage-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{storage-dir}}

[confirm]
delete-download-dir: verify-env
    @echo "deleting {{download-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{download-dir}}

[confirm]
delete-extract-dir: verify-env
    @echo "deleting {{work-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{work-dir}}

# checks ADF directories in .env are defined, exists, and can be written too
@verify-env:
    ../../tools/verify_env.sh