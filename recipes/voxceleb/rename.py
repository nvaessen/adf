import pathlib

import click


@click.command()
@click.argument("root_dir", type=pathlib.Path)
@click.option("--extension", type=click.Choice([".json", ".wav"]), default=".wav")
@click.option("--undo", is_flag=True, default=False)
def main(root_dir: pathlib.Path, undo: bool, extension: str):
    for file in root_dir.rglob(f"*{extension}"):
        speaker_id, session_id, utt_id = (
            file.parent.parent.name,
            file.parent.name,
            file.name[file.name.rfind("_") + 1 : file.name.rfind(".")],
        )

        if undo:
            new_name = f"{utt_id}{extension}"
        else:
            new_name = f"{speaker_id}_{session_id}_{utt_id}{extension}"

        file.rename(file.parent / new_name)


if __name__ == "__main__":
    main()
