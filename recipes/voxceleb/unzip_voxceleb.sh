#! /usr/bin/env bash
set -e

# define paths to tar files and output location
DATA_DIR=$1
EXTRACT_DIR=$2

# make sure extraction dir exists
mkdir -p "$EXTRACT_DIR"

# utility function for extracting
extract() {
  zip_file=$1
  name=$2

  if [ -f "$zip_file" ]; then
    echo "extracting $zip_file"
    unzip -u "$zip_file" -d "$EXTRACT_DIR"/"$name"

    # set file permission to read-only
    chmod -R a-rwx,ug+rX "$EXTRACT_DIR"/"$name"
  else
     echo "file $zip_file does not exist."
     exit 1
  fi
}

# extract the wav data from the archives
extract "$DATA_DIR"/voxceleb/wav/vox1_dev_wav.zip vox1_dev
extract "$DATA_DIR"/voxceleb/wav/vox1_test_wav.zip vox1_test

extract "$DATA_DIR"/voxceleb/wav/vox2_dev_wav.zip vox2_dev
extract "$DATA_DIR"/voxceleb/wav/vox2_test_wav.zip vox2_test

# make sure directories are writeable
find "$EXTRACT_DIR" -type d -exec chmod u+w {} +
