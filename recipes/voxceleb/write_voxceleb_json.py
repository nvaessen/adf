import json
import pathlib
from typing import Dict

import click
import joblib
import pandas as pd
import tqdm
import torchaudio


def get_ids(identifier: str):
    # speaker_id length 7
    # session_id length 11
    # utterance_id length 5 (excluding extension)
    # 2 separation tokens (_)
    assert len(identifier) == 7 + 11 + 5 + 2
    assert identifier[7] == "_"
    assert identifier[19] == "_"

    speaker_id = identifier[0:7]
    session_id = identifier[8:19]
    utt_id = identifier[20:]

    return speaker_id, session_id, utt_id


def get_json_of_audio_file(
    audio_file: pathlib.Path,
    bio_sex_of_speakers: Dict[str, str],
):
    session_id = audio_file.parent.name
    speaker_id = audio_file.parent.parent.name

    key = audio_file.stem
    spk, ses, utt = get_ids(key)
    assert spk == speaker_id
    assert ses == session_id

    meta: torchaudio.AudioMetaData = torchaudio.info(audio_file)

    json_content = {
        "identifier": key,
        "speaker_id": speaker_id,
        "session_id": session_id,
        "biological_sex": bio_sex_of_speakers[speaker_id],
        "number_of_speakers": 1,
        "number_of_channels": meta.num_channels,
        "number_of_samples": meta.num_frames,
        "sample_rate": meta.sample_rate,
    }

    return json_content


@click.command
@click.argument("extract_dir", type=pathlib.Path)
@click.argument("vox1_csv", type=pathlib.Path)
@click.argument("vox2_csv", type=pathlib.Path)
def main(
    extract_dir: pathlib.Path,
    vox1_csv: pathlib.Path,
    vox2_csv: pathlib.Path,
):
    vox1_df = pd.read_csv(vox1_csv, sep="\t")
    vox2_df = pd.read_csv(vox2_csv, sep=" ,", engine="python")

    bio_sex_of_speaker = {}

    for index, values in vox1_df.iterrows():
        bio_sex_of_speaker[values["VoxCeleb1 ID"]] = values["Gender"]

    for index, values in vox2_df.iterrows():
        bio_sex_of_speaker[values["VoxCeleb2 ID"]] = values["Gender"]

    # create a JSON file for each audio file
    print(f"finding all `*.wav` files in {extract_dir}")
    audio_files = [f for f in extract_dir.rglob("*.wav")]

    def _write(audio_file: pathlib.Path):
        json_file = audio_file.parent / f"{audio_file.stem}.json"
        json_content = get_json_of_audio_file(audio_file, bio_sex_of_speaker)

        with json_file.open("w") as f:
            json.dump(json_content, f)

    parallel = joblib.Parallel(n_jobs=-1, return_as="generator")
    generator = parallel(joblib.delayed(_write)(af) for af in audio_files)

    # show progress bar
    print("writing JSON files...")
    for _ in tqdm.tqdm(generator, total=len(audio_files)):
        pass


if __name__ == "__main__":
    main()
