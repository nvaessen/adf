#!/usr/bin/env bash

# default directory to save files in
DOWNLOAD_DIR=$1

# make sure all potential directories exist
mkdir -p "$DOWNLOAD_DIR"

# default directory to save files in
if ! command -v aria2c &> /dev/null
then
    echo "The 'aria2c' command could not be found. You can install it with 'apt-get install aria2'."
    echo "Otherwise, manually download the VoxCeleb dataset and place it in $DOWNLOAD_DIR."
    exit 1
fi

# download meta files
aria2c -d "$DOWNLOAD_DIR" --file-allocation=none --seed-time=0 --select-file=1-9 --allow-overwrite=true voxceleb.torrent

# download txt files
aria2c -d "$DOWNLOAD_DIR" --file-allocation=none --seed-time=0 --select-file=10-13 --allow-overwrite=true voxceleb.torrent

# download wav files
aria2c -d "$DOWNLOAD_DIR" --file-allocation=none --seed-time=0 --select-file=14-17 --allow-overwrite=true voxceleb.torrent
