import pathlib
import shutil

from collections import defaultdict
from typing import Dict

import click
import pandas as pd

from write_voxceleb_json import get_ids


@click.command
@click.argument("vox2_dev_folder", type=pathlib.Path)
@click.argument("vox2_dev_json", type=pathlib.Path)
@click.argument("subset_dir", type=pathlib.Path)
@click.argument("mode", type=click.Choice(["dev", "val", "960h", "1utt", "1sess"]))
@click.option("--undo", is_flag=True, default=False)
def main(
    vox2_dev_folder: pathlib.Path,
    vox2_dev_json: pathlib.Path,
    subset_dir: pathlib.Path,
    mode: str = "full",
    undo: bool = False,
):
    # load dataframe of all samples in vox2 dev set
    print(f"loading {vox2_dev_json}")
    with vox2_dev_json.open("r") as f:
        df = pd.read_json(f, orient="records", lines=True)

    assert len(df) == 1_092_009, len(df)

    # mark train/val/dev
    df = mark_dev(df)
    df = mark_val(df)

    if mode == "dev":
        print(f"train: {get_length(df, 'train'):.2f} hours")
        print(f"dev: {get_length(df, 'dev'):.2f} hours")
        print(f"val: {get_length(df, 'val'):.2f} hours")

        create_dev(vox2_dev_folder, df, subset_dir, undo)
    elif mode == "val":
        print(f"train: {get_length(df, 'train'):.2f} hours")
        print(f"dev: {get_length(df, 'dev'):.2f} hours")
        print(f"val: {get_length(df, 'val'):.2f} hours")

        create_val(vox2_dev_folder, df, subset_dir, undo)
    elif mode == "960h" or mode == "1utt" or mode == "1sess":
        create_train_subset(vox2_dev_folder, df, subset_dir, undo, mode)
    else:
        raise ValueError(f"unknown {mode=}")


def get_length(df, subset=None):
    if subset is not None:
        df = df.loc[df["subset"] == subset]

    frames = df["number_of_samples"].sum()
    hours = frames / 16_000 / 3600

    return hours


def mark_dev(df: pd.DataFrame):
    # 47 men
    # 47 women
    # hold out 94 out of 5994 so 5900 speakers remain
    dev_speakers = []

    for sex, df_sex in df.groupby("biological_sex"):
        speaker_ids = sorted(df_sex["speaker_id"].unique().tolist())
        dev_speakers.extend(speaker_ids[0:47])

    assert len(dev_speakers) == 94

    df["subset"] = "train"
    df.loc[df["speaker_id"].isin(dev_speakers), "subset"] = "dev"

    return df


def mark_val(df: pd.DataFrame):
    # mark the first utterance in the first 2 sessions for each speaker
    df_train = df.loc[df["subset"] == "train"]

    val_utterances = []
    for speaker, df_speaker in df_train.groupby("speaker_id", sort=True):
        count = 0
        for session, df_session in df_speaker.groupby("session_id", sort=True):
            if count > 1:
                break

            if len(df_session) == 1:
                continue

            df_session = df_session.sort_values("identifier")
            val_utterances.append(df_session.iloc[0]["identifier"])
            count += 1

    df.loc[df["identifier"].isin(val_utterances), "subset"] = "val"

    return df


def create_dev(
    vox2_dir: pathlib.Path, vox2_df: pd.DataFrame, subset_dir: pathlib.Path, undo: bool
):
    if undo:
        folders = [d for d in subset_dir.iterdir() if d.is_dir()]

        if len(folders) != 94:
            raise ValueError(f"expected {subset_dir} to hold 95 subdirs")

        for f in folders:
            shutil.move(f, vox2_dir / "wav" / f.name)

        subset_dir.rmdir()
    else:
        df = vox2_df.loc[vox2_df["subset"] == "dev"]
        dev_speakers = df["speaker_id"].unique().tolist()

        # move each dev speaker to new folder
        if subset_dir.exists():
            raise ValueError(f"{subset_dir} already exists")

        subset_dir.mkdir(exist_ok=False, parents=True)

        for speaker in dev_speakers:
            source_dir = vox2_dir / "wav" / speaker
            assert source_dir.exists() and len([d for d in source_dir.iterdir()]) > 0

            target_dir = subset_dir / speaker

            shutil.move(source_dir, target_dir)


def create_val(
    vox2_dir: pathlib.Path, vox2_df: pd.DataFrame, subset_dir: pathlib.Path, undo: bool
):
    df = vox2_df.loc[vox2_df["subset"] == "val"]

    # move each validation utterance to new folder
    if not undo and subset_dir.exists():
        raise ValueError(f"{subset_dir} already exists")

    for index, row in df.iterrows():
        speaker_id, session_id, utt_id = get_ids(row["identifier"])

        source_dir = vox2_dir / "wav" / speaker_id / session_id
        target_dir = subset_dir / speaker_id / session_id

        if not undo:
            target_dir.mkdir(parents=True, exist_ok=False)

        for ext in ["json", "wav"]:
            source_file = source_dir / f"{speaker_id}_{session_id}_{utt_id}.{ext}"
            target_file = target_dir / source_file.name

            if not undo:
                assert source_file.exists()
                assert not target_file.exists()
                shutil.move(source_file, target_file)

            else:
                assert target_file.exists()
                assert not source_file.exists()

                shutil.move(target_file, source_file)

        if undo:
            target_dir.rmdir()

    if undo:
        [d.rmdir() for d in subset_dir.iterdir() if d.is_dir()]
        subset_dir.rmdir()


def create_train_subset(
    vox2_dir: pathlib.Path,
    vox2_df: pd.DataFrame,
    subset_dir: pathlib.Path,
    undo: bool,
    mode: str,
):
    if undo:
        print(f"to --undo, simply `rm -r {subset_dir}`")
        return

    vox2_df = vox2_df.loc[vox2_df["subset"] == "train"]

    # create session_map
    print("creating speaker-session map")
    speaker_session_map = defaultdict(lambda: defaultdict(list))

    for speaker, speaker_df in vox2_df.groupby("speaker_id"):
        for session, session_df in speaker_df.groupby("session_id"):
            speaker_session_map[speaker][session].extend(
                sorted(
                    zip(session_df["identifier"], session_df["number_of_samples"]),
                    key=lambda tpl: tpl[0],
                )
            )

    # select utterances
    if mode == "960h":
        utterance_ids = _find_960h_utterances(speaker_session_map)
    elif mode == "1utt":
        utterance_ids = _find_1utt_per_session(speaker_session_map)
    elif mode == "1sess":
        utterance_ids = _find_1session_per_speaker(speaker_session_map)
    else:
        raise ValueError(f"unknown {mode=}")

    # symlink to all selected files
    print("creating symlink")
    for utt in utterance_ids:
        speaker_id, session_id, utt_id = get_ids(utt)

        source_dir = vox2_dir / "wav" / speaker_id / session_id
        target_dir = subset_dir / speaker_id / session_id

        target_dir.mkdir(exist_ok=True, parents=True)

        for ext in ["json", "wav"]:
            source_file = source_dir / f"{utt}.{ext}"
            target_file = target_dir / source_file.name

            target_file.symlink_to(source_file)


def _find_960h_utterances(speaker_session_map: Dict[str, Dict[str, list]]):
    # find utterances until 960h is reached
    print("selecting 960h hours of data")

    total_files = sum(
        sum(len(w) for w in v.values()) for v in speaker_session_map.values()
    )

    utterance_ids = []
    utterance_lengths_hour = 0

    while utterance_lengths_hour < 960:
        for speaker, session_map in speaker_session_map.items():
            for session, utt_list in session_map.items():
                if len(utt_list) == 0:
                    continue

                utt, length = utt_list.pop()
                length_in_hour = length / 16_000 / 3600

                utterance_ids.append(utt)
                utterance_lengths_hour += length_in_hour

    print(f"selected {len(utterance_ids)} files (out of {total_files})")

    return utterance_ids


def _find_1utt_per_session(speaker_session_map: Dict[str, Dict[str, list]]):
    # pick 1 utterance out of each session
    print("selecting 1 (median-length) utterance per session")

    total_files = sum(
        sum(len(w) for w in v.values()) for v in speaker_session_map.values()
    )

    utterance_ids = []
    utterance_length_hour = 0

    for speaker, session_map in speaker_session_map.items():
        for session_id, utterance_lst in session_map.items():
            # sort utterance by length
            utterance_lst = sorted(utterance_lst, key=lambda tpl: tpl[1])

            utt, length = utterance_lst[len(utterance_lst) // 2]

            utterance_ids.append(utt)
            utterance_length_hour += length / 16_000 / 3600

    print(f"selected {len(utterance_ids)} files (out of {total_files})")
    print(f"total length: {utterance_length_hour:.2f} hours")

    return utterance_ids


def _find_1session_per_speaker(speaker_session_map: Dict[str, Dict[str, list]]):
    # pick all utterances of 1 session (the longest) per speaker
    print("selecting all utterances of longest session per speaker")

    total_files = sum(
        sum(len(w) for w in v.values()) for v in speaker_session_map.values()
    )

    utterance_ids = []
    utterance_length_hour = 0

    for speaker, session_map in speaker_session_map.items():
        # sort by length of session
        session_lst = sorted(
            session_map.keys(), key=lambda s: sum(tpl[1] for tpl in session_map[s])
        )

        # pick utterances from session
        selected_session = session_lst[-1]
        session_utterances = session_map[selected_session]

        utterance_ids.extend([tpl[0] for tpl in session_utterances])
        utterance_length_hour += (
            sum(tpl[1] for tpl in session_utterances) / 16_000 / 3600
        )

    print(f"selected {len(utterance_ids)} files (out of {total_files})")
    print(f"total length: {utterance_length_hour:.2f} hours")

    return utterance_ids


if __name__ == "__main__":
    main()
