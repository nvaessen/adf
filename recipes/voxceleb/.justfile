set dotenv-load

# folders for librispeech
storage-dir     := "${ADF_STORAGE_DIR}/voxceleb/adf"
download-dir    := "${ADF_DOWNLOAD_DIR}/voxceleb/download"
work-dir        := "${ADF_WORK_DIR}/voxceleb/extract"

# Performs all steps to start using the librispeech dataset
setup: verify-download extract rename write-json concat-json-vox2-dev split-dev split-val write-tar write-idx
setup-subsets: link-960h link-1sess link-1utt write-tar-subset write-idx-subset

# Downloads tar.gz files from OpenSRL
download: verify-env
    ./download_voxceleb.sh {{download-dir}}

verify-download: verify-env
    ./verify_voxceleb.sh {{download-dir}}

# Extract the downloaded tar files
extract: verify-env
    ./unzip_voxceleb.sh {{download-dir}} {{work-dir}}

# rename all audio files so they have unique names
rename: verify-env
    python rename.py {{work-dir}}/vox1_test
    python rename.py {{work-dir}}/vox2_test

    python rename.py {{work-dir}}/vox2_dev
    python rename.py {{work-dir}}/vox1_dev

rename-undo: verify-env
    # rename all audio files so they have unique names
    python rename.py {{work-dir}}/vox1_test --undo
    python rename.py {{work-dir}}/vox2_test --undo

    python rename.py {{work-dir}}/vox2_dev --undo
    python rename.py {{work-dir}}/vox1_dev --undo

# Write a label file for each audio file in extracted dataset
write-json: verify-env
    python write_voxceleb_json.py {{work-dir}} {{download-dir}}/voxceleb/meta/vox1_meta.csv {{download-dir}}/voxceleb/meta/vox2_meta.csv

# concat all JSON files into a single file to easily create a dataframe for splitting dataset
concat-json-vox2-dev:
    find {{work-dir}}/vox2_dev -name "*.json" -exec jq . -c {} + > {{work-dir}}/vox2_dev.json

# create a held-out dev set
split-dev: verify-env
    python subset_gen.py {{work-dir}}/vox2_dev {{work-dir}}/vox2_dev.json {{work-dir}}/vc2_dev dev

# move dev set files back
unsplit-dev: verify-env
    python subset_gen.py {{work-dir}}/vox2_dev {{work-dir}}/vox2_dev.json {{work-dir}}/vc2_dev dev --undo

# create a validation set
split-val: verify-env
    python subset_gen.py {{work-dir}}/vox2_dev {{work-dir}}/vox2_dev.json {{work-dir}}/vc2_val val

# move val set files back
unsplit-val: verify-env
    python subset_gen.py {{work-dir}}/vox2_dev {{work-dir}}/vox2_dev.json {{work-dir}}/vc2_val val --undo

# create a 960h train subset
link-960h: verify-env
    python subset_gen.py {{work-dir}}/vox2_dev {{work-dir}}/vox2_dev.json {{work-dir}}/vc2_960h 960h

# create a dataset with only 1 utt per session (277.18 hour)
link-1utt: verify-env
    python subset_gen.py {{work-dir}}/vox2_dev {{work-dir}}/vox2_dev.json {{work-dir}}/vc2_1utt 1utt

# create a dataset with 1 session per speaker (437.12 hour)
link-1sess: verify-env
    python subset_gen.py {{work-dir}}/vox2_dev {{work-dir}}/vox2_dev.json {{work-dir}}/vc2_1sess 1sess

# delete all symlinked data subsets train subset (symlinks only)
remove-links: verify-env
    rm -r {{work-dir}}/vc2_960h
    rm -r {{work-dir}}/vc2_1utt
    rm -r {{work-dir}}/vc2_1sess

# Write the data into ADF format
write-tar: verify-env \
   _write_val _write_dev _write_train
   # write_test_o write_test_e write_test_h

write-tar-subset: verify-env \
    _write_960h _write_1sess _write_1utt

_write_train:
    adf tar \
    --sort-by-length --files-per-tar 5000 --name vc2_train \
    {{work-dir}}/vox2_dev \
    {{storage-dir}}/vc2_train

_write_val:
    adf tar \
    --sort-by-length --files-per-tar 5000 --name vc2_val \
    {{work-dir}}/vc2_val \
    {{storage-dir}}/vc2_val

_write_dev:
    adf tar \
    --sort-by-length --files-per-tar 5000 --name vc2_dev \
    {{work-dir}}/vc2_dev \
    {{storage-dir}}/vc2_dev

_write_960h:
    adf tar \
    --sort-by-length --files-per-tar 5000 --name vc2_960h \
    {{work-dir}}/vc2_960h \
    {{storage-dir}}/vc2_960h

_write_1sess:
    adf tar \
    --sort-by-length --files-per-tar 5000 --name vc2_1sess \
    {{work-dir}}/vc2_1sess \
    {{storage-dir}}/vc2_1sess

_write_1utt:
    adf tar \
    --sort-by-length --files-per-tar 5000 --name vc2_1utt \
    {{work-dir}}/vc2_1utt \
    {{storage-dir}}/vc2_1utt

write-idx: verify-env
    adf index {{storage-dir}}/vc2_val   --subset val
    adf index {{storage-dir}}/vc2_dev   --subset dev
    adf index {{storage-dir}}/vc2_train --subset train

write-idx-subset:
    adf index {{storage-dir}}/vc2_960h  --subset train
    adf index {{storage-dir}}/vc2_1sess --subset train
    adf index {{storage-dir}}/vc2_1utt  --subset train

[confirm]
delete-adf-dir: verify-env
    @echo "deleting {{storage-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{storage-dir}}

[confirm]
delete-download-dir: verify-env
    @echo "deleting {{download-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{download-dir}}

[confirm]
delete-extract-dir: verify-env
    @echo "deleting {{work-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{work-dir}}

# checks ADF directories in .env are defined, exists, and can be written too
@verify-env:
    ../../tools/verify_env.sh