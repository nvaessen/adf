#!/usr/bin/env bash
set -e

# default directory to save files in
DOWNLOAD_DIR=$1

# make sure all potential directories exist
mkdir -p "$DOWNLOAD_DIR"

# verify the checksums
verify() {
  # input
  CHECKSUM=$1
  FILENAME=$2

  # check if file with checksum exists; this means we can safely skip download
  if [ ! -f "$FILENAME.checksum.txt" ]; then
    if [ ! -f "$FILENAME" ]; then
        echo "$FILENAME does not exist"
        exit 1
    fi

    echo "verifying $FILENAME"
    ../../tools/verify_checksum.sh "$FILENAME" "$CHECKSUM"
    echo "$CHECKSUM" > "$FILENAME.checksum.txt"
  else
    echo "$DOWNLOAD_DIR/$FILENAME exists and the checksum verified (as corresponding .checksum.txt file exists)"
  fi
}

# meta
verify 09710b779bc221585f5837ef0341a1d2 "$DOWNLOAD_DIR"/voxceleb/meta/iden_split.txt
verify b9ecf7aa49d4b656aa927a8092844e4a "$DOWNLOAD_DIR"/voxceleb/meta/list_test_all.txt
verify a53e059deb562ffcfc092bf5d90d9f3a "$DOWNLOAD_DIR"/voxceleb/meta/list_test_all2.txt
verify 21c341b6b2168eea2634df0fb4b8fff1 "$DOWNLOAD_DIR"/voxceleb/meta/list_test_hard.txt
verify 857790e09d579a68eb2e339a090343c8 "$DOWNLOAD_DIR"/voxceleb/meta/list_test_hard2.txt
verify 29fc7cc1c5d59f0816dc15d6e8be60f7 "$DOWNLOAD_DIR"/voxceleb/meta/veri_test.txt
verify b73110731c9223c1461fe49cb48dddfc "$DOWNLOAD_DIR"/voxceleb/meta/veri_test2.txt
verify f5067cae5157ca27904c4b0a926e2661 "$DOWNLOAD_DIR"/voxceleb/meta/vox1_meta.csv
verify 1cb01e5f0ad19ccc39b5ad0cdac4c403 "$DOWNLOAD_DIR"/voxceleb/meta/vox2_meta.csv

# wav
verify ae63e55b951748cc486645f532ba230b "$DOWNLOAD_DIR"/voxceleb/wav/vox1_dev_wav.zip
verify 185fdc63c3c739954633d50379a3d102 "$DOWNLOAD_DIR"/voxceleb/wav/vox1_test_wav.zip
verify d9c936b030ab88387f623bf605dc68d1 "$DOWNLOAD_DIR"/voxceleb/wav/vox2_dev_wav.zip
verify d4749094cd68ea882cbef5c3ada37003 "$DOWNLOAD_DIR"/voxceleb/wav/vox2_test_wav.zip
