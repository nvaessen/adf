#!/bin/bash
#SBATCH --job-name=demucs
#SBATCH --output=array_job_%A_%a.out
#SBATCH --error=array_job_%A_%a.err
#SBATCH --array=1-156
#SBATCH --gres=gpu:1
#SBATCH --cpus-per-task=6
#SBATCH --mem=23G
#SBATCH --partition=das
#SBATCH --account=das
#SBATCH --qos=das-small
#SBATCH --time=04:00:00

# activate venv
source .venv/bin/activate

# extraction dir
EXTRACT_DIR=$(dotenv get ADF_WORK_DIR)/fma/extract

if [ ! -d "$EXTRACT_DIR" ]; then
  echo "$EXTRACT_DIR does not exist."
  exit 1
fi

# select the directory based on SLURM_ARRAY_TASK_ID
DIR=$(find "$EXTRACT_DIR"/fma_large/* -type d | sort | head -n "$SLURM_ARRAY_TASK_ID" | tail -n 1)
echo selected "$DIR" with SLURM_ARRAY_TASK_ID="$SLURM_ARRAY_TASK_ID"

# count how many files will be given to demucs
NUM_FILES=$(find "$DIR" -name "*.mp3" | wc -l)

if [ "$NUM_FILES" -eq 0 ]; then
  echo "$DIR does not contain any .mp3 files"
  exit 1
fi

# execute demucs on all files in the selected directory
find "$DIR" -name "*.mp3" -exec demucs --two-stems=vocals -o "$EXTRACT_DIR"/demucs --filename "$SLURM_ARRAY_TASK_ID/{stem}/{track}.{ext}" {} +

# count how many files were created by demucs
NUM_SPLIT=$(find "$EXTRACT_DIR"/demucs -name "$(basename "$DIR")*.wav" | wc -l)

# warn if not all files were split
NUM_SPLIT=$((NUM_SPLIT/2)) # correct for vocal/no-vocal
if [ "$NUM_FILES" -ne "$NUM_SPLIT" ]; then
  echo "$DIR" "$SLURM_ARRAY_TASK_ID" >> demucs_task_failure.txt
fi
