import os
import pathlib
import shutil
from typing import List

import click
import pandas as pd
import tqdm


def get_genre_path(
    main_dir: pathlib.Path, genre_df: pd.DataFrame, genre_id: int
) -> pathlib.Path:
    item = genre_df.loc[genre_id]

    if item.parent == 0:
        return main_dir / str(item.title)
    else:
        return get_genre_path(main_dir, genre_df, item.parent) / str(item.title)


def find_track(extraction_dir: pathlib.Path, track_id: int, removed_ids: List[int]):
    full_id = f"{track_id:06d}"
    subdir = full_id[0:3]

    track_path = extraction_dir / "fma_large" / subdir / f"{full_id}.mp3"

    if track_path.exists():
        return track_path
    else:
        if track_id in removed_ids:
            return None
        else:
            raise ValueError(f"cannot find {track_id=}, {track_path} does not exist")


@click.command()
@click.argument("extraction_dir", type=pathlib.Path)
@click.argument("removed_from_large", type=pathlib.Path)
def main(extraction_dir: pathlib.Path, removed_from_large: pathlib.Path):
    # get track and genre csv
    df_tracks = pd.read_csv(
        extraction_dir / "fma_metadata" / "tracks.csv", index_col=0, header=[0, 1]
    )
    df_genres = pd.read_csv(extraction_dir / "fma_metadata" / "genres.csv", index_col=0)

    # get IDs which were removed from large
    removed_ids = []
    with removed_from_large.open("r") as f:
        for ln in f.readlines():
            path = pathlib.Path(ln.strip().split("=>")[0].strip())
            removed_ids.append(int(path.stem))

    # loop over all tracks and make symlink in genre one or more genre directories
    main_dir = extraction_dir / "genres"

    for idx, genre_list_str in tqdm.tqdm(
        df_tracks[("track", "genres")].items(), total=len(df_tracks)
    ):
        # we skip tracks with multiple top level genres
        if pd.isna(df_tracks.loc[idx]["track", "genre_top"]):
            continue

        for genre_id in eval(genre_list_str):
            genre_directory = get_genre_path(main_dir, df_genres, genre_id)
            genre_directory.mkdir(exist_ok=True, parents=True)

            target_path = find_track(extraction_dir, idx, removed_ids)

            if target_path is None:
                # errata removed this file
                continue

            symlink_path = genre_directory / target_path.name

            if symlink_path.exists():
                symlink_path.unlink()

            os.symlink(target_path, symlink_path)


if __name__ == "__main__":
    main()
