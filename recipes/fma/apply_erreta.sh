EXTRACT_DIR=$1

# the dataset has some files which are not 30s long - we will delete them
# https://github.com/mdeff/fma/wiki

# small
sed 's/ => .*//' errors_fma_small.txt  | xargs -I {} rm -f "$EXTRACT_DIR"/{}

# medium
sed 's/ => .*//' errors_fma_medium.txt | xargs -I {} rm -f "$EXTRACT_DIR"/{}

# large
sed 's/ => .*//' errors_fma_large.txt  | xargs -I {} rm -f "$EXTRACT_DIR"/{}

# files with zero power
rm -f "$EXTRACT_DIR"/fma_*/001/001376.mp3
rm -f "$EXTRACT_DIR"/fma_*/010/010256.mp3
rm -f "$EXTRACT_DIR"/fma_*/012/012977.mp3
rm -f "$EXTRACT_DIR"/fma_*/020/020608.mp3
rm -f "$EXTRACT_DIR"/fma_*/020/020609.mp3
rm -f "$EXTRACT_DIR"/fma_*/020/020610.mp3
rm -f "$EXTRACT_DIR"/fma_*/020/020612.mp3
rm -f "$EXTRACT_DIR"/fma_*/020/020614.mp3
rm -f "$EXTRACT_DIR"/fma_*/020/020615.mp3
rm -f "$EXTRACT_DIR"/fma_*/020/020617.mp3
rm -f "$EXTRACT_DIR"/fma_*/020/020618.mp3
rm -f "$EXTRACT_DIR"/fma_*/020/020620.mp3
rm -f "$EXTRACT_DIR"/fma_*/020/020621.mp3
rm -f "$EXTRACT_DIR"/fma_*/020/020623.mp3
rm -f "$EXTRACT_DIR"/fma_*/020/020626.mp3
rm -f "$EXTRACT_DIR"/fma_*/020/020855.mp3
rm -f "$EXTRACT_DIR"/fma_*/020/020868.mp3
rm -f "$EXTRACT_DIR"/fma_*/020/020896.mp3
rm -f "$EXTRACT_DIR"/fma_*/028/028355.mp3
rm -f "$EXTRACT_DIR"/fma_*/044/044374.mp3
rm -f "$EXTRACT_DIR"/fma_*/045/045338.mp3
rm -f "$EXTRACT_DIR"/fma_*/048/048949.mp3
rm -f "$EXTRACT_DIR"/fma_*/057/057593.mp3
rm -f "$EXTRACT_DIR"/fma_*/059/059219.mp3
rm -f "$EXTRACT_DIR"/fma_*/072/072158.mp3
rm -f "$EXTRACT_DIR"/fma_*/081/081978.mp3
rm -f "$EXTRACT_DIR"/fma_*/084/084469.mp3
rm -f "$EXTRACT_DIR"/fma_*/091/091379.mp3
rm -f "$EXTRACT_DIR"/fma_*/097/097080.mp3
rm -f "$EXTRACT_DIR"/fma_*/101/101691.mp3
rm -f "$EXTRACT_DIR"/fma_*/110/110515.mp3
rm -f "$EXTRACT_DIR"/fma_*/112/112068.mp3
rm -f "$EXTRACT_DIR"/fma_*/126/126614.mp3
rm -f "$EXTRACT_DIR"/fma_*/127/127218.mp3
rm -f "$EXTRACT_DIR"/fma_*/128/128915.mp3
rm -f "$EXTRACT_DIR"/fma_*/129/129815.mp3
rm -f "$EXTRACT_DIR"/fma_*/137/137819.mp3
rm -f "$EXTRACT_DIR"/fma_*/152/152292.mp3
