#! /usr/bin/env python
########################################################################################
#
# This script supports writing the LibriSpeech dataset into the
# ADF-format by providing a JSON label file for each audio file.
#
# Author(s): Nik Vaessen
########################################################################################

import json
import pathlib

import click
import torchaudio
import tqdm

########################################################################################
# entrypoint of script


def get_json_of_audio_file(audio_file: pathlib.Path):
    key = audio_file.stem
    meta: torchaudio.AudioMetaData = torchaudio.info(audio_file)

    json_content = {
        "identifier": key,
        "number_of_channels": meta.num_channels,
        "number_of_samples": meta.num_frames,
        "sample_rate": meta.sample_rate,
    }

    return json_content


@click.command()
@click.argument("extraction_directory", nargs=1, type=pathlib.Path)
def main(extraction_directory: pathlib.Path):
    # create a JSON file for each audio file
    print(f"writing JSON files in {extraction_directory}")
    audio_files = [f for f in extraction_directory.rglob("*.wav")]

    if len(audio_files) == 0:
        raise ValueError(
            f"could not find any .wav files in {extraction_directory}. "
            f"Did you convert all mp3's to wav?"
        )

    for audio_file in tqdm.tqdm(sorted(audio_files)):
        json_file = audio_file.parent / f"{audio_file.stem}.json"
        json_content = get_json_of_audio_file(audio_file)

        with json_file.open("w") as f:
            json.dump(json_content, f)


if __name__ == "__main__":
    main()
