import pathlib
from typing import Optional

import click
import torchaudio
import dotenv
import tqdm


def find_error(
    directory: pathlib.Path,
    out: pathlib.Path,
    print_relative_to: Optional[pathlib.Path] = None,
):
    print("finding errors in", directory, "and writing to", out)

    with out.open("w") as out_f:
        all_files = sorted(directory.rglob("*.mp3"), key=lambda x: int(x.stem))

        for f in tqdm.tqdm(all_files):
            if print_relative_to is not None:
                file_path = f.relative_to(print_relative_to)
            else:
                file_path = f

            try:
                info = torchaudio.info(f)
                duration = info.num_frames / info.sample_rate
                if abs(duration - 30) > 0.05:
                    print(
                        file_path,
                        f"=> file has {duration:.02f} sec of audio",
                        flush=True,
                        file=out_f,
                    )
            except (RuntimeError, TypeError):
                print(
                    file_path,
                    f"=> invalid load (file size {f.stat().st_size})",
                    flush=True,
                    file=out_f,
                )


@click.command()
@click.argument("directory", type=pathlib.Path, required=True)
def main(directory: pathlib.Path):
    for version in [
        "small",
        "medium",
        "large",
    ]:
        find_error(
            directory / f"fma_{version}",
            pathlib.Path.cwd() / f"errors_fma_{version}.txt",
            directory,
        )


if __name__ == "__main__":
    dotenv.load_dotenv()
    main()
