set dotenv-load

# folders for librispeech
storage-dir     := "${ADF_STORAGE_DIR}/fma/storage"
download-dir    := "${ADF_DOWNLOAD_DIR}/fma/download"
work-dir        := "${ADF_WORK_DIR}/fma/work"

# Performs all steps to start using the librispeech dataset
setup: download extract apply-erreta to-wav write-json write-tar write-idx

# Downloads tar.gz files from OpenSRL
download: verify-env
    ./download_fma.sh {{download-dir}}

# Extract the downloaded tar files
extract: verify-env
    ./unzip_fma.sh {{download-dir}} {{work-dir}}

find-erreta:
    python find_error.py {{work-dir}}

apply-erreta:
    ./apply_erreta.sh {{work-dir}}

create-genre-folder: verify-env
    python3 symlink_genres.py {{work-dir}} errors_fma_large.txt

# Converts audio to 16khz wav files
to-wav: verify-env
    ../../tools/convert/wav.py --dir {{work-dir}}/fma_small  --ext .mp3 --workers $(nproc) --overwrite
    ../../tools/convert/wav.py --dir {{work-dir}}/fma_medium --ext .mp3 --workers $(nproc) --overwrite
    ../../tools/convert/wav.py --dir {{work-dir}}/fma_large  --ext .mp3 --workers $(nproc) --overwrite

# Compute the MFCC features of the audio
to-mfcc: verify-env
    ../../tools/convert/mfcc.py --dir {{work-dir}} --ext .flac --workers $(nproc) --overwrite

# Write a label file for each audio file in extracted dataset
write-json: verify-env
    python write_fma_json.py {{work-dir}}/fma_small
    python write_fma_json.py {{work-dir}}/fma_medium
    python write_fma_json.py {{work-dir}}/fma_large

# Write the data into ADF format
write-tar: verify-env \
    _write_small _write_medium _write_large

write-idx: verify-env
    adf index {{storage-dir}}/fma_small  --subset train
    adf index {{storage-dir}}/fma_medium --subset train
    adf index {{storage-dir}}/fma_large  --subset train

_write_small: verify-env
    # small - ~ 67 hours
    adf tar \
    --files-per-tar 5000 --name fma_small \
    {{work-dir}}/fma_small \
    {{storage-dir}}/fma_small

_write_medium: verify-env
    # medium - ~200 hours
    adf tar \
    --files-per-tar 5000 --name fma_medium \
    {{work-dir}}/fma_medium \
    {{storage-dir}}/fma_medium

_write_large: verify-env
    # large - ~888 hours
    adf tar \
    --files-per-tar 5000 --name fma_large \
    {{work-dir}}/fma_large \
    {{storage-dir}}/fma_large

# logic to make dataset for instrumental/vocal fragments
split-vocal-instrumental: verify-env
    find {{work-dir}}/fma_large -name "*.mp3" -exec demucs --two-stems=vocals -o {{work-dir}}/demucs --filename "{stem}/{track}.{ext}" {} +

# logic to make dataset for instrumental/vocal fragments
split-vocal-instrumental-slurm: verify-env
    cd ../../ && sbatch recipes/fma/remove_vocals.sh

demucs-to-wav: verify-env
   ../../tools/convert/wav.py --dir {{work-dir}}/demucs  --ext .wav --out .wav --workers $(nproc) --overwrite

demucs-write-json: verify-env
    python write_fma_json.py {{work-dir}}/demucs

demucs-write-tar: verify-env
    # vocal - ~888 hours
    adf tar \
    --files-per-tar 5000 --name fma_vocal \
    {{work-dir}}/demucs/htdemucs/vocals/ \
    {{storage-dir}}/fma_vocals

    # instrumental - ~888 hours
    adf tar \
    --files-per-tar 5000 --name fma_instrumental \
    {{work-dir}}/demucs/htdemucs/no_vocals/ \
    {{storage-dir}}/fma_instrumental

demucs-write-idx: verify-env
    adf index {{storage-dir}}/fma_vocals  --subset train
    adf index {{storage-dir}}/fma_instrumental  --subset train

[confirm]
delete-adf-dir: verify-env
    @echo "deleting {{storage-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{storage-dir}}

[confirm]
delete-download-dir: verify-env
    @echo "deleting {{download-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{download-dir}}

[confirm]
delete-extract-dir: verify-env
    @echo "deleting {{work-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{work-dir}}

# checks ADF directories in .env are defined, exists, and can be written too
@verify-env:
    ../../tools/verify_env.sh