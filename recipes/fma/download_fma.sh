# default directory to save files in
DOWNLOAD_DIR=$1

# make sure all potential directories exist
mkdir -p "$DOWNLOAD_DIR"

# default directory to save files in
echo "Downloading the FMA dataset to $DOWNLOAD_DIR"

# bash function to download/verify/skip if exists
download() {
  # input
  URL=$1
  FILENAME=$2
  CHECKSUM=$3

  # check if file with checksum exists; this means we can safely skip download
  if [ ! -f "$DOWNLOAD_DIR/$FILENAME.checksum.txt" ]; then
    echo
    echo "downloading $FILENAME"
    curl -C - "$URL" --output "$DOWNLOAD_DIR/$FILENAME"
    echo "verifying checksum..."
    if ../../tools/verify_checksum.sh "$DOWNLOAD_DIR/$FILENAME" "$CHECKSUM"; then
      echo "$CHECKSUM" > "$DOWNLOAD_DIR/$FILENAME.checksum.txt"
    fi
  else
    echo "$DOWNLOAD_DIR/$FILENAME exists and the checksum verified (as corresponding .checksum.txt file exists)"
  fi
}

# download small
download https://os.unil.cloud.switch.ch/fma/fma_small.zip fma_small.zip ade154f733639d52e35e32f5593efe5be76c6d70

# download medium
download https://os.unil.cloud.switch.ch/fma/fma_medium.zip fma_medium.zip c67b69ea232021025fca9231fc1c7c1a063ab50b

# download large
download https://os.unil.cloud.switch.ch/fma/fma_large.zip fma_large.zip 497109f4dd721066b5ce5e5f250ec604dc78939e

# download metadata
download https://os.unil.cloud.switch.ch/fma/fma_metadata.zip fma_metadata.zip f0df49ffe5f2a6008d7dc83c6915b31835dfe733