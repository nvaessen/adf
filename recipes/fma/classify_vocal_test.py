# some comments:
# majority is vocal
# some are partially vocal, e.g., first 15 seconds are instrumental
# loudness is not normalized

import os
import pandas as pd
import pathlib
import torchaudio
import torch
import whisper

csv = pathlib.Path("fma_vocal_inst_label.csv")
root_fma_work_dir = pathlib.Path(
    os.environ.get("DATA_DIR", "/mnt/data/adf/work/fma/work")
)

full = root_fma_work_dir / "fma_small/000"
vocal = root_fma_work_dir / "demucs/htdemucs/1/vocals"
instrumental = root_fma_work_dir / "demucs/htdemucs/1/no_vocals"

df = pd.read_csv(csv)
whisper_model = whisper.load_model(
    "large-v2", download_root=os.environ.get("DOWNLOAD_DIR", None)
)


def whisper_s2t(t):
    # load audio and pad/trim it to fit 30 seconds
    audio = whisper.pad_or_trim(t)

    # make log-Mel spectrogram and move to the same device as the model
    mel = whisper.log_mel_spectrogram(audio, n_mels=whisper_model.dims.n_mels).to(
        whisper_model.device
    )

    options = whisper.DecodingOptions()
    result = whisper.decode(whisper_model, mel, options)

    for r in result:
        print(r.no_speech_prob)
        print(r.text)


def power(t):
    chunk_powers = []
    for i in [0, 16_000 * 5, 16_000 * 10, 16_000 * 15, 16_000 * 20, 16_000 * 25]:
        chunk = t[:, i : i + 16_000 * 5]
        chunk_powers.append(chunk.pow(2).sum())

    return torch.tensor(chunk_powers)


ratio_vocal = []
ratio_non_vocal = []

for idx, (song_id, vocal_label, comment) in df.iterrows():
    song_fn = f"{song_id:06d}.wav"
    is_vocal = vocal_label == 1

    if is_vocal:
        continue

    full_tensor, _ = torchaudio.load(full / song_fn)
    vocal_tensor, _ = torchaudio.load(vocal / song_fn)
    instrumental_tensor, _ = torchaudio.load(instrumental / song_fn)

    power_full = power(full_tensor)
    power_vocal = power(vocal_tensor)
    power_instrumental = power(instrumental_tensor)

    ratio = power_vocal / power_full
    avg_ratio = ratio.mean().item()

    print(f"{song_fn=} {is_vocal=}")
    print("full", power(full_tensor), str(full / song_fn))
    print("vocal", power(vocal_tensor), str(vocal / song_fn))
    print("instrumental", power(instrumental_tensor), str(instrumental / song_fn))
    print(f"{ratio=}")

    if is_vocal:
        ratio_vocal.append(ratio)
    else:
        ratio_non_vocal.append(ratio)

print(ratio_non_vocal)
print(ratio_vocal)
