# some comments:
# majority is vocal
# some are partially vocal, e.g., first 15 seconds are instrumental
# loudness is not normalized

import pathlib
import torchaudio
import torch

full = pathlib.Path("/mnt/hdd/adf/fma/work/fma_large/000")
vocal = pathlib.Path("/mnt/hdd/adf/fma/work/demucs/htdemucs/1/vocals")
instrumental = pathlib.Path("/mnt/hdd/adf/fma/work/demucs/htdemucs/1/no_vocals")

vocal_folder = pathlib.Path("/mnt/hdd/adf/fma/work/fma_large_vocal")
instrumental_folder = pathlib.Path("/mnt/hdd/adf/fma/work/fma_large_instrumental")

vocal_folder.mkdir()
instrumental_folder.mkdir()


def power(t):
    print(t.mean())
    print(t.min())
    print(t.max())

    chunk_powers = []
    for i in [0, 16_000 * 5, 16_000 * 10, 16_000 * 15, 16_000 * 20, 16_000 * 25]:
        start = i
        end = start + 16_000 * 5
        chunk = t[:, start:end]
        chunk_powers.append(chunk.pow(2).sum())

    return torch.tensor(chunk_powers)


vocal_files = []
instrumental_files = []

for wav_file in sorted(full.glob("*.wav")):
    vocal_file = vocal / wav_file.name
    instrumental_file = instrumental / wav_file.name

    full_tensor, _ = torchaudio.load(wav_file)
    vocal_tensor, _ = torchaudio.load(vocal / wav_file.name)
    instrumental_tensor, _ = torchaudio.load(instrumental / wav_file.name)

    print(wav_file)
    print("full")
    power_full = power(full_tensor)
    print(power_full)

    print("vocal")
    power_vocal = power(vocal_tensor)
    print(power_vocal)

    print("instrumental")
    power_instrumental = power(instrumental_tensor)
    print(power_instrumental)

    ratio = power_vocal / power_full
    max_ratio = ratio.max().item()
    print(ratio)
    print(f"{max_ratio=}")

    if max_ratio > 0.01:
        # vocal
        vocal_files.append(wav_file)
    else:
        # non vocal
        instrumental_files.append(wav_file)

# create symlinks
for af in vocal_files:
    (vocal_folder / af.name).symlink_to(af)
for af in instrumental_files:
    (instrumental_folder / af.name).symlink_to(af)
