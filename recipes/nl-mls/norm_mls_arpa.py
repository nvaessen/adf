from pathlib import Path
from normalize_vocab import normalize

arpa_path = Path("/home/nvaessen/Downloads/mls_lm_dutch/5-gram_lm.arpa")
out_path = Path("/home/nvaessen/Downloads/mls_lm_dutch/5-gram_lm_norm.arpa")

with arpa_path.open("r") as f:
    lines = [ln.strip() for ln in f.readlines()]

with out_path.open("w") as fh:
    num_words = None

    for ln in lines:
        tab_count = ln.count("\t")
        if tab_count == 0:
            print(ln, file=fh)
            if ln.startswith("\\") and ln.endswith("-grams:"):
                num_words = int(ln[1:].split("-")[0])
        else:
            data = ln.split("\t")

            text = data[1]
            words = text.split(" ")
            assert len(words) == num_words, f"ln='{ln}' words='{words}' {num_words=}"

            normalized_words = []
            for w in words:
                if w in ["<unk>", "<s>", "</s>"]:
                    normalized_words.append(w)
                else:
                    w_norm = normalize(w)
                    if w_norm.count(" ") > 0:
                        w_norm = w_norm.replace(" ", "")

                    normalized_words.append(w_norm)

            norm_text = " ".join(normalized_words)
            assert norm_text.count(" ") == text.count(
                " "
            ), f"ln='{ln}' text='{text}' norm_text='{norm_text}'"

            data[1] = norm_text
            print("\t".join(data), file=fh)
