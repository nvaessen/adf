import pathlib
import pandas as pd

path = pathlib.Path("/home/nik/adf/nl-mls/work/mls_dutch_opus/metainfo.txt")

def load_df():
    rows = []
    for line in path.open('r').readlines()[1:]:
        values = [v.strip() for v in line.strip().split("|")]
        assert len(values) == 7
        speaker, gender, partition, minutes, book_id, title, chapter = values

        rows.append({
            'speaker': speaker,
            'gender': gender,
            "partition": partition,
            "minutes": float(minutes),
            "book_id": book_id,
            "title": title,
            "chapter": chapter
        })

    df = pd.DataFrame(rows)
    return df

def _info(speaker: int, df_train: pd.DataFrame):
    spk = str(speaker)
    subdf = df_train.loc[df_train['speaker'] == spk]

    duration_min = round(subdf['minutes'].sum())
    num_books = len(subdf['book_id'].unique())
    duration_hour = round(duration_min/60)

    print(f"{spk=} {num_books=} {duration_min=} {duration_hour=}")

    book_id_map = {}
    for book_id, bookdf in subdf.groupby("book_id"):
        book_duration = bookdf['minutes'].sum()
        book_id_map[book_id] = book_duration

    for k, v in sorted(book_id_map.items(), key= lambda tpl: tpl[1]):
        print(f"\tbook={k} duration={round(v)} min, {round(v/60)} hours")

def main():
    df = load_df()
    df_train = df.loc[df['partition'] == "train"]
    print(len(df_train))

    speakers = df_train['speaker'].unique()
    print(len(speakers))
    print(df_train['minutes'].sum() / 60)

    speaker_time_map = {}
    speaker_gender_map = {}
    for speaker, subdf in df_train.groupby("speaker"):
        speaker_time = subdf['minutes'].sum()
        speaker_gender = subdf['gender'].unique()
        assert len(speaker_gender) == 1

        speaker_time_map[speaker] = speaker_time
        speaker_gender_map[speaker] = speaker_gender.item()

    male_speakers = sorted([int(k) for k, v in speaker_gender_map.items() if v == "M"])
    female_speakers = sorted([int(k) for k, v in speaker_gender_map.items() if v == "F"])

    print("male speakers")
    for spk in male_speakers:
        _info(spk, df_train)

    print("female speakers")
    for spk in female_speakers:
        _info(spk, df_train)

if __name__ == '__main__':
    main()