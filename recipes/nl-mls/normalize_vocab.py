import re

vocab_mapping = {
    " ": " ",
    "'": "'",
    "-": " ",
    "a": "a",
    "b": "b",
    "c": "c",
    "d": "d",
    "e": "e",
    "f": "f",
    "g": "g",
    "h": "h",
    "i": "i",
    "j": "j",
    "k": "k",
    "l": "l",
    "m": "m",
    "n": "n",
    "o": "o",
    "p": "p",
    "q": "q",
    "r": "r",
    "s": "s",
    "t": "t",
    "u": "u",
    "v": "v",
    "w": "w",
    "x": "x",
    "y": "y",
    "z": "z",
    "à": "a",
    "â": "a",
    "ä": "a",
    "æ": "ae",
    "ç": "c",
    "è": "e",
    "é": "e",
    "ê": "e",
    "ë": "e",
    "î": "i",
    "ï": "i",
    "ó": "o",
    "ô": "o",
    "ö": "o",
    "ù": "u",
    "û": "u",
    "ü": "u",
    "œ": "oe",
    "ß": "ss",
}


def normalize(x: str):
    # change each non-ascii symbol as to the mapping above (and everything is lowercase)
    first_pass = "".join([vocab_mapping[c] for c in x.lower()])

    # fix whitespace due to vocabulary mapping
    second_pass = re.sub(r"\s\s+", " ", first_pass.strip())

    # remove 'quoted' parts of a sentence
    third_pass = re.sub(r"\s\'(.*?)\'\s", r" \1 ", second_pass)

    return third_pass
