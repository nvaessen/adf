#! /usr/bin/env bash
set -e

# default directory to save files in
DOWNLOAD_DIR=$1
echo "Starting to download LibriSpeech dataset to $DOWNLOAD_DIR"

# make sure all potential directories exist
mkdir -p "$DOWNLOAD_DIR"

# bash function to download/verify/skip if exists
download() {
  # input
  URL=$1
  FILENAME=$2
  CHECKSUM=$3

  # check if file with checksum exists; this means we can safely skip download
  if [ ! -f "$DOWNLOAD_DIR/$FILENAME.checksum.txt" ]; then
    echo
    echo "downloading $FILENAME"
    curl -C - "$URL" --output "$DOWNLOAD_DIR/$FILENAME"
    echo "verifying checksum..."
    ../../tools/verify_checksum.sh "$DOWNLOAD_DIR/$FILENAME" "$CHECKSUM"
    echo "$CHECKSUM" > "$DOWNLOAD_DIR/$FILENAME.checksum.txt"
  else
    echo "$DOWNLOAD_DIR/$FILENAME exists and the checksum verified (as corresponding .checksum.txt file exists)"
  fi
}

## download files
download https://dl.fbaipublicfiles.com/mls/mls_dutch_opus.tar.gz mls_dutch_opus.tar.gz 96658c55ef85993a56cf2efbf6f83f57

echo 'Download complete (or files were already present)'