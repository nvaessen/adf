set dotenv-load

# folders for librispeech
storage-dir     := "${ADF_STORAGE_DIR}/nl-mls/storage"
download-dir    := "${ADF_DOWNLOAD_DIR}/nl-mls/download"
work-dir        := "${ADF_WORK_DIR}/nl-mls/work"

# Performs all steps to start using the librispeech dataset
setup: download extract to-wav write-json write-tar write-idx

# Downloads tar.gz files from OpenSRL
download: verify-env
    ./download_mls_nl.sh {{download-dir}}

# Extract the downloaded tar files
extract: verify-env
    ./untar_mls_nl.sh {{download-dir}} {{work-dir}}

# Converts audio to 16khz wav files
to-wav: verify-env
    ../../tools/convert/wav.py --dir {{work-dir}} --ext .opus --workers $(nproc) --overwrite

# Write a label file for each audio file in extracted dataset
write-json: verify-env
    ./write_mls_json.py {{work-dir}}/mls_dutch_opus/train {{work-dir}}/mls_dutch_opus/metainfo.txt
    ./write_mls_json.py {{work-dir}}/mls_dutch_opus/dev   {{work-dir}}/mls_dutch_opus/metainfo.txt
    ./write_mls_json.py {{work-dir}}/mls_dutch_opus/test  {{work-dir}}/mls_dutch_opus/metainfo.txt

# Write the librispeech into ADF format
write-tar: verify-env
    # train
    adf tar \
    --sort-by-length --files-per-tar 5000 --name mls_nl_train \
    {{work-dir}}/mls_dutch_opus/train {{storage-dir}}/train
    # dev
    adf tar \
    --sort-by-length --files-per-tar 5000 --name mls_nl_dev \
    {{work-dir}}/mls_dutch_opus/dev {{storage-dir}}/dev
    # test
    adf tar \
    --sort-by-length --files-per-tar 5000 --name mls_nl_test \
    {{work-dir}}/mls_dutch_opus/test {{storage-dir}}/test

make-light:
    # create subset
    python3 light_subset.py \
    {{work-dir}}/mls_dutch_opus/train/audio/ \
    {{work-dir}}/mls_dutch_opus/train-light/audio \
    {{work-dir}}/mls_dutch_opus/metainfo.txt

    cp {{work-dir}}/mls_dutch_opus/train/*.txt {{work-dir}}/mls_dutch_opus/train-light/

    # convert to wav
    ../../tools/convert/wav.py --dir {{work-dir}}/mls_dutch_opus/train-light/audio --ext .opus --workers $(nproc) --overwrite

    # write json
    ./write_mls_json.py {{work-dir}}/mls_dutch_opus/train-light {{work-dir}}/mls_dutch_opus/metainfo.txt

    # make tar
    adf tar \
    --sort-by-length --frames-per-tar 5h --name mls_nl_train_light \
    {{work-dir}}/mls_dutch_opus/train-light {{storage-dir}}/train-light

    # write index
    adf index {{storage-dir}}/train-light --subset train

write-idx: verify-env
    adf index {{storage-dir}}/train --subset train
    adf index {{storage-dir}}/dev   --subset dev
    adf index {{storage-dir}}/test  --subset test

[confirm]
delete-adf-dir: verify-env
    @echo "deleting {{storage-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{storage-dir}}

[confirm]
delete-download-dir: verify-env
    @echo "deleting {{download-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{download-dir}}

[confirm]
delete-extract-dir: verify-env
    @echo "deleting {{work-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{work-dir}}

# checks ADF directories in .env are defined, exists, and can be written too
@verify-env:
    ../../tools/verify_env.sh