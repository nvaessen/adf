import pathlib
from normalize_vocab import normalize

vocab_counts = pathlib.Path("/home/nvaessen/Downloads/mls_lm_dutch/vocab_counts.txt")
out = "lexicon.txt"

with vocab_counts.open("r") as f:
    words = set()
    for ln in f.readlines():
        # print(ln, end="")
        word = ln.strip().split("\t")[0]
        if "œ" in word or "ß" in word:
            pass
        else:
            word = normalize(word)

            if word.count(" ") > 0:
                word = word.replace(" ", "")

            words.add(word)

with open(out, "w") as fh:
    for word in sorted(words):
        print(f"{word}\t{' '.join(word+'|')}", file=fh)
