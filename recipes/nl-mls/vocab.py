import pandas as pd

split_tsv_train = "/mnt/hdd/adf/nl-mls/work/mls_dutch_opus/train/transcripts.txt"
split_tsv_dev = "/mnt/hdd/adf/nl-mls/work/mls_dutch_opus/dev/transcripts.txt"
split_tsv_test = "/mnt/hdd/adf/nl-mls/work/mls_dutch_opus/test/transcripts.txt"

df_train = pd.read_csv(
    split_tsv_train, sep="\t", header=None, names=["key", "sentence"]
)
df_dev = pd.read_csv(split_tsv_dev, sep="\t", header=None, names=["key", "sentence"])
df_test = pd.read_csv(split_tsv_test, sep="\t", header=None, names=["key", "sentence"])

df = pd.concat([df_train, df_test, df_dev])
# print(df.columns)
# print(df.to_string())

vocab = set()
weird = [
    # "à",
    # "â",
    # "ä",
    # "æ",
    "ç",
    # "è",
    # "é",
    # "ê",
    # "ë",
    # "î",
    # "ï",
    # "ó",
    # "ô",
    # "ö",
    # "ù",
    # "û",
    # "ü",
]
for i, row in df.iterrows():
    if any([c in weird for c in row["sentence"]]):
        print(row["key"], row["sentence"])

    for c in row["sentence"].lower():
        vocab.add(c)

print(sorted(list(vocab)))
