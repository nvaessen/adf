import pathlib
import shutil
from collections import defaultdict

import click
import pandas as pd

def load_train_df(meta_file: pathlib.Path):
    rows = []
    for line in meta_file.open('r').readlines()[1:]:
        values = [v.strip() for v in line.strip().split("|")]
        assert len(values) == 7
        speaker, gender, partition, minutes, book_id, title, chapter = values

        rows.append({
            'speaker': speaker,
            'gender': gender,
            "partition": partition,
            "minutes": float(minutes),
            "book_id": book_id,
            "title": title,
            "chapter": chapter
        })

    df = pd.DataFrame([r for r in rows if r['partition'] == 'train'])
    return df

def get_books_with_duration(df):
    data = []
    for book, book_df in df.groupby("book_id"):
        duration = book_df['minutes'].sum()
        data.append((book, duration))

    return sorted(data, key=lambda tpl: tpl[1])

def select_bookid_for_each_speaker(df):
    speaker_book_map = defaultdict(list)
    total_duration = 0
    for spk, spk_df in df.groupby("speaker"):
        books = get_books_with_duration(spk_df)
        num = len(books)

        # select median duration book
        if num <= 3:
            sel = books[-1]
        else:
            median_idx = num // 2
            sel = books[median_idx]

        selected_duration = sel[1]
        total_duration += selected_duration
        # print(spk, num, round(selected_duration), 'min')
        speaker_book_map[spk] = sel[0]

    # print("total:", round(total_duration), 'min', round(total_duration/60), 'h')
    return speaker_book_map

@click.command()
@click.argument("train_dir", type=pathlib.Path)
@click.argument("dest_dir", type=pathlib.Path)
@click.argument("meta_file", type=pathlib.Path)
def main(train_dir: pathlib.Path, dest_dir: pathlib.Path, meta_file: pathlib.Path):
    df = load_train_df(meta_file)
    spk_book_map = select_bookid_for_each_speaker(df)

    for spk, book_id in spk_book_map.items():
        old_dir = train_dir / f"{spk}" / f"{book_id}"
        new_dir = dest_dir / f"{spk}" / f"{book_id}"
        assert old_dir.exists()
        new_dir.mkdir(parents=True, exist_ok=True)

        for f in old_dir.iterdir():
            assert f.is_file()
            shutil.copy(f, new_dir)




if __name__ == '__main__':
    main()