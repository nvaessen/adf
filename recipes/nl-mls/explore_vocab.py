import pathlib
import random
import jiwer
import pandas as pd

from normalize_vocab import normalize
from adf.util import get_adf_working_dir

import dotenv

dotenv.load_dotenv(pathlib.Path(__file__).parent.parent.parent / ".env")
work_dir = get_adf_working_dir()

df_train = pd.read_csv(
    work_dir / "nl-mls/work/mls_dutch_opus/train/transcripts.txt",
    sep="\t",
    names=["key", "sentence"],
)
df_dev = pd.read_csv(
    work_dir / "nl-mls/work/mls_dutch_opus/dev/transcripts.txt",
    sep="\t",
    names=["key", "sentence"],
)
df_test = pd.read_csv(
    work_dir / "nl-mls/work/mls_dutch_opus/test/transcripts.txt",
    sep="\t",
    names=["key", "sentence"],
)

df = pd.concat([df_train, df_dev, df_test])

raw_vocab = set()
normalized_vocab = set()
for sentence in df["sentence"]:
    for c in sentence.lower():
        raw_vocab.add(c)
    for c in normalize(sentence):
        normalized_vocab.add(c)

print("raw vocab")
print(sorted(raw_vocab))
print("normalized vocab")
print(sorted(normalized_vocab))

print("\n10 random sentences\n")
seed = 1234
sentences = list(df["sentence"])

random.seed(seed)
random.shuffle(sentences)

for x in sentences[:10]:
    print(x)
    print(normalize(x))
    print()

# sentences with more than 3 edits
print("\nsentences with 6 or more edits\n")
for x in sentences:
    y = normalize(x)
    r = jiwer.process_characters(x.lower(), y)

    changes = r.substitutions + r.deletions + r.insertions
    if changes >= 6:
        print(x)
        print(y)
        print()
