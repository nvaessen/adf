#! /usr/bin/env python
########################################################################################
#
# This script supports writing the LibriSpeech dataset into the
# ADF-format by providing a JSON label file for each audio file.
#
# Author(s): Nik Vaessen
########################################################################################

import json
import pathlib

from typing import Dict

import click
import torchaudio
import tqdm

########################################################################################
# entrypoint of script


def get_json_of_audio_file(
    audio_file: pathlib.Path,
    transcripts: Dict[str, str],
    bio_sex_of_speakers: Dict[str, str],
):
    key = audio_file.stem
    speaker_id, chapter_id, utt_id = audio_file.stem.split("-")
    transcription_str = transcripts[key]

    meta: torchaudio.AudioMetaData = torchaudio.info(audio_file)

    json_content = {
        "identifier": key,
        "transcription": [
            {"token": word.lower()} for word in transcription_str.split(" ")
        ],
        "speaker_id": speaker_id,
        "language_tag": "en",
        "session_id": chapter_id,
        "biological_sex": bio_sex_of_speakers[speaker_id],
        "number_of_speakers": 1,
        "number_of_channels": meta.num_channels,
        "number_of_samples": meta.num_frames,
        "sample_rate": meta.sample_rate,
    }

    return json_content


@click.command()
@click.argument("extraction_directory", nargs=1, type=pathlib.Path)
@click.option("--allow-duplicate", is_flag=True, default=False)
def main(extraction_directory: pathlib.Path, allow_duplicate: bool):
    # load biological sex information
    speakers_file = None
    for f in extraction_directory.rglob("SPEAKERS.TXT"):
        speakers_file = f
        break

    assert speakers_file is not None
    print(f"loading {speakers_file}")

    bio_sex_of_speakers = {}
    with speakers_file.open("r") as f:
        for line in f:
            if line.startswith(";"):
                continue

            if line.count("|") < 4:
                continue

            line = line.split("|")

            speaker_id = line[0].strip()
            sex = line[1].strip()

            bio_sex_of_speakers[speaker_id] = sex

    # load all transcriptions
    transcripts = {}
    print("collecting transcripts...")
    transcripts_files = [f for f in extraction_directory.rglob("*.trans.txt")]
    for transcript_file in tqdm.tqdm(transcripts_files):
        with transcript_file.open("r") as f:
            for line in f.readlines():
                line = line.strip().split(" ")
                key = line[0]
                transcript = " ".join(line[1:])

                if not allow_duplicate:
                    assert key not in transcripts

                transcripts[key] = transcript

    # create a JSON file for each audio file
    print("writing JSON files...")
    audio_files = [f for f in extraction_directory.rglob("*.flac")]
    for audio_file in tqdm.tqdm(audio_files):
        json_file = audio_file.parent / f"{audio_file.stem}.json"
        json_content = get_json_of_audio_file(
            audio_file, transcripts, bio_sex_of_speakers
        )

        with json_file.open("w") as f:
            json.dump(json_content, f)


if __name__ == "__main__":
    main()
