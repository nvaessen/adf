#! /usr/bin/env python
########################################################################################
#
# This script supports writing the LibriSpeech dataset into the
# ADF-format by creating a validation split.
#
# Author(s): Nik Vaessen
########################################################################################

import json
import pathlib
import shutil

import click

########################################################################################
# entrypoint of script


def move_one_utterance_per_session(train_dir: pathlib.Path, val_dir: pathlib.Path):
    total_speakers = 0
    total_sessions = 0
    total_files = 0
    total_duration = 0

    sessions_skipped = 0
    files_moved = 0
    duration_moved = 0

    for speaker_dir in train_dir.iterdir():
        if not speaker_dir.is_dir():
            raise ValueError(f"{train_dir=} should not contain any files")

        total_speakers += 1

        for session_dir in speaker_dir.iterdir():
            if not session_dir.is_dir():
                raise ValueError(f"{speaker_dir} should not contain any files")

            total_sessions += 1

            utterances = [
                f.stem
                for f in session_dir.iterdir()
                if f.is_file() and f.suffix in [".flac"]
            ]

            if len(utterances) == 0:
                raise ValueError(f"session {session_dir} does not have any utterances")

            if len(utterances) == 1:
                sessions_skipped += 1
                continue

            # sort utterances by length
            file_lengths = {
                utt: json.load((session_dir / f"{utt}.json").open("r"))[
                    "number_of_samples"
                ]
                for utt in utterances
            }
            utterances = sorted(utterances, key=lambda f: file_lengths[f])
            utterance_to_move = utterances[len(utterances) // 2]

            target_dir = val_dir / speaker_dir.name / session_dir.name
            target_dir.mkdir(exist_ok=True, parents=True)

            files_moved += 1
            total_files += len(utterances)

            total_duration += sum(file_lengths.values())
            duration_moved += file_lengths[utterance_to_move]

            for f in session_dir.glob(f"{utterance_to_move}.*"):
                shutil.move(f, target_dir)

    print(
        f"skipped {sessions_skipped} out of {total_sessions} sessions "
        f"due to only having a single utterance"
    )
    print(
        f"moved {files_moved} out of {total_files} files "
        f"({files_moved/total_files*100:.2f}% of the data)"
    )
    print(
        f"moved files had a length of {duration_moved/16_000:.2f} sec "
        f"out of a total of {total_duration/16_000:.2f} sec "
        f"({duration_moved/total_duration*100:.2f}% of the dataset duration)\n"
    )

    return files_moved, duration_moved, total_files, total_duration


def undo_split(train_dir: pathlib.Path, val_dir: pathlib.Path):
    if not val_dir.exists():
        print(f"validation directory {val_dir} does not exist; nothing to undo")
        return

    for speaker_dir in val_dir.iterdir():
        if not speaker_dir.is_dir():
            raise ValueError(f"{val_dir} should not contain any files")

        for session_dir in speaker_dir.iterdir():
            if not session_dir.is_dir():
                raise ValueError(f"{speaker_dir} should not contain any files")

            for f in session_dir.iterdir():
                if f.is_dir():
                    raise ValueError(
                        f"{session_dir} should not contain any directories"
                    )

                target = train_dir / speaker_dir.name / session_dir.name
                shutil.move(f, target)

            session_dir.rmdir()

        speaker_dir.rmdir()

    val_dir.rmdir()


@click.command()
@click.argument("extraction_directory", nargs=1, type=pathlib.Path)
@click.option("--undo", is_flag=True, default=False)
def main(extraction_directory: pathlib.Path, undo: bool):
    train_clean_100 = extraction_directory / "LibriSpeech" / "train-clean-100"
    val_clean_100 = extraction_directory / "LibriSpeech" / "val-clean-100"

    train_clean_360 = extraction_directory / "LibriSpeech" / "train-clean-360"
    val_clean_360 = extraction_directory / "LibriSpeech" / "val-clean-360"

    train_other_500 = extraction_directory / "LibriSpeech" / "train-other-500"
    val_other_500 = extraction_directory / "LibriSpeech" / "val-other-500"

    if undo:
        print("restoring train-clean-100")
        undo_split(train_clean_100, val_clean_100)

        print("restoring train-clean-360")
        undo_split(train_clean_360, val_clean_360)

        print("restoring train-other-500")
        undo_split(train_other_500, val_other_500)

    else:
        all_files = 0
        all_filed_moved = 0

        all_duration = 0
        all_duration_moved = 0

        print("splitting train-clean-100")
        (
            files_moved,
            duration_moved,
            total_files,
            total_duration,
        ) = move_one_utterance_per_session(train_clean_100, val_clean_100)

        all_files += total_files
        all_filed_moved += files_moved
        all_duration += total_duration
        all_duration_moved += duration_moved

        print("splitting train-clean-360")
        (
            files_moved,
            duration_moved,
            total_files,
            total_duration,
        ) = move_one_utterance_per_session(train_clean_360, val_clean_360)

        all_files += total_files
        all_filed_moved += files_moved
        all_duration += total_duration
        all_duration_moved += duration_moved

        print("splitting train-other-500")
        (
            files_moved,
            duration_moved,
            total_files,
            total_duration,
        ) = move_one_utterance_per_session(train_other_500, val_other_500)

        all_files += total_files
        all_filed_moved += files_moved
        all_duration += total_duration
        all_duration_moved += duration_moved

        print(
            f"validation split contains {all_filed_moved}/{all_files} files "
            f"= {all_filed_moved/all_files*100:.2f}% of the data files"
        )
        all_duration = all_duration / 16_000 / 60 / 60
        all_duration_moved = all_duration_moved / 16_000 / 60 / 60
        print(
            f"validation split contains {all_duration_moved:.1f}/{all_duration:.1f}"
            f" hours = {all_duration_moved/all_duration*100:.2f}% of the data duration"
        )


if __name__ == "__main__":
    main()
