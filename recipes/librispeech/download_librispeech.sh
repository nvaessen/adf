#! /usr/bin/env bash
set -e

# default directory to save files in
DOWNLOAD_DIR=$1
echo "Starting to download LibriSpeech dataset to $DOWNLOAD_DIR"

# make sure all potential directories exist
mkdir -p "$DOWNLOAD_DIR"

# bash function to download/verify/skip if exists
download() {
  # input
  URL=$1
  FILENAME=$2
  CHECKSUM=$3

  # check if file with checksum exists; this means we can safely skip download
  if [ ! -f "$DOWNLOAD_DIR/$FILENAME.checksum.txt" ]; then
    echo
    echo "downloading $FILENAME"
    curl -C - "$URL" --output "$DOWNLOAD_DIR/$FILENAME"
    echo "verifying checksum..."
    ../../tools/verify_checksum.sh "$DOWNLOAD_DIR/$FILENAME" "$CHECKSUM"
    echo "$CHECKSUM" > "$DOWNLOAD_DIR/$FILENAME.checksum.txt"
  else
    echo "$DOWNLOAD_DIR/$FILENAME exists and the checksum verified (as corresponding .checksum.txt file exists)"
  fi
}

## download files

# dev
download https://openslr.trmal.net/resources/12/dev-clean.tar.gz dev-clean.tar.gz 42e2234ba48799c1f50f24a7926300a1
download https://openslr.trmal.net/resources/12/dev-other.tar.gz dev-other.tar.gz c8d0bcc9cca99d4f8b62fcc847357931

# test
download https://openslr.trmal.net/resources/12/test-clean.tar.gz test-clean.tar.gz 32fa31d27d2e1cad72775fee3f4849a9
download https://openslr.trmal.net/resources/12/test-other.tar.gz test-other.tar.gz fb5a50374b501bb3bac4815ee91d3135

# train
download https://openslr.trmal.net/resources/12/train-clean-100.tar.gz train-clean-100.tar.gz 2a93770f6d5c6c964bc36631d331a522
download https://openslr.trmal.net/resources/12/train-clean-360.tar.gz train-clean-360.tar.gz c0e676e450a7ff2f54aeade5171606fa
download https://openslr.trmal.net/resources/12/train-other-500.tar.gz train-other-500.tar.gz d1a0fd59409feb2c614ce4d30c387708

echo 'Download complete (or files were already present)'