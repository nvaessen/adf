#! /usr/bin/env bash
set -e

# define paths to tar files and output location
DATA_DIR=$1
EXTRACT_DIR=$2

# make sure all potential directories exist
mkdir -p "$DATA_DIR" "$EXTRACT_DIR"

# utility function for extracting
extract() {
  tar_file=$1
  if [ -f "$tar_file" ]; then
    echo "extracting $tar_file"
    tar xzfv "$tar_file" -C "$EXTRACT_DIR"
  else
     echo "file $tar_file does not exist."
  fi
}

# dev data
extract "$DATA_DIR"/dev-clean.tar.gz
extract "$DATA_DIR"/dev-other.tar.gz

# test data
extract "$DATA_DIR"/test-clean.tar.gz
extract "$DATA_DIR"/test-other.tar.gz

# train data
extract "$DATA_DIR"/train-clean-100.tar.gz
extract "$DATA_DIR"/train-clean-360.tar.gz
extract "$DATA_DIR"/train-other-500.tar.gz
