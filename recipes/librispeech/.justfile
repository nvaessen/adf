set dotenv-load

# folders for librispeech
storage-dir     := "${ADF_STORAGE_DIR}/librispeech/storage"
download-dir    := "${ADF_DOWNLOAD_DIR}/librispeech/download"
work-dir        := "${ADF_WORK_DIR}/librispeech/work"

# Performs all steps to start using the librispeech dataset
setup: download extract to-wav write-json split-val write-tar write-idx

# Downloads tar.gz files from OpenSRL
download: verify-env
    ./download_librispeech.sh {{download-dir}}

# Extract the downloaded tar files
extract: verify-env
    ./untar_librispeech_archives.sh {{download-dir}} {{work-dir}}

# Converts audio to 16khz wav files
to-wav: verify-env
    ../../tools/convert/wav.py --dir {{work-dir}} --ext .flac --workers $(nproc) --overwrite

# Compute the MFCC features of the audio
to-mfcc: verify-env
    ../../tools/convert/mfcc.py --dir {{work-dir}} --ext .flac --workers $(nproc) --overwrite

# Write a label file for each audio file in extracted dataset
write-json: verify-env
    ./write_librispeech_json.py {{work-dir}}

split-val: verify-env
    ./create_librispeech_validation_set.py {{work-dir}}

unsplit-val: verify-env
    ./create_librispeech_validation_set.py {{work-dir}} --undo


# Write the librispeech into ADF format
write-tar: verify-env \
    _write_dev_clean _write_dev_other \
    _write_test_clean _write_test_other \
    _write_val_clean_100 _write_val_clean_360 _write_val_other_500 \
    _write_train_clean_100 _write_train_clean_360 _write_train_other_500

write-idx: verify-env
    adf index {{storage-dir}}/train-clean-100 --subset train
    adf index {{storage-dir}}/train-clean-360 --subset train
    adf index {{storage-dir}}/train-other-500 --subset train

    adf index {{storage-dir}}/val-clean-100 --subset val
    adf index {{storage-dir}}/val-clean-360 --subset val
    adf index {{storage-dir}}/val-other-500 --subset val

    adf index {{storage-dir}}/dev-clean --subset dev
    adf index {{storage-dir}}/dev-other --subset dev

    adf index {{storage-dir}}/test-clean --subset test
    adf index {{storage-dir}}/test-other --subset test

_write_train_clean_100: verify-env
    # train - clean 100
    adf tar \
    --sort-by-length --frames-per-tar 5h --name ls_train_clean_100 \
    {{work-dir}}/LibriSpeech/train-clean-100 \
    {{storage-dir}}/train-clean-100

_write_train_clean_360: verify-env
    # train - clean 360
    adf tar \
    --sort-by-length --frames-per-tar 5h --name ls_train_clean_360 \
    {{work-dir}}/LibriSpeech/train-clean-360 \
    {{storage-dir}}/train-clean-360

_write_train_other_500: verify-env
    # train - other 500
    adf tar \
    --sort-by-length --frames-per-tar 5h --name ls_train_other_500 \
    {{work-dir}}/LibriSpeech/train-other-500 \
    {{storage-dir}}/train-other-500

_write_val_clean_100: verify-env
    # val - clean 100
    adf tar \
    --sort-by-length --frames-per-tar -1 --name ls_val_clean_100 \
    {{work-dir}}/LibriSpeech/val-clean-100 \
    {{storage-dir}}/val-clean-100

_write_val_clean_360: verify-env
    # val - clean 360
    adf tar \
    --sort-by-length --frames-per-tar -1 --name ls_val_clean_360 \
    {{work-dir}}/LibriSpeech/val-clean-360 \
    {{storage-dir}}/val-clean-360

_write_val_other_500: verify-env
    # val - other 500
    adf tar \
    --sort-by-length --frames-per-tar -1 --name ls_val_other_500 \
    {{work-dir}}/LibriSpeech/val-other-500 \
    {{storage-dir}}/val-other-500

_write_dev_clean: verify-env
    # dev clean
    adf tar \
    --sort-by-length --frames-per-tar -1 --name ls_dev_clean \
    {{work-dir}}/LibriSpeech/dev-clean \
    {{storage-dir}}/dev-clean

_write_dev_other: verify-env
    # dev other
    adf tar \
    --sort-by-length --frames-per-tar -1 --name ls_dev_other \
    {{work-dir}}/LibriSpeech/dev-other \
    {{storage-dir}}/dev-other

_write_test_clean: verify-env
    # test clean
    adf tar \
    --sort-by-length --frames-per-tar -1 --name ls_test_clean \
    {{work-dir}}/LibriSpeech/test-clean \
    {{storage-dir}}/test-clean

_write_test_other: verify-env
    # test other
    adf tar \
    --sort-by-length --frames-per-tar -1 --name ls_test_other \
    {{work-dir}}/LibriSpeech/test-other \
    {{storage-dir}}/test-other

[confirm]
delete-adf-dir: verify-env
    @echo "deleting {{storage-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{storage-dir}}

[confirm]
delete-download-dir: verify-env
    @echo "deleting {{download-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{download-dir}}

[confirm]
delete-extract-dir: verify-env
    @echo "deleting {{work-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{work-dir}}

# checks ADF directories in .env are defined, exists, and can be written too
@verify-env:
    ../../tools/verify_env.sh