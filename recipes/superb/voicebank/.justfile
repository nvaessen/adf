set dotenv-load

# folders for librispeech
storage-dir     := "${ADF_STORAGE_DIR}/voicebank/adf"
download-dir    := "${ADF_DOWNLOAD_DIR}/voicebank/download"
work-dir        := "${ADF_WORK_DIR}/voicebank/extract"

# Performs all steps to start using the librispeech dataset
setup: download extract

download: verify-env
    rm -rf {{download-dir}}/tmp
    mkdir -p {{download-dir}}/tmp
    ./download_voicebank.sh {{download-dir}}
    rm -rf {{download-dir}}/tmp

extract: verify-env
    ./unzip_voicebank.sh {{download-dir}} {{work-dir}}

[confirm]
delete-adf-dir: verify-env
    @echo "deleting {{storage-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{storage-dir}}

[confirm]
delete-download-dir: verify-env
    @echo "deleting {{download-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{download-dir}}

[confirm]
delete-extract-dir: verify-env
    @echo "deleting {{work-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{work-dir}}

# checks ADF directories in .env are defined, exists, and can be written too
@verify-env:
    ../../../tools/verify_env.sh