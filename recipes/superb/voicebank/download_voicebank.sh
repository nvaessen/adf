#! /usr/bin/env bash
set -e

# default directory to save files in
DOWNLOAD_DIR=$1

# make sure all potential directories exist
mkdir -p "$DOWNLOAD_DIR"

MISSING=0

# bash function to download/verify/skip if exists
download() {
  # input
  URL=$1
  FILENAME=$2
  CHECKSUM=$3

  # check if file with checksum exists; this means we can safely skip download
  if [ ! -f "$DOWNLOAD_DIR/$FILENAME.checksum.txt" ]; then
    if [ ! -f "$DOWNLOAD_DIR/$FILENAME" ]; then
      python3 prepare_voicebank.py "$DOWNLOAD_DIR" "$DOWNLOAD_DIR"/tmp
    fi
    echo "verifying checksum of $DOWNLOAD_DIR/$FILENAME"
    ../../../tools/verify_checksum.sh "$DOWNLOAD_DIR/$FILENAME" "$CHECKSUM"
    echo "$CHECKSUM" > "$DOWNLOAD_DIR/$FILENAME.checksum.txt"
  else
    echo "$DOWNLOAD_DIR/$FILENAME exists and the checksum verified (as corresponding .checksum.txt file exists)"
  fi
}

## download files
download with-python-script noisy-vctk-16k.zip 6ea2e7278401681d79ef540a83cf9e87

exit $MISSING