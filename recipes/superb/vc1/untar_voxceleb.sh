#! /usr/bin/env bash
set -e

# define paths to tar files and output location
DATA_DIR=$1
EXTRACT_DIR=$2

# make sure all potential directories exist
mkdir -p "$DATA_DIR" "$EXTRACT_DIR"

# utility function for extracting
extract() {
  tar_file=$1
  name=$2
  if [ -f "$tar_file" ]; then
    echo "extracting $tar_file"
    mkdir -p "$EXTRACT_DIR/$name"
    tar xfv "$tar_file" -C "$EXTRACT_DIR/$name"
  else
     echo "file $tar_file does not exist."
  fi
}

# extract the wav data from the archives
extract "$DATA_DIR"/vox1_test_wav.tar test
extract "$DATA_DIR"/vox1_dev_wav.tar dev
