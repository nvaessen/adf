#! /usr/bin/env bash
set -e

# default directory to save files in
DOWNLOAD_DIR=$1

# make sure all potential directories exist
mkdir -p "$DOWNLOAD_DIR"

# bash function to download/verify/skip if exist
MISSING=0

download() {
  # input
  URL=$1
  FILENAME=$2
  CHECKSUM=$3

  # check if file with checksum exists; this means we can safely skip download
  if [ ! -f "$DOWNLOAD_DIR/$FILENAME.checksum.txt" ]; then
    if [ ! -e "$DOWNLOAD_DIR/$FILENAME" ]; then
      echo "please manually download $FILENAME and place it in $DOWNLOAD_DIR"
      MISSING=1
      return
    fi
    echo "verifying checksum of $DOWNLOAD_DIR/$FILENAME"
    ../../../tools/verify_checksum.sh "$DOWNLOAD_DIR/$FILENAME" "$CHECKSUM"
    echo "$CHECKSUM" > "$DOWNLOAD_DIR/$FILENAME.checksum.txt"

    # remove compression
    echo "decompressing $DOWNLOAD_DIR/$FILENAME"
    bsdtar -cf "$DOWNLOAD_DIR/$(basename -s .zip "$FILENAME")".tar @"$DOWNLOAD_DIR/$FILENAME"

  else
    echo "$DOWNLOAD_DIR/$FILENAME exists and the checksum verified (as corresponding .checksum.txt file exists)"
  fi
}

## download files
download manual vox1_test_wav.zip 185fdc63c3c739954633d50379a3d102
download manual vox1_dev_wav.zip ae63e55b951748cc486645f532ba230b

exit $MISSING