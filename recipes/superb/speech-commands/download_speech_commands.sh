#! /usr/bin/env bash
set -e

# default directory to save files in
DOWNLOAD_DIR=$1

# make sure all potential directories exist
mkdir -p "$DOWNLOAD_DIR"

# bash function to download/verify/skip if exists
download() {
  # input
  URL=$1
  FILENAME=$2
  CHECKSUM=$3

  # check if file with checksum exists; this means we can safely skip download
  if [ ! -f "$DOWNLOAD_DIR/$FILENAME.checksum.txt" ]; then
    echo
    echo "downloading $FILENAME"
    curl -C - "$URL" --output "$DOWNLOAD_DIR/$FILENAME"
    echo "verifying checksum..."
    ../../../tools/verify_checksum.sh "$DOWNLOAD_DIR/$FILENAME" "$CHECKSUM"
    echo "$CHECKSUM" > "$DOWNLOAD_DIR/$FILENAME.checksum.txt"
  else
    echo "$DOWNLOAD_DIR/$FILENAME exists and the checksum verified (as corresponding .checksum.txt file exists)"
  fi
}

## download files
download http://download.tensorflow.org/data/speech_commands_v0.01.tar.gz speech_commands_v0.01.tar.gz 3cd23799cb2bbdec517f1cc028f8d43c
download http://download.tensorflow.org/data/speech_commands_test_set_v0.01.tar.gz speech_commands_test_set_v0.01.tar.gz ca549636d7a545273cdcc6a73f7086a3
