#! /usr/bin/env bash
set -e

# define paths to tar files and output location
DATA_DIR=$1
EXTRACT_DIR_ROOT=$2

# make sure all potential directories exist
mkdir -p "$DATA_DIR" "$EXTRACT_DIR_ROOT"

# utility function for extracting
extract() {
  tar_file=$1
  if [ -f "$tar_file" ]; then
    echo "extracting $tar_file"
    tar xzfv "$tar_file" -C "$EXTRACT_DIR"
  else
     echo "file $tar_file does not exist."
  fi
}

# train
EXTRACT_DIR=$EXTRACT_DIR_ROOT/train
mkdir -p $EXTRACT_DIR
extract "$DATA_DIR"/speech_commands_v0.01.tar.gz

# test
EXTRACT_DIR=$EXTRACT_DIR_ROOT/test
mkdir -p $EXTRACT_DIR
extract "$DATA_DIR"/speech_commands_test_set_v0.01.tar.gz
