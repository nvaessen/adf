#! /usr/bin/env bash
set -e

# define paths to tar files and output location
DATA_DIR=$1
EXTRACT_DIR=$2

# make sure extraction dir exists
mkdir -p "$EXTRACT_DIR"

# utility function for extracting
extract() {
  zip_file=$1

  if [ -f "$zip_file" ]; then
    echo "extracting $zip_file"
    unzip -u "$zip_file" -d "$EXTRACT_DIR"
  else
     echo "file $zip_file does not exist."
     exit 1
  fi
}

# extract the wav data from the archives
extract "$DATA_DIR"/wham_noise.zip
