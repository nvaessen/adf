set dotenv-load

# folders for librispeech
download-dir-ls := "${ADF_DOWNLOAD_DIR}/librimix/download"
download-dir    := "${ADF_DOWNLOAD_DIR}/librimix/download"
work-dir        := "${ADF_WORK_DIR}/librimix/extract"
storage-dir     := "${ADF_STORAGE_DIR}/librimix/adf"

# Performs all steps to start using the librispeech dataset
setup: download extract augment-train-noise create-sd-mix prepare-diarization create-ss-mix

# Downloads tar.gz files from OpenSRL
download: verify-env
    ./download_librispeech.sh {{download-dir-ls}}
    ./download_wham.sh {{download-dir}}

# Extract the downloaded tar files
extract: verify-env
    ./untar_librispeech_archives.sh {{download-dir-ls}} {{work-dir}}
    ./unzip_wham.sh {{download-dir}} {{work-dir}}

augment-train-noise:
    python augment_train_noise.py --wham_dir {{work-dir}}/wham_noise

create-sd-mix:
    python create_librimix_from_metadata.py \
      --librispeech_dir {{work-dir}}/LibriSpeech \
      --wham_dir {{work-dir}}/wham_noise \
      --metadata_dir metadata/Libri2Mix \
      --librimix_outdir {{work-dir}}/mix-sd \
      --n_src 2 \
      --freqs 16k \
      --modes max \
      --types mix_both

prepare-diarization:
    python prepare_diarization.py \
      --source_dir {{work-dir}}/mix-sd/Libri2Mix/wav16k/max/metadata \
      --target_dir {{work-dir}}/mix-sd/Libri2Mix/wav16k/max \
      --rttm_dir metadata/LibriSpeech

create-ss-mix:
    python create_librimix_from_metadata.py \
      --librispeech_dir {{work-dir}}/LibriSpeech \
      --wham_dir {{work-dir}}/wham_noise \
      --metadata_dir metadata/Libri2Mix \
      --librimix_outdir {{work-dir}}/mix-ss \
      --n_src 2 \
      --freqs 16k \
      --modes min \
      --types mix_clean

[confirm]
delete-adf-dir: verify-env
    @echo "deleting {{storage-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{storage-dir}}

[confirm]
delete-download-dir: verify-env
    @echo "deleting {{download-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{download-dir}}

[confirm]
delete-extract-dir: verify-env
    @echo "deleting {{work-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{work-dir}}

# checks ADF directories in .env are defined, exists, and can be written too
@verify-env:
    ../../../tools/verify_env.sh