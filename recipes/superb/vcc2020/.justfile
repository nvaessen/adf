set dotenv-load

# folders for librispeech
storage-dir     := "${ADF_STORAGE_DIR}/vcc2020/adf"
download-dir    := "${ADF_DOWNLOAD_DIR}/vcc2020/download"
work-dir        := "${ADF_WORK_DIR}/vcc2020/extract"

# Performs all steps to start using the librispeech dataset
setup: download extract

download: verify-env
    ./download_vcc2020.sh {{download-dir}}/data
    ./download_vocoder.sh {{download-dir}}/models

extract:
    rsync -aP {{download-dir}}/ {{work-dir}}

[confirm]
delete-adf-dir: verify-env
    @echo "deleting {{storage-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{storage-dir}}

[confirm]
delete-download-dir: verify-env
    @echo "deleting {{download-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{download-dir}}

[confirm]
delete-extract-dir: verify-env
    @echo "deleting {{work-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{work-dir}}

# checks ADF directories in .env are defined, exists, and can be written too
@verify-env:
    ../../../tools/verify_env.sh