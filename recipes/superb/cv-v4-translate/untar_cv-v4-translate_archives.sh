#! /usr/bin/env bash
set -e

# define paths to tar files and output location
DATA_DIR=$1
EXTRACT_DIR=$2

# make sure all potential directories exist
mkdir -p "$DATA_DIR" "$EXTRACT_DIR"

# utility function for extracting
extract() {
  tar_file=$1
  if [ -f "$tar_file" ]; then
    echo "extracting $tar_file"
    tar xzfv "$tar_file" -C "$EXTRACT_DIR"
  else
     echo "file $tar_file does not exist."
  fi
}

# english
extract "$DATA_DIR"/en.tar.gz

# delete bad data
rm $EXTRACT_DIR/clips/common_voice_en_19411965.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411966.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411967.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411968.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411974.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411975.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411976.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411977.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411978.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411984.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411985.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411986.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411987.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411988.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411989.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411990.mp3
rm $EXTRACT_DIR/clips/common_voice_en_19411992.mp3