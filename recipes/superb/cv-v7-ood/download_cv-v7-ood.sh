#! /usr/bin/env bash
set -e

# default directory to save files in
DOWNLOAD_DIR=$1

# make sure all potential directories exist
mkdir -p "$DOWNLOAD_DIR"

MISSING=0

# bash function to download/verify/skip if exists
download() {
  # input
  URL=$1
  FILENAME=$2
  CHECKSUM=$3

  # check if file with checksum exists; this means we can safely skip download
  if [ ! -f "$DOWNLOAD_DIR/$FILENAME.checksum.txt" ]; then
    if [ ! -f "$DOWNLOAD_DIR/$FILENAME" ]; then
      echo "please manually download $FILENAME from https://commonvoice.mozilla.org/en/datasets (version 7) and place it in $DOWNLOAD_DIR"
      MISSING=1
      return
    fi
    echo "verifying checksum of $DOWNLOAD_DIR/$FILENAME"
    ../../../tools/verify_checksum.sh "$DOWNLOAD_DIR/$FILENAME" "$CHECKSUM"
    echo "$CHECKSUM" > "$DOWNLOAD_DIR/$FILENAME.checksum.txt"

    # remove .gz
    echo "decompressing $DOWNLOAD_DIR/$FILENAME"
    pigz -kd -p "$(nproc)" "$DOWNLOAD_DIR/$FILENAME"

  else
    echo "$DOWNLOAD_DIR/$FILENAME exists and the checksum verified (as corresponding .checksum.txt file exists)"
  fi
}

## download files

# spanish
download manual cv-corpus-7.0-2021-07-21-es.tar.gz 22915f54e1dd4c86155ffc34bf30a1ddb8eb98c04f35b467d23f7e669d0f8301

# arabic
download manual cv-corpus-7.0-2021-07-21-ar.tar.gz 5fdfae60464ec9298eccaf2b567494133c2592ef9de596145f1d308439fc840a

# chinese
download manual cv-corpus-7.0-2021-07-21-zh-CN.tar.gz e95b9b8320234dc8310fbf58d9068d3dd51bec7c234e878b522daee7eb19a726

exit $MISSING