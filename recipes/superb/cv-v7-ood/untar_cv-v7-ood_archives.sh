#! /usr/bin/env bash
set -e

# define paths to tar files and output location
DATA_DIR=$1
EXTRACT_DIR=$2
MODE=$3

# make sure all potential directories exist
mkdir -p "$DATA_DIR" "$EXTRACT_DIR"

# utility function for extracting
extract() {
  tar_file=$1
  if [ -f "$tar_file" ]; then
    echo "extracting $tar_file"
    tar xfv "$tar_file" -C "$EXTRACT_DIR"
  else
     echo "file $tar_file does not exist."
  fi
}

# spanish
if [ "$MODE" == "es" ]; then
    extract "$DATA_DIR"/cv-corpus-7.0-2021-07-21-es.tar
fi

# arabic
if [ "$MODE" == "ar" ]; then
    extract "$DATA_DIR"/cv-corpus-7.0-2021-07-21-ar.tar
fi

# chinese
if [ "$MODE" == "zh" ]; then
    extract "$DATA_DIR"/cv-corpus-7.0-2021-07-21-zh-CN.tar
fi