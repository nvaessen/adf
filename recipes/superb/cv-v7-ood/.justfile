set dotenv-load

# folders for librispeech
storage-dir     := "${ADF_STORAGE_DIR}/cv-v7-ood/adf"
download-dir    := "${ADF_DOWNLOAD_DIR}/cv-v7-ood/download"
work-dir        := "${ADF_WORK_DIR}/cv-v7-ood/extract"

# Performs all steps to start using the librispeech dataset
setup: download extract

download: verify-env
    ./download_cv-v7-ood.sh {{download-dir}}

extract: extract-es extract-ar extract-zh-CN

extract-es: verify-env
    ./untar_cv-v7-ood_archives.sh {{download-dir}} {{work-dir}} es

extract-ar: verify-env
    ./untar_cv-v7-ood_archives.sh {{download-dir}} {{work-dir}} ar

extract-zh-CN: verify-env
    ./untar_cv-v7-ood_archives.sh {{download-dir}} {{work-dir}} zh

[confirm]
delete-adf-dir: verify-env
    @echo "deleting {{storage-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{storage-dir}}

[confirm]
delete-download-dir: verify-env
    @echo "deleting {{download-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{download-dir}}

[confirm]
delete-extract-dir: verify-env
    @echo "deleting {{work-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{work-dir}}

# checks ADF directories in .env are defined, exists, and can be written too
@verify-env:
    ../../../tools/verify_env.sh