#! /usr/bin/env bash
set -e

# default directory to save files in
DOWNLOAD_DIR=$1

# make sure all potential directories exist
mkdir -p "$DOWNLOAD_DIR"

MISSING=0

# bash function to download/verify/skip if exists
download() {
  # input
  URL=$1
  FILENAME=$2
  CHECKSUM=$3

  # check if file with checksum exists; this means we can safely skip download
  if [ ! -f "$DOWNLOAD_DIR/$FILENAME.checksum.txt" ]; then
    if [ ! -f "$DOWNLOAD_DIR/$FILENAME" ]; then
      echo "please manually download $FILENAME from https://sail.usc.edu/iemocap/iemocap_release.htm and place it in $DOWNLOAD_DIR"
      MISSING=1
      return
    fi
    echo "verifying checksum of $DOWNLOAD_DIR/$FILENAME"
    ../../../tools/verify_checksum.sh "$DOWNLOAD_DIR/$FILENAME" "$CHECKSUM"
    echo "$CHECKSUM" > "$DOWNLOAD_DIR/$FILENAME.checksum.txt"

    # remove .gz
    echo "decompressing $DOWNLOAD_DIR/$FILENAME"
    pigz -kd -p "$(nproc)" "$DOWNLOAD_DIR/$FILENAME"

  else
    echo "$DOWNLOAD_DIR/$FILENAME exists and the checksum verified (as corresponding .checksum.txt file exists)"
  fi
}

## download files
download manual IEMOCAP_full_release_withoutVideos.tar.gz 6f2e6ecb72dd45a9af6b801ee999cb5a

exit $MISSING