#! /usr/bin/env bash
set -e

# define paths to tar files and output location
DATA_DIR=$1
EXTRACT_DIR=$2

# make sure all potential directories exist
mkdir -p "$DATA_DIR" "$EXTRACT_DIR"

# utility function for extracting
extract() {
  tar_file=$1
  if [ -f "$tar_file" ]; then
    echo "extracting $tar_file"
    tar xfv "$tar_file" -C "$EXTRACT_DIR"
  else
     echo "file $tar_file does not exist."
  fi
}

# iemocap
extract "$DATA_DIR"/IEMOCAP_full_release_withoutVideos.tar
