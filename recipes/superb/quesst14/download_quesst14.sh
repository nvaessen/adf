#! /usr/bin/env bash
set -e

# default directory to save files in
DOWNLOAD_DIR=$1

# make sure all potential directories exist
mkdir -p "$DOWNLOAD_DIR"

# bash function to download/verify/skip if exists
download() {
  # input
  URL=$1
  FILENAME=$2
  CHECKSUM=$3

  # check if file with checksum exists; this means we can safely skip download
  if [ ! -f "$DOWNLOAD_DIR/$FILENAME.checksum.txt" ]; then
    echo
    echo "downloading $FILENAME"
    curl -C - "$URL" --output "$DOWNLOAD_DIR/$FILENAME"
    echo "verifying checksum..."
    ../../../tools/verify_checksum.sh "$DOWNLOAD_DIR/$FILENAME" "$CHECKSUM"
    echo "$CHECKSUM" > "$DOWNLOAD_DIR/$FILENAME.checksum.txt"
  else
    echo "$DOWNLOAD_DIR/$FILENAME exists and the checksum verified (as corresponding .checksum.txt file exists)"
  fi
}

## download files
download https://speech.fit.vutbr.cz/files/quesst14Database.tgz quesst14Database.tgz 3bfd993bedeee9a45816bf77549ca288
