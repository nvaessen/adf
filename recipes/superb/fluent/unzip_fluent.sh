#! /usr/bin/env bash
set -e

# define paths to tar files and output location
DATA_DIR=$1
EXTRACT_DIR=$2

# make sure all potential directories exist
mkdir -p "$DATA_DIR" "$EXTRACT_DIR"

# utility function for extracting
extract() {
  zip_file=$1
  name=$2

  if [ -f "$zip_file" ]; then
    echo "extracting $zip_file"
    unzip -u "$zip_file" -d "$EXTRACT_DIR"/"$name"
  else
     echo "file $zip_file does not exist."
     exit 1
  fi
}

# fluent
extract "$DATA_DIR"/fluent.zip
