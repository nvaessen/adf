set dotenv-load

# folders for librispeech
storage-dir     := "${ADF_STORAGE_DIR}/SBCSAE/adf"
download-dir    := "${ADF_DOWNLOAD_DIR}/SBCSAE/download"
work-dir        := "${ADF_WORK_DIR}/SBCSAE/extract"

# Performs all steps to start using the librispeech dataset
setup: download preprocess

download: verify-env
    #!/usr/bin/env bash
    if just verify 1> /dev/null 2> /dev/null; then
        echo 'dataset already downloaded'
        exit 0
    fi

    ./download_SBCSAE.sh {{download-dir}}
    rmdir {{download-dir}}/mp3/0wav

verify: verify-env
    #!/usr/bin/env bash
    CHECKSUM_FILE=$PWD/checksums.txt
    cd {{download-dir}}
    md5sum --check $CHECKSUM_FILE

preprocess: verify-env
    python preprocess.py \
      --trn {{download-dir}}/trn --mp3 {{download-dir}}/mp3 \
      --wav {{work-dir}}/wav --tsv {{work-dir}}/tsv

[confirm]
delete-adf-dir: verify-env
    @echo "deleting {{storage-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{storage-dir}}

[confirm]
delete-download-dir: verify-env
    @echo "deleting {{download-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{download-dir}}

[confirm]
delete-extract-dir: verify-env
    @echo "deleting {{work-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{work-dir}}

# checks ADF directories in .env are defined, exists, and can be written too
@verify-env:
    ../../../tools/verify_env.sh