import json
import pathlib
import string

from typing import List

import click
import joblib
import tqdm
import torchaudio
import pandas as pd


def get_json_of_audio_file(audio_file: pathlib.Path, transcription: str):
    key = audio_file.stem

    meta: torchaudio.AudioMetaData = torchaudio.info(audio_file)
    assert meta.sample_rate == 16_000

    json_content = {
        "identifier": key,
        "transcription": [{"token": word.lower()} for word in transcription.split(" ")],
        "number_of_speakers": 1,
        "number_of_channels": meta.num_channels,
        "number_of_samples": meta.num_frames,
        "sample_rate": meta.sample_rate,
    }

    return json_content


def _parse_ltr(line: str, remove_in_brackets: bool = False):
    words = ["".join(w.strip().split(" ")) for w in line.split("|") if len(w) > 0]

    if remove_in_brackets:
        new_words = []
        for w in words:
            if w.startswith("[") and w.endswith("]"):
                continue
            new_words.append(w)
        words = new_words

    sentence = " ".join(words)
    sentence = sentence.strip()

    return sentence


def delete_invalid(
    audio_files: List[pathlib.Path],
    df: pd.DataFrame,
):
    df = df.loc[df["valid"] == False]
    invalid_keys = set(df["key"])
    print(f"found {len(df)} invalid files")

    filtered = []
    for f in audio_files:
        key = f.stem

        if key in invalid_keys:
            print("deleting", f)
            f.unlink()
        else:
            filtered.append(f)

    return filtered


def find_transcription(audio_file: pathlib.Path, meta_df: pd.DataFrame):
    key = audio_file.stem
    row = meta_df.loc[meta_df["key"] == key]

    if len(row) != 1:
        print(row)
        exit()
    assert len(row) == 1

    transcription = row["transcription"].item()
    assert len(transcription) > 0

    return transcription


def get_meta_dataframe(meta_folder: pathlib.Path):
    train_tsv = meta_folder / "train.tsv"
    train_ltr = meta_folder / "train.ltr"

    test_tsv = meta_folder / "eval.tsv"
    test_ltr = meta_folder / "eval.ltr"

    # load train
    train_df = pd.read_csv(train_tsv, sep="\t", header=0, names=["file", "length"])
    with train_ltr.open("r") as f:
        train_transcriptions = [_parse_ltr(l.strip()) for l in f.readlines()]

    train_df["transcription"] = train_transcriptions
    train_df["split"] = "train"
    train_df["valid"] = True

    train_vocab = sorted(list(set("".join(train_df["transcription"]))))

    assert len(train_vocab) == 28
    assert all(c in train_vocab for c in string.ascii_lowercase)
    assert " " in train_vocab and "'" in train_vocab

    # load test
    test_df = pd.read_csv(test_tsv, sep="\t", header=0, names=["file", "length"])
    with test_ltr.open("r") as f:
        test_transcriptions = [
            _parse_ltr(l.strip(), remove_in_brackets=True) for l in f.readlines()
        ]

    test_df["transcription"] = test_transcriptions
    test_df["split"] = "test"

    # put nbest id's which are ignored due to annoying characters
    test_df["valid"] = test_df["transcription"].map(
        lambda s: not any(c not in train_vocab for c in s) and len(s) > 0
    )

    # merge df
    df = pd.concat([train_df, test_df])
    return df


@click.command
@click.argument("extract_dir", type=pathlib.Path)
@click.option("--meta", "meta_folder", required=True, type=pathlib.Path)
def main(extract_dir: pathlib.Path, meta_folder: pathlib.Path):
    # load transcriptions
    meta_df = get_meta_dataframe(meta_folder)
    meta_df["key"] = meta_df["file"].map(lambda s: pathlib.Path(s).stem)

    assert len(meta_df) == len(meta_df["key"].unique())
    meta_df.to_csv(meta_folder / "dataframe.tsv", sep="\t", index=False)

    # create a JSON file for each audio file
    print(f"finding all `*.wav` files in {extract_dir}")
    audio_files = [f for f in extract_dir.rglob("*.wav")]

    # delete invalid audio files
    print("finding invalid audio files")
    audio_files = delete_invalid(audio_files, meta_df)

    # cache key and transcriptions
    transcription_cache = {}

    print("making transcription cache")
    for name, row in meta_df.iterrows():
        key = row["key"]
        transcription = row["transcription"]
        transcription_cache[key] = transcription

    # write json of each valid file
    def _write(audio_file: pathlib.Path, transcript: str):
        json_file = audio_file.parent / f"{audio_file.stem}.json"
        json_content = get_json_of_audio_file(audio_file, transcript)

        with json_file.open("w") as f:
            json.dump(json_content, f)

    parallel = joblib.Parallel(n_jobs=-1, return_as="generator")
    generator = parallel(
        joblib.delayed(_write)(af, transcription_cache[af.stem]) for af in audio_files
    )

    # show progress bar
    print("writing JSON files...")
    for _ in tqdm.tqdm(generator, total=len(audio_files)):
        pass


if __name__ == "__main__":
    main()
