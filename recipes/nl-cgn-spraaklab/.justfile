set dotenv-load

# folders for librispeech
storage-dir     := "${ADF_STORAGE_DIR}/cgn-spraaklab/storage"
download-dir    := "${ADF_STORAGE_DIR}/cgn-spraaklab/download"
work-dir        := "${ADF_WORK_DIR}/cgn-spraaklab/work"

# Performs all steps to start using the librispeech dataset
setup: extract-data write-json write-tar write-idx

# Get all wav files into the work-dir
extract-data: verify-env
    # extract data to work-dir
    tar xfv {{download-dir}}/cgn.tar -C {{work-dir}} --strip-components=1
    ls {{work-dir}}

# Write a label file for each audio file in extracted dataset
write-json: verify-env
    # write json files
    python write_json.py {{work-dir}} --meta {{download-dir}}/meta

# Write the data into ADF format
write-tar: verify-env
    # train
    adf tar \
    --sort-by-length --frames-per-tar 5h --name cgn_spraaklab \
    {{work-dir}}/breedband \
    {{work-dir}}/breedband-vl \
    {{work-dir}}/common_voice_nl \
    {{work-dir}}/telefoon \
    {{storage-dir}}/train

    # test
    adf tar \
    --sort-by-length --frames-per-tar 5h --name cgn_spraaklab \
    {{work-dir}}/nbest {{storage-dir}}/test

write-idx: verify-env
    adf index {{storage-dir}}/train --subset train
    adf index {{storage-dir}}/test --subset test

[confirm]
delete-adf-dir: verify-env
    @echo "deleting {{storage-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{storage-dir}}

[confirm]
delete-extract-dir: verify-env
    @echo "deleting {{work-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{work-dir}}

# checks ADF directories in .env are defined, exists, and can be written too
@verify-env:
    ../../tools/verify_env.sh
    mkdir -p {{storage-dir}}
    mkdir -p {{download-dir}}
    mkdir -p {{work-dir}}
