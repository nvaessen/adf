set dotenv-load

# folders for librispeech
storage-dir     := "${ADF_STORAGE_DIR}/libri-light/storage"
download-dir    := "${ADF_DOWNLOAD_DIR}/libri-light/download"
work-dir        := "${ADF_WORK_DIR}/libri-light/work"

# Performs all steps to start using the librispeech dataset
setup: download extract to-wav write-json write-tar write-idx

# Downloads tar.gz files from OpenSRL
download: verify-env
    ./download_librispeech_10h.sh {{download-dir}}

# Extract the downloaded tar files
extract: verify-env
    ./untar_librispeech_10h.sh {{download-dir}} {{work-dir}}

# Converts audio to 16khz wav files
to-wav: verify-env
    ../../tools/convert/wav.py --dir {{work-dir}} --ext .flac --workers $(nproc) --overwrite

# Compute the MFCC features of the audio
to-mfcc: verify-env
    ../../tools/convert/mfcc.py --dir {{work-dir}} --ext .flac --workers $(nproc) --overwrite

# Write a label file for each audio file in extracted dataset
write-json: verify-env
    ../librispeech/write_librispeech_json.py {{work-dir}} --allow-duplicate

# Write the data into ADF format
write-tar: verify-env \
    _write_train_10m _write_train_1h _write_train_10h

write-idx: verify-env
    adf index {{storage-dir}}/train-10m --subset train
    adf index {{storage-dir}}/train-1h --subset train
    adf index {{storage-dir}}/train-10h --subset train

_write_train_10m: verify-env
    # train - 10 minutes
    adf tar \
    --sort-by-length --files-per-tar -1 --name ls_train_10m \
    {{work-dir}}/LibriSpeech/train-10m \
    {{storage-dir}}/train-10m

_write_train_1h: verify-env
    # train - 1 hour
    adf tar \
    --sort-by-length --files-per-tar -1 --name ls_train_1h \
    {{work-dir}}/LibriSpeech/train-1h \
    {{storage-dir}}/train-1h


_write_train_10h: verify-env
    # train - 10 hour
    adf tar \
    --sort-by-length --files-per-tar -1 --name ls_train_10h \
    {{work-dir}}/LibriSpeech/train-10h \
    {{storage-dir}}/train-10h

[confirm]
delete-adf-dir: verify-env
    @echo "deleting {{storage-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{storage-dir}}

[confirm]
delete-download-dir: verify-env
    @echo "deleting {{download-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{download-dir}}

[confirm]
delete-extract-dir: verify-env
    @echo "deleting {{work-dir}} in 10 seconds..."
    @sleep 10s
    rm -rf {{work-dir}}

# checks ADF directories in .env are defined, exists, and can be written too
@verify-env:
    ../../tools/verify_env.sh