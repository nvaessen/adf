#! /usr/bin/env bash
set -e

# default directory to save files in
DOWNLOAD_DIR=$1

# make sure all potential directories exist
mkdir -p "$DOWNLOAD_DIR"

# default directory to save files in
echo "Downloading libri-light fine-tuning dataset to $DOWNLOAD_DIR"

# bash function to download/verify/skip if exists
download() {
  # input
  URL=$1
  FILENAME=$2
  CHECKSUM=$3

  # check if file with checksum exists; this means we can safely skip download
  if [ ! -f "$DOWNLOAD_DIR/$FILENAME.checksum.txt" ]; then
    echo
    echo "downloading $FILENAME"
    curl -C - "$URL" --output "$DOWNLOAD_DIR/$FILENAME"
    echo "verifying checksum..."
    ../../tools/verify_checksum.sh "$DOWNLOAD_DIR/$FILENAME" "$CHECKSUM"
    echo "$CHECKSUM" > "$DOWNLOAD_DIR/$FILENAME.checksum.txt"
  else
    echo "$DOWNLOAD_DIR/$FILENAME exists and the checksum verified (as corresponding .checksum.txt file exists)"
  fi
}

# download the 10m/1h/10h split
download https://dl.fbaipublicfiles.com/librilight/data/librispeech_finetuning.tgz librispeech_10h.tar.gz 7f83024cb1334bfa372d1af2c75c3a77

# download the LibriSpeech dev-clean set for the SPEAKERS.txt file
download https://openslr.trmal.net/resources/12/dev-clean.tar.gz dev-clean.tar.gz 42e2234ba48799c1f50f24a7926300a1
