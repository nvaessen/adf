#! /usr/bin/env bash
set -e

# define paths to tar files and output location
DATA_DIR=$1
EXTRACT_DIR=$2

# make sure extraction dir exists
mkdir -p "$EXTRACT_DIR"

# utility function for extracting
extract() {
  tar_file=$1
  if [ -f "$tar_file" ]; then
    echo "extracting $tar_file"
    tar xzfv "$tar_file" -C "$EXTRACT_DIR" > /dev/null
  else
     echo "file $tar_file does not exist."
  fi
}

# extract the 10m/1h/10h split from the archive
extract "$DATA_DIR"/librispeech_10h.tar.gz

# get the speaker labels from the LibriSpeech dev-clean tarfile
extract "$DATA_DIR"/dev-clean.tar.gz
rm -rf "$EXTRACT_DIR"/LibriSpeech/dev-clean # but keep the .TXT files

# make a new folder for each set which mimics data structure of LibriSpeech
mkdir -p "$EXTRACT_DIR"/LibriSpeech/train-10m
mkdir -p "$EXTRACT_DIR"/LibriSpeech/train-1h
mkdir -p "$EXTRACT_DIR"/LibriSpeech/train-10h

# 10m
rsync "$EXTRACT_DIR"/librispeech_finetuning/1h/0/*/ "$EXTRACT_DIR"/LibriSpeech/train-10m/ -a

# 1h
rsync "$EXTRACT_DIR"/librispeech_finetuning/1h/*/*/ "$EXTRACT_DIR"/LibriSpeech/train-1h/ -a

# 10h
rsync "$EXTRACT_DIR"/librispeech_finetuning/1h/*/*/ "$EXTRACT_DIR"/LibriSpeech/train-10h/ -a
rsync "$EXTRACT_DIR"/librispeech_finetuning/9h/*/ "$EXTRACT_DIR"/LibriSpeech/train-10h/ -a

# fixes the transcription files of sessions which are in both 1h and 9h split
# (and thus would be overwritten by rsync)
echo 'fixing transcription labels due to rsync overwrite'
python3 fix_txt_10h.py "$EXTRACT_DIR"/LibriSpeech/train-10h/ "$EXTRACT_DIR"/librispeech_finetuning/

# delete original extraction
rm -rf "$EXTRACT_DIR"/librispeech_finetuning

