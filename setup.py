from distutils.core import setup

from setuptools import find_packages

setup(
    name="adf",
    version="0.1",
    description="",
    author="Nik Vaessen",
    author_email="nikvaes@gmail.com",
    packages=find_packages(),
    install_requires=[
        "fastjsonschema",
        "click",
        "pyarrow",
        "pandas",
        "pandera",
        "tqdm",
        "torch",
        "torchaudio",
        "numpy",
        "python-dotenv",
    ],
    entry_points={
        "console_scripts": [
            "adf=adf.cli:cli",
            "adf-tools=tools.cli:main",
        ],
    },
)
