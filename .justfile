set dotenv-load

# full-blown datasets for speech tasks
mod librispeech 'recipes/librispeech/.justfile'
mod libri-light 'recipes/libri-light/.justfile'
mod voxceleb 'recipes/voxceleb/.justfile'

# dutch
mod nl-cgn-spraaklab 'recipes/nl-cgn-spraaklab/.justfile'
mod nl-mls 'recipes/nl-mls/.justfile'
mod nl-cv 'recipes/nl-cv/.justfile'

# noise
mod fma 'recipes/fma/.justfile'
mod musan 'recipes/musan/.justfile'

# only for superb
mod speech-commands 'recipes/superb/speech-commands/.justfile'
mod cv-v4-translate 'recipes/superb/cv-v4-translate/.justfile'
mod cv-v7-ood 'recipes/superb/cv-v7-ood/.justfile'
mod SBCSAE 'recipes/superb/SBCSAE/.justfile'
mod quesst14 'recipes/superb/quesst14/.justfile'
mod vc1 'recipes/superb/vc1/.justfile'
mod librimix 'recipes/superb/librimix/.justfile'
mod ls100h 'recipes/superb/ls100h/.justfile'
mod iemocap 'recipes/superb/iemocap/.justfile'
mod fluent 'recipes/superb/fluent/.justfile'
mod vcc2020 'recipes/superb/vcc2020/.justfile'
mod snips 'recipes/superb/snips/.justfile'
mod voicebank 'recipes/superb/voicebank/.justfile'

# List all recipes
default:
  @just --unstable --unsorted --list

