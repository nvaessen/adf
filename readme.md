# Audio Dataset Format

Audio Dataset Format (ADF) is a description. Some plot of how fast it is.

## Goals of ADF

1. Fast(est) loading of (large) audio data for (data-parallel) neural network training
2. Flexible data manipulation, including custom sampling and augmentation
3. Reproducible, robust and verifiable data pipelines
4. Recipes for setting up ADF with most state-of-the-art audio datasets
5. Tools for pre-processing audio to create new dataset recipes
6. Designed to work with network file systems (prevent many small files)

## High-level design overview of ADF

ADF

```
tar1.tar
tar1.idx
tar2.tar
tar2.idx
other1.tar
other2.idx
schema.json
```

### Map-based versus Iterable

If it's important to read the data in a streaming fashion: iterator dataset

If more precision for selecting certain samples is required: map-based at the cost of
opening more file-handles

## ADF cli API

```
adf create --json-schema /path/to/schema.json --name  /path/to/dir /path/to/other-dir /path/to/write-dir
adf info /path/to/write-dir
adf 
```

## Usage

Overview of usage

### Basic usage

### Datasets

List of datasets provided out-of-the-box:

Default storage locations:

...

### Distributed data-parallel usage

### Manipulating the data

###       