import pathlib
import shutil

wav_dir = pathlib.Path("/home/nik/data/adf-work/librispeech/extract/")
nfs_dir = pathlib.Path("/mnt/data/benchmark/librispeech")
txt_file = pathlib.Path("wav_files.txt")
nfs_text_file = pathlib.Path("wav_files.nfs.txt")
limit = 10000

audio_paths = [fn for fn in sorted(wav_dir.rglob("*.wav"))][0:limit]

with txt_file.open("w") as f:
    for fn in audio_paths:
        f.write(f"{str(fn.absolute())}\n")

with nfs_text_file.open("w") as f:
    nfs_dir.mkdir(parents=True, exist_ok=True)

    for fn in audio_paths:
        copy_path = nfs_dir / fn.name
        shutil.copy(fn, copy_path)
        f.write(f"{str(copy_path.absolute())}\n")
