import pathlib

import numpy as np
import pandas as pd
import torch
import torchaudio
import pyarrow as pa

from rich.progress import track
from typing import List
from torch.utils.data import Dataset, DataLoader

txt_file = pathlib.Path("wav_files.txt")
arrow_file = pathlib.Path("wav_files.np.pd.arrow")


class FeatherDataset(Dataset):
    pass

    def __init__(self, data_file: pathlib.Path):
        self.data_file = data_file
        self.reader = pa.ipc.open_file(self.data_file)

    def __len__(self):
        return self.reader.num_record_batches

    def __getitem__(self, item: int):
        batch: pd.DataFrame = self.reader.get_batch(item).to_pandas()
        audio_tensor = torch.from_numpy(next(batch.itertuples()).audio).clone()

        return audio_tensor

    @staticmethod
    def collate(samples: List[torch.Tensor]):
        original_size = [s.shape[0] for s in samples]
        max_size = max(original_size)

        samples = [
            torch.nn.functional.pad(s, (0, max_size - s.shape[0])) for s in samples
        ]

        return torch.stack(samples), original_size


def main():
    num_workers = 4
    batch_size = 100
    num_files = 10000

    # create feather file
    if not arrow_file.exists():
        with pa.OSFile(str(arrow_file), mode="w") as sink:
            example_batch = pa.RecordBatch.from_pandas(
                pd.DataFrame.from_dict(
                    {
                        "path": ["a/b/c", "x/y/x"],
                        "audio": [np.zeros((16_000,)), np.zeros((32_000,))],
                    }
                )
            )
            schema = example_batch.schema
            print(schema)

            with pa.ipc.new_file(sink, schema) as writer:
                with txt_file.open("r") as f:
                    audio_paths = [pathlib.Path(ln.strip()) for ln in f.readlines()][
                        0:num_files
                    ]

                for f in track(audio_paths):
                    audio_tensor, sr = torchaudio.load(f)

                    batch = pa.RecordBatch.from_pandas(
                        pd.DataFrame.from_dict(
                            {
                                "path": [str(f.absolute())],
                                "audio": [audio_tensor.squeeze().numpy()],
                            }
                        ),
                        schema=schema,
                    )
                    writer.write_batch(batch)

    ds = FeatherDataset(arrow_file)
    print(f"using {num_workers} workers with batch size {batch_size}")

    total_original = 0
    total_padded = 0
    for batch, lengths in track(
        DataLoader(
            ds,
            batch_size=batch_size,
            collate_fn=FeatherDataset.collate,
            num_workers=num_workers,
        )
    ):
        total_original += sum(lengths)
        total_padded += batch.shape[0] * batch.shape[1]

    print(
        f"original data had {total_original} frames "
        f"({total_original/16_000/60/60:.2f} hours)"
    )
    print(
        f"padded data had {total_padded} frames "
        f"({total_padded/16_000/60/60:.2f} hours)"
    )


if __name__ == "__main__":
    main()
