import pathlib

import click
import pandas as pd
import torch
import torchaudio
import pyarrow as pa

from rich.progress import track
from typing import List
from torch.utils.data import Dataset, DataLoader


class FeatherDataset(Dataset):
    def __init__(self, data_file: pathlib.Path):
        self.data_file = data_file

        # should be none on the main thread (and thus when calling __init__)
        self.worker_info = torch.utils.data.get_worker_info()
        self.worker_init = False

        self.readers = {self.worker_info: pa.ipc.open_file(self.data_file)}

    def __len__(self):
        return self.readers[self.worker_info].num_record_batches

    @staticmethod
    def worker_init_fn(worker_id: int):
        worker_info = torch.utils.data.get_worker_info()
        self = worker_info.dataset

        self.worker_info = worker_info
        self.readers[worker_info] = pa.ipc.open_file(self.data_file)

    def __getitem__(self, item: int):
        batch: pa.RecordBatch = self.readers[self.worker_info].get_batch(item)

        path = batch[0].tolist()[0]
        binary_str = batch[1].tolist()[0]

        audio_tensor, sr = torchaudio.load(binary_str)

        return audio_tensor.squeeze()

    @staticmethod
    def collate(samples: List[torch.Tensor]):
        original_size = [s.shape[0] for s in samples]
        max_size = max(original_size)

        samples = [
            torch.nn.functional.pad(s, (0, max_size - s.shape[0])) for s in samples
        ]

        return torch.stack(samples), original_size


@click.command()
@click.option("-w", "--workers", type=int, default=0)
@click.option("-b", "--batch-size", type=int, default=100)
@click.option("-n", "--use-nfs", is_flag=True, default=False)
def main(workers: int, batch_size: int, use_nfs: bool):
    print(f"{workers=} {batch_size=} {use_nfs=}")

    nfs_dir = pathlib.Path("/mnt/data/benchmark/")

    if use_nfs:
        txt_file = pathlib.Path("wav_files.nfs.txt")

        arrow_file = nfs_dir / "wav_files.binary.arrow"
    else:
        txt_file = pathlib.Path("wav_files.txt")
        arrow_file = pathlib.Path("wav_files.binary.arrow")

    # create feather file
    if not arrow_file.exists():
        with pa.OSFile(str(arrow_file), mode="w") as sink:
            schema = pa.schema([("path", pa.string()), ("file", pa.binary())])

            with pa.ipc.new_file(sink, schema) as writer:
                with txt_file.open("r") as f:
                    audio_paths = [pathlib.Path(ln.strip()) for ln in f.readlines()]

                for f in track(audio_paths):
                    batch = pa.RecordBatch.from_pydict(
                        pd.DataFrame.from_dict(
                            {
                                "path": [str(f.absolute())],
                                "file": [f.read_bytes()],
                            }
                        ),
                        schema=schema,
                    )
                    writer.write_batch(batch)

    ds = FeatherDataset(arrow_file)

    total_original = 0
    total_padded = 0
    for batch, lengths in track(
        DataLoader(
            ds,
            batch_size=batch_size,
            collate_fn=FeatherDataset.collate,
            num_workers=workers,
            worker_init_fn=FeatherDataset.worker_init_fn,
        )
    ):
        total_original += sum(lengths)
        total_padded += batch.shape[0] * batch.shape[1]

    print(
        f"original data had {total_original} frames "
        f"({total_original/16_000/60/60:.2f} hours)"
    )
    print(
        f"padded data had {total_padded} frames "
        f"({total_padded/16_000/60/60:.2f} hours)"
    )


if __name__ == "__main__":
    main()
