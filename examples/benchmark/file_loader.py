import pathlib

import click
import torch
import torchaudio

from rich.progress import track
from typing import List
from torch.utils.data import Dataset, DataLoader


class FileDataset(Dataset):
    def __init__(self, audio_paths: List[pathlib.Path]):
        self.audio_paths = audio_paths
        print(f"loading {len(audio_paths)} files")

    def __len__(self):
        return len(self.audio_paths)

    def __getitem__(self, item: int):
        file = self.audio_paths[item]

        audio_tensor, sr = torchaudio.load(file)
        return audio_tensor

    @staticmethod
    def collate(samples: List[torch.Tensor]):
        original_size = [s.shape[1] for s in samples]
        max_size = max(original_size)

        samples = [
            torch.nn.functional.pad(s, (0, max_size - s.shape[1])) for s in samples
        ]

        return torch.concat(samples, dim=0), original_size


@click.command()
@click.option("-w", "--workers", type=int, default=0)
@click.option("-b", "--batch-size", type=int, default=100)
@click.option("-n", "--use-nfs", is_flag=True, default=False)
def main(workers: int, batch_size: int, use_nfs: bool):
    print(f"{workers=} {batch_size=} {use_nfs=}")

    if use_nfs:
        txt_file = pathlib.Path("wav_files.nfs.txt")
    else:
        txt_file = pathlib.Path("wav_files.txt")

    # load list of files
    with txt_file.open("r") as f:
        audio_paths = [pathlib.Path(ln.strip()) for ln in f.readlines()]

    ds = FileDataset(audio_paths)

    total_original = 0
    total_padded = 0
    for batch, lengths in track(
        DataLoader(
            ds,
            batch_size=batch_size,
            collate_fn=FileDataset.collate,
            num_workers=workers,
        )
    ):
        total_original += sum(lengths)
        total_padded += batch.shape[0] * batch.shape[1]

    print(
        f"original data had {total_original} frames "
        f"({total_original/16_000/60/60:.2f} hours)"
    )
    print(
        f"padded data had {total_padded} frames "
        f"({total_padded/16_000/60/60:.2f} hours)"
    )


if __name__ == "__main__":
    main()
