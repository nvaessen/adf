| Command | Mean [s] | Min [s] | Max [s] | Relative |
|:---|---:|---:|---:|---:|
| `python file_loader.py -w 0` | 70.437 ± 0.378 | 70.000 | 70.672 | 5.36 ± 0.06 |
| `python file_loader.py -w 0 --use-nfs` | 74.391 ± 0.268 | 74.202 | 74.698 | 5.66 ± 0.06 |
| `python arrow_loader_binary.py -w 0` | 66.376 ± 0.252 | 66.096 | 66.586 | 5.05 ± 0.05 |
| `python arrow_loader_binary.py -w 0 --use-nfs` | 65.104 ± 1.212 | 63.740 | 66.057 | 4.95 ± 0.10 |
| `python file_loader.py -w 1` | 68.095 ± 0.233 | 67.846 | 68.307 | 5.18 ± 0.05 |
| `python file_loader.py -w 1 --use-nfs` | 71.714 ± 0.320 | 71.453 | 72.071 | 5.45 ± 0.06 |
| `python arrow_loader_binary.py -w 1` | 65.068 ± 0.553 | 64.450 | 65.516 | 4.95 ± 0.06 |
| `python arrow_loader_binary.py -w 1 --use-nfs` | 64.473 ± 0.525 | 63.870 | 64.834 | 4.90 ± 0.06 |
| `python file_loader.py -w 2` | 35.833 ± 0.176 | 35.672 | 36.022 | 2.72 ± 0.03 |
| `python file_loader.py -w 2 --use-nfs` | 37.402 ± 0.184 | 37.269 | 37.611 | 2.84 ± 0.03 |
| `python arrow_loader_binary.py -w 2` | 34.565 ± 0.141 | 34.438 | 34.717 | 2.63 ± 0.03 |
| `python arrow_loader_binary.py -w 2 --use-nfs` | 34.198 ± 0.112 | 34.112 | 34.324 | 2.60 ± 0.03 |
| `python file_loader.py -w 4` | 20.037 ± 0.025 | 20.012 | 20.061 | 1.52 ± 0.01 |
| `python file_loader.py -w 4 --use-nfs` | 20.807 ± 0.065 | 20.732 | 20.850 | 1.58 ± 0.02 |
| `python arrow_loader_binary.py -w 4` | 19.393 ± 0.082 | 19.342 | 19.488 | 1.47 ± 0.01 |
| `python arrow_loader_binary.py -w 4 --use-nfs` | 19.378 ± 0.127 | 19.243 | 19.495 | 1.47 ± 0.02 |
| `python file_loader.py -w 8` | 13.150 ± 0.119 | 13.039 | 13.276 | 1.00 |
| `python file_loader.py -w 8 --use-nfs` | 13.710 ± 0.202 | 13.485 | 13.874 | 1.04 ± 0.02 |
| `python arrow_loader_binary.py -w 8` | 13.180 ± 0.110 | 13.056 | 13.265 | 1.00 ± 0.01 |
| `python arrow_loader_binary.py -w 8 --use-nfs` | 13.266 ± 0.315 | 12.940 | 13.569 | 1.01 ± 0.03 |
